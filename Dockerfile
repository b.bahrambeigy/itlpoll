FROM alpine:3.7
MAINTAINER Bahram Bahrambeigy <bahramwhh@gmail.com>

RUN apk --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ \
    add \
    curl \
    bash \
    php7 \
    mariadb-client \
    openssl \
    libmcrypt \
    php7-openssl \
    php7-fpm \
    php7-json \
    php7-phar \
    php7-opcache \
    php7-curl \
    php7-iconv \
    php7-mbstring \
    php7-zlib \
    php7-session \
    php7-ctype \
    php7-gd \
    php7-xml \
    php7-simplexml \
    php7-mysqli \
    php7-dom && \
    ln -s /usr/sbin/php-fpm7 /usr/sbin/php-fpm

USER root
COPY src /src

COPY entrypoint.sh /entrypoint.sh

EXPOSE 9000/tcp

WORKDIR /src
ENTRYPOINT ["/entrypoint.sh"]