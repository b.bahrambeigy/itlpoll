ITLPoll 
A Simple and fast PHP7 & MySQL/MariaDB polling system. 
Ready to be used on docker. 

# How to use docker-compose: 
* First: 
 - docker-compose build 
* Then: 
 - docker-compose up 

* Now enjoy ITLPoll on your localhost:
  - FrontEnd: http://127.0.0.1
  - AdminPanel: http://127.0.0.1/admin/index.php
