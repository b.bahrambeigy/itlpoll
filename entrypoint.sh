#!/bin/sh

DB_HOST="itlpoll_mysql"

while ! mysqladmin ping -h"$DB_HOST" --silent; do
    sleep 1
done

php /src/install/cli.php itlpoll_mysql itlpoll itlpoll itlpoll && php-fpm -F -R -O
