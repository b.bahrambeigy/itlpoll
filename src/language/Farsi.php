<?php
/* **********************************************************************
*************************************************************************
####    This App Is Written By : Bahrambeigy - ITLPoll Version 3    #####
########       [https://gitlab.com/b.bahrambeigy/itlpoll]        ########
####              This application is free of charge :)             #####
####                Contact me: bahramwhh@gmail.com                 #####
*************************************************************************
#########################################################################
File : farsi.php
Writer : Bahrambeigy(bahramwhh@gmail.com)
Description : This File Is The Persian Language For ITLPoll System For
Using This Language Set it As Default Language In Admin Panel.
**********************************************************************
*/

// General Strings
define("_SUBMIT","ثبت نظر");
define("_RESET","پاک کردن کادرها");
define("_HTMLRESULTS","نتایج به صورت HTML");
define("_VOTED","رای داده شده");
define("_IMGRESULTS", "نتایج به صورت عکس");
define("_TOTALVOTED","تعداد نظرهای داده‌شده");
define("_THANKS", "با تشکر از شرکت شما در نظرسنجی");
define("_SURVEY", "صفحه نظرخواهی [ITLPoll]");

// Voters Section
define("_SURVEYNEEDSLOGGING", "این نظرخواهی نیاز به وارد شدن به سیستم دارد.</b><br/> پس اگر اشتراک رای‌دهی دارید، وارد شوید. درغیراین‌صورت می‌توانید ثبت نام کنید.");
define("_REGISTERVOTER", "ایجاد یک اشتراک رای‌دهی جدید");
define("_LOGINDETAILS", "اطلاعات ورود");
define("_REGISTERETAILS", "اطلاعات ثبت‌نام");
define("_USERNAMEEMAIL", "نام کاربری (ایمیل) شما");
define("_VOTERSPASSWORD", "رمز عبور شما");
define("_VOTERSPASSWORD2", "تکرار مجدد رمزعبور");
define("_IMAGEVERIFICATION", "تاییده امنیتی");
define("_VOTERSLOGINTITLE", "نظرخواهی نیاز به ورود دارد");
define("_REGISTEREXPLAIN", "لطفا فرم زیر را جهت ایجاد یک اشتراک رای‌دهی جدید پر کنید");
define("_VOTERLOGGEDOUT", "شما هم اکنون خارج شده‌اید.");
define("_VOTERSMANAGEMENT", "مدیریت رای‌دهندگان");
define("_VOTERSACCOUNTINGDISABLED", "حسابرسی رای‌دهندگان غیرفعال است");
define("_ENABLEVOTERSACCOUNTING", "فعال‌کردن حسابرسی رای‌دهندگان");
define("_ENABLEDISABLEVOTERMANAGEADMINCONFIRM", "فعال/غیرفعال کردن کل سیستم حسابرسی رای‌دهندگان یا تایید مدیریت آن‌ها");
define("_DISABLEVOTERSACCOUNTING", "غیرفعال کردن حسابرسی رای‌دهندگان");
define("_ENABLEADMINCONFRIMATION", "فعال کردن تایید مدیر");
define("_DISABLEADMINCONFRIMATION", "غیرفعال کردن تایید مدیر");
define("_VOTERMGMSEARCHEXPLAIN", "جستجو برای یک رای‌دهنده و ویرایش و یا حذف آن : ");
define("_SEARCH", "جستجو");
define("_VOTERSMGMWAITINGEXPLAIN", "رای‌دهندگانی که منتظر تایید مدیر هستند. بر روی نام‌(های) کاربری کلیک کنید تا فعال شوند : ");
define("_ENABLED", "فعال شده");
define("_VERIFIED", "تایید شده");
define("_LATESTPARTVOTERS", "آخرین رای‌دهندگان شرکت‌کننده در رای‌گیری");
define("_TOTALVOTES", "تعداد رای‌ها");
define("_LASTLOGINDATE", "تاریخ آخرین ورود");
define("_LASTVOTEDDATE", "آخرین تاریخ رای");
define("_LASTVOTEFROM", "آخرین رای از");
define("_EDITVOTERPROFILE", "ویرایش پروفایل رای‌دهنده : ");
define("_REMOVEACCOUNT", "حذف اشتراک");
define("_UPDATEACCOUNT", "به‌روزرسانی اشتراک");
define("_VOTERMGMENABLEDSUCC", "حسابرسی اشتراک‌های رای‌دهندگان با موفقیت فعال گردید.");
define("_SELCECTEDVOTERENABLEDSUCC", "اشتراک رای‌دهنده‌ی انتخاب شده با موفقیت فعال گردید.");
define("_VOTERMGMDISABLEDSUCC", "حسابرسی اشتراک‌های رای‌دهندگان با موفقیت غیرفعال گردید.");
define("_VOTERADMINCONFENABLEDSUCC", "تایید مدیر برای رای‌دهندگان با موفقیت فعال گردید.");
define("_VOTERADMINCONFDISABLEDSUCC", "تایید مدیر برای رای‌دهندگان با موفقیت غیرفعال گردید.");
define("_PLZPROVIDEUSERNAME", "لطفاً نام‌کاربری برای جستجو ارائه کنید !");
define("_NOSUCHVOTERUSER", "چنین کاربر رای‌دهنده‌ی با این نام وجود ندارد !");
define("_VOTERACCDELETEDSUCC", "اشتراک رای‌دهنده با موفقیت حذف گردید.");
define("_VOTERPROFILEUPDATEDSUCC", "اطلاعات اشتراک رای‌دهنده با موفقیت به‌روز گردید.");
define("_VOTERNOWAITINGUSER", "هیچ رای‌دهنده‌ی منتظر تایید وجود ندارد ;)");
define("_VOTERTHEREISNOVOTEATALL", "هیچ رایی کلاً وجود ندارد. ;)");
define("_THANKSVOTERREGISTER", "با تشکر. یک نامه به ایمیل شما ارسال شده است. شما باید بر روی لینک موجود در آن کلیک کنید تا اکانت شما فعال شود.");

define("_GENERATIONTIME", "زمان تولید : ");
define("_SECONDS", "(ثانیه)");
define("_CANTEMBEDITLPOLL", "شما نمی‌توانید سیستم را زمانی که بیشتر از یک نظرسنجی فعال است و یا ثبت‌نام رای‌گیران فعال است، به صورت تعبیه‌شده به کار بگیرید.");


// Administrator Languages
define("_UNAME","نام کاربری");
define("_PASS","رمز عبور");
define("_POLLQ","عنوان نظرسنجی");
define("_CHOICES","انتخاب ها");
define("_ACTIVE","فعال کردن");
define("_DELETE","حذف کردن");
define("_CHANGE","ویرایش این نظرسنجی");
define("_EXPIERE","تاریخ انقضا");
define("_ADMIN","ITLPoll پانل مدیریت");
define("_BACKMAIN","برگشت به صفحه ی اصلی");
define("_LOGIN","ورود");
define("_CLICKTOEDIT","جهت ویرایش نظرسنجی شماره %s اینجا را کلیک کنید");
define("_WELCOME","به پانل مدیریت سیستم نظرسنجی سبک فناوری اطلاعات خوش آمدید");
define("_ADDPOLL","درج نظرسنجی جدید");
define("_EDITPOLL","ویرایش و یا حذف نظرسنجی ها");
define("_EDITCONFIGS","ویرایش تنظیمات و مدیریت کاربران");
define("_LOGOUT","خروج");
define("_ADDNOTICE","نکته : زمانی که نظرسنجی جدید را اضافه کردید تا زمانی که آن را فعال نکنید به طور خودکار در صفحه ی کاربر نمایش داده نمی شود.");
define("_EDITNOTICE","برای ویرایش نظرسنجی ها بر روی آیکن مربوطه ی آن در ستون اعداد کلیک کنید.");
define("_DELETENOTICE","برای حذف نظرسنجی ها بر روی آیکن مربوطه در ستون حذف کلیک کنید. دقت کنید که حذف شده ها قابل بازیابی نیستند.");
define("_CHOICENUM","تعداد گزینه های نظرسنجی را وارد کنید");
define("_EDITINGPOLL","ویرایش نظرسنجی");
define("_CONTINUE","ادامه");
define("_ENTERUQH","عنوان را اینجا وارد کنید");
define("_ENTERCHOICES","گزینه ها را اینجا وارد کنید");
define("_JALALINOTE", "تاریخ ها را تنها در فرمت هجری شمسی (جلالی) وارد کنید!");
define("_DATEEXPLAIN","تنها با فرمت YYYY-MM-DD");
define("_ENTRSTRDATE", "تاریخ شروع را وارد کنید");
define("_STARTDATE", "تاریخ آغاز");
define("_STARTDATEEXP", "خالی بگذارید اگر نمی خواهید که در تاریخ مشخصی آغاز شود.");
define("_NUMBER","شماره");
define("_YES","بله");
define("_NO","نه");
define("_PAGES", "صفحات");
define("_NEXT", "صفحه بعدی");
define("_PREV", "صفحه قبلی");
define("_ADMINCONFS","تنظیمات محیطی و اشتراک شما");
define("_ADMINNAME","نام کاربری اشتراک");
define("_CHADMINPASS","رمز عبور اشتراک");
define("_PASSNOTICE","برای هرگونه تغییر بایستی رمزعبور خود را مجددا وارد کنید");
define("_CHPASS","تغییر رمزعبور");
define("_DISABLED","غیرفعال کردن سیستم؟");
define("_LANG","زبان پیشفرض سیستم");
define("_TEMPLATE","قالب پیشفرض سیستم");
define("_SAVE","ذخیره تغییرات");
define("_OLDPASS","رمز فعلی");
define("_NEWPASS","رمز جدید");
define("_NEWPASSAGAIN","رمز جدید (دوباره)");
define("_DEFRESULT", "نحوه نمایش نتایج");
define("_DEFGRAPH", "گراف پیشفرض");
define("_COLOR1", "رنگ اول");
define("_COLOR2", "رنگ دوم");
define("_COLORSEL", "راهنمای رنگ");
define("_EMAIL", "پست الکترونیک");
define("_NUMARCHIVE", "تعداد نمایش در آرشیو");
define("_ENTERADMINPASS", "برای امنیت بیشتر شما بایستی برای هرگونه تغییر رمز عبور خود را دوباره وارد کنید");
define("_POLLARCHIVE", "آرشیو نظرسنجی ها");
define("_CONFIRMDEL","آیا مطمئن هستید که می خواهید این نظرسنجی را حذف کنید؟");
define("_LOGGEDOUT","شما هم اکنون از پانل مدیریت خارج شده اید.");
define("_ENTEREXPIRE","تاریخ انقضای نظرسنجی را وارد کنید (خالی=هرگز)");
define("_ADDSUCC","نظرسنجی جدید با موفقیت به سیستم اضافه شد!");
define("_PASSCHSUCC","رمز عبور شما با موفقیت تغییر یافت!");
define("_CONFCHSUCC","تغییرات پیکربندی شما با موفقیت تغییر یافت!");
define("_DELSUCC","نظرسنجی موردنظر با موفقیت حذف شد!");
define("_ACTSUCC","نظرسنجی موردنظر با موفقیت فعال گردید!");
define("_FORGET", "سیستم بازیابی رمزعبور فراموش شده");
define("_RESETPASS", "تغییر رمز");
define("_FORGETPASSWD", "آیا رمز عبور خود را فراموش کرده اید؟");
define("_MAILSENT", "نامه ی تغییر رمز عبور برای پست الکترونیکی شما فرستاده شد!");
define("_PASSRESETED", "رمز عبور شما با موفقیت تغییر یافت");
define("_POLLCHSUCC","نظرسنجی با موفقیت تغییر یافت!");
define("_RIGHTS"," تمام حقوق متعلق است به :  <a href=\"https://gitlab.com/b.bahrambeigy/itlpoll\">سیستم نظرسنجی ITLPoll</a> - نویسنده : بهرام بیگی");
define("_GROUPDUPATED", "حقوق دسترسی گروه ها با موفقیت به روز شد !");
define("_USERUPDATED", "اطلاعات کاربران با موفقیت به روز شد !");
define("_GROUPCREATED", "گروه جدید با موفقیت به سیستم اضافه شد !");
define("_USERCREATED", "کاربر جدید با موفقیت به سیستم اضافه شد !");
define("_MANAGEGROUP", "مدیریت و ایجاد گروه های دسترسی");
define("_NAME", "نام");
define("_CANADD", "توانایی اضافه");
define("_CANEDIT", "توانایی ویرایش");
define("_CANDELETE", "توانایی حذف");
define("_CANCONFIG", "توانایی پیکربندی");
define("_CANGROUP", "توانایی گروه بندی یا ایجاد کاربر");
define("_UPDATE", "به روز کردن");
define("_CREATEGROUP", "ایجاد گروه جدید");
define("_MANAGEUSERS", "مدیریت و ایجاد کاربران");
define("_PERMISSION", "دسترسی");
define("_BLOCK", "مسدود کردن");
define("_CREATEUSER", "ایجاد کاربر جدید");
define("_CREATE", "ایجاد");
define("_GROUPDELETED", "گروه مورد نظر با موفقیت حذف شد!");
define("_USERDELETED", "کاربر موردنظر با موفقیت حذف شد!");
define("_DELETENOTE", "توجه کنید که حذف کردن (( بدون هیچ گونه سوالی )) این کار را انجام می دهد!");
define("_USERWITHGROUP", "حذف این گروه امکان پذیر نیست. زیرا کاربر(هایی) آن را به عنوان گروه انتخاب کرده اند. لذا ابتدا آن گروه(ها) را حذف کرده و سپس اقدام کنید.");
define("_CHANGEDLATESTPOLL", "شما قبلا حداقل در یک نظرسنجی شرکت کرده‌اید اما هم‌اکنون نظرسنجی دیگری فعال است لذا می‌توانید شرکت کنید");
define("_ENABLEMULTIPLE", "فعال کردن انتخاب چندگانه گزینه‌ها؟");
define("_ENABLEMULTIPLESHORT", "چندگانه؟");
define("_SHOWACTIVEANDCONFIRMED", "تنها نظرسنجی‌های فعال و تایید شده را نمایش بده");




// Version 3.1
define("_CLICKTOACTIVATE", "برای فعال کردن این نظرسنجی اینجا را کلیک کنید");
define("_CLICKTODEACTIVATE", "برای غیرفعال کردن این نظرسنجی اینجا را کلیک کنید");
define("_TIP", "نکته");
define("_ACTIVED", "فعال‌شده");
define("_NOTACTIVED", "فعال‌نشده");
define("_CONFIRMED", "تاییدشده");
define("_NOTCONFIRMED", "تاییدنشده");
define("_SAVEACTIVEMSG1", "تغییراتی در نظرسنجی‌های فعال داده شده است. شما می‌توانید عنوان نظرخواهی در صفحه‌ی انتهایی کاربران را اینجا تغییر دهید");
define("_SAVEACTIVEMSG2", "توجه کنید که با زدن دکمه‌ی «ذخیره‌کردن تغییرات» تمامی اعداد آماری موجود در نتایج تمامی نظرسنجی‌های فعال صفر می‌شوند");
define("_SAVEACTIVEMSG3", "و یا اینکه می‌توانید بسادگی دکمه‌ی «نادیده‌گرفتن تغییرات» را بزنید");
define("_SURVEYTITLE", "عنوان پیغام نظرسنجی");
define("_SAVECHANGES", "ذخیره کردن تغییرات");
define("_DISCARDCHANGES", "نادیده‌گرفتن تغییرات");
define("_DISCARDEDCHANGES", "همه‌ی تغییرات نادیده گرفته شدند!");
define("_SAVEDCHANGES", "همه‌ی تغییرات ذخیره شدند و اعداد ریست شدند!");
define("_CONFIRMSAVECHANGES", "آیا مطمئن هستید که می‌خواهید تغییرات را ذخیره کنید ؟! همه‌ی اعداد آماری همه‌ی نظرسنجی‌های فعال صفر می‌شوند!");
define("_HAVELATESTVERSION", "شما آخرین نسخه‌ی سیستم را دارید");
define("_CANTCHECKLATESTVERSION", "خطا در گرفتن آخرین نسخه از وب‌سایت سیستم !");
define("_NEWVERSIONAVAILABLE", "نسخه‌ی جدید سیستم آماده‌ی دانلود است. از <a target='blank' href='http://itlpoll.sourceforge.net/'>اینجا</a> بگیرید");

// version 3.2
define("_BACKTOUSERSFRONTEND", "برگشت به صفحه کاربران نهایی");
define("_SHOWFRONTEND", "نمایش صفحه کاربران نهایی");


// Errors
define("_ECOOKIES","کوکی های مرورگر شما غیرفعال است!");
define("_EPOLLED","شما قبلا رای داده اید و دوباره نمی توانید در نظرسنجی شرکت کنید!");
define("_CANTDEL","با عرض پوزش ! شما نمی توانید نظرسنجی فعال را حذف کنید. لطفا نظرسنجی دیگری را فعال و دوباره اقدام نمایید.");
define("_ESERVER","با عرض پوزش ! یک خطای سرور اتفاق افتاده است. لطفا بعدا مراجعه نمایید.");
define("_EXPIERED", "متاسفانه نظرسنجی فعال و یا یکی از نظرسنجی‌های فعال منقضی شده‌اند - با مدیریت سایت تماس بگیرید "); 
define("_NOTSTARTED", "متاسفانه نظرسنجی فعال و یا یکی از نظرسنجی فعال هنوز شروع نشده‌اند - با مدیریت سایت تماس بگیرید");
define("_WRONGUOP","نام کاربری و یا رمز عبور شما نادرست است!");
define("_BRUTEFORCEREACHED", "با عرض پوزش شما از حد مجاز بیشتر از ۳ بار وارد کردن نام‌کاربری و رمزعبور تجاوز کرده‌اید و برای همین به مدت ۱۰ دقیقه امکان ورود نخواهید داشت"); // 3.1 changed
define("_LOGGED","شما هم اکنون وارد شده اید!");
define("_NOTEXIST","خطا ! این فایل موجود نیست !");
define("_EMYSQL","با عرض پوزش ! سیستم نمی تواند به بانک اطلاعاتی متصل شود. لطفا فایل تنظیمات را بررسی کنید.");
define("_DISABLEDPOLL","با عرض پوزش این نظرسنجی بوسیله ی مدیریت غیرفعال شده است");
define("_YVOTED","شما قبلا شرکت کرده اید!");
define("_NOROWS","با عرض پوزش چنین اطلاعاتی با این شماره در بانک اطلاعاتی موجود نیست !");
define("_UPERROR","نام کاربری و یا رمز عبور شما نادرست است !");
define("_CHERROR","شما هیچ گزینه ای را انتخاب نکرده اید و یا کادر انتخاب خالی است");
define("_PASSNOTMATCH","با عرض پوزش رمز عبور شما با رمز عبور کنونی مطابقت نمی کند!");
define("_PASSERRORM","با عرض پوزش رمز عبور شما با رمز عبور کنونی مطابقت نمی کند!");
define("_PASSERRORA","رمز عبور اول و دوم شما با هم مطابقت نمی کند !");
define("_NOEMAIL", "پست الکترونیکی که وارد نموده اید با اطلاعات اشتراک شما مطابفت نمی کند!");
define("_NOUSER", "نام کاربری که وارد نموده اید با اطلاعات اشتراک شما مطابقت نمی کند!");
define("_ERRORMAIL", "خطا جهت فرستادن ایمیل! لطفا مدتی بعد سعی کنید !");
define("_ERRORRESET", "خطا در تغییر رمز شما در بانک اطلاعتی بوجود آمده است!");
define("_INVCODE", "کد نادرست است!");
define("_DNTREMOVEINST","شما پوشه ی نصب سیستم نظرسنجی فناوری اطلاعات را حذف نکرده اید. با این کار سیستم شما در خطر است. لطفا آن را سریعا حذف کنید!");
define("_NOTVALIDEMAIL", "قالب پست الکترونیکی شما معتبر نیست ! لطفا برگردید و آن را اصلاح کنید.");
define("_NOACTPOLL", "یک مشکل بزرگ! هیچ نظرسنجی فعال نیست ! پس سیستم اولین نظرسنجی موجود را فعال می کند.");
define("_BIGMAXCH", "شما نمی توانید تعداد گزینه بیشتر از آنچه که در زمان نصب به عنوان حداکثر داده اید وارد کنید");
define("_LOWNUMCH", "تعداد گزینه ها حداقل بایستی 2 باشد.");
define("_INVLDNUM", "شماره نادرست وارد شده است - خطر نفوذ!");
define("_INVLDDATE", "تاریخ وارد شده با قالب درست وارد نشده است!");
define("_ERRINTER", "خطا در اطلاعات وارد شده برای درج در بانک اطلاعاتی بوجود آمده است!");
define("_ENOUGHPERM", "شما دسترسی کافی برای انجام این عمل را ندارید!");
define("_INVALIDUSER", "کاربر غیرمجاز وارد شده است ! - خطر نفوذ!");
define("_EUSRUPDATE", "خطا : نمی توان اطلاعات کاربران را به روز کرد!");
define("_EUSRUPDATE", "خطا : نمی توان اطلاعات گروه های دسترسی را به روز کرد!");
define("_ENEWGROUP", "خطا : نمی توان گروه جدید درست کرد!");
define("_ENEWUSER", "خطا : نمی توان کاربر جدید درست کرد !");
define("_ACCOUNTBLOCKED", "اشتراک شما مسدود شده است! شما نمی توانید وارد شوید!");
define("_NOGROUP", "خطا : هیچ گروهی در بانک اطلاعاتی موجود نیست !");
define("_NOGROUPUSER", "خطا : هیچ گروه و یا کاربری در سیستم ثبت نشده است!");
define("_LANGDIRREAD", "خطا : پوشه ی زبان ها قابل خواندن نیست !");
define("_TEMPDIRREAD", "خطا : پوشه ی قالب ها قابل خواندن نیست !");

// voters section errors 
define("_INVALIDVERIFICATIONLINK", "لینک تایید نادرست است !");
define("_SUCCVERIFIEDADMINWAIT", "اشتراک شما با موفقیت تایید گردید و شما باید منتظر تایید مدیر(ان) باشید تا اشتراک شما را فعال کنند.");
define("_SUCCVERIFIEDCOMPLETE", "اشتراک شما با موفقیت فعال گردید و هم اکنون می‌توانید وارد سیستم شوید.");
define("_USERNAMENOTVALID", "فرمت نام‌کاربری (ایمیل) شما نادرست است.");
define("_CAPTCHANOTCORRECT", "کد تایید عکسی وارد شده نادرست است !");
define("_USERNAMEUSEDBEFORE", "باعرض پوزش این نام‌کاربری (ایمیل) قبلاً مورد استفاده قرار گرفته است.");
define("_CANTADDVOTERACCOUNT", "نمی‌توان اشتراک رای‌دهنده جدید درست کرد !");
define("_VOTERNOTVERIFIED", "اشتراک شما هنوز بوسیله‌ی خودتان تایید نشده است. لطفا ایمیل خود را باز کرده و لینک تایید را کلیک کنید.");
define("_VOTERNOTENABLED", "اشتراک شما بوسیله‌ی مدیران هنوز فعال‌سازی نشده است. لطفا صبور باشید ...  ");
?>
