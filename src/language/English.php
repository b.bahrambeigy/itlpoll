<?php
/* **********************************************************************
*************************************************************************
####    This App Is Written By : Bahrambeigy - ITLPoll Version 3    #####
########       [https://gitlab.com/b.bahrambeigy/itlpoll]        ########
####              This application is free of charge :)             #####
####                Contact me: bahramwhh@gmail.com                 #####
*************************************************************************
########################################################################
File : english.php
Writer : Bahrambeigy (bahramwhh@gmail.com)
Description : Default english translation for ITLPoll
**********************************************************************
*/

// General Strings
define("_SUBMIT","Vote It!");
define("_RESET","Clear Boxes");
define("_HTMLRESULTS","HTML Results");
define("_VOTED","Voted");
define("_IMGRESULTS", "Image Results");
define("_TOTALVOTED","Number Of voted peoples");
define("_THANKS", "Thank you for your vote(s)");
define("_SURVEY", "Survey Page [Powered by ITLPoll]");

// Voters Section (Version 3.2)
define("_SURVEYNEEDSLOGGING", "This Survey Needs logging in into your voting account.</b><br/> so, If you have an account please login, otherwise you can register in the site.");
define("_REGISTERVOTER", "Register New Voter Account");
define("_LOGINDETAILS", "Login Details");
define("_REGISTERETAILS", "Register Details");
define("_USERNAMEEMAIL", "User Name (email)");
define("_VOTERSPASSWORD", "Your Password");
define("_VOTERSPASSWORD2", "Repeat Your Password");
define("_IMAGEVERIFICATION", "Image Verification");
define("_VOTERSLOGINTITLE", "Survey Needs Logging In");
define("_REGISTEREXPLAIN", "Please fill below form to create new voter account");
define("_VOTERLOGGEDOUT", "You are now logged out.");
define("_VOTERSMANAGEMENT", "Voters Management");
define("_VOTERSACCOUNTINGDISABLED", "Voters accounting is disabled! ");
define("_ENABLEVOTERSACCOUNTING", "Enable Voters Accounting");
define("_ENABLEDISABLEVOTERMANAGEADMINCONFIRM", "Disable/Enable whole voter accounting or admin confirmation ");
define("_DISABLEVOTERSACCOUNTING", "Disable Voters Accounting");
define("_ENABLEADMINCONFRIMATION", "Enable Admin Confirmation");
define("_DISABLEADMINCONFRIMATION", "Disable Admin Confirmation");
define("_VOTERMGMSEARCHEXPLAIN", "Search a voter and edit or delete :");
define("_SEARCH", "Search");
define("_VOTERSMGMWAITINGEXPLAIN", "Voters waiting to be approved (Click on username(s) to confirm) :");
define("_ENABLED", "Enabled");
define("_VERIFIED", "Verified");
define("_LATESTPARTVOTERS", "Latest participated voters");
define("_TOTALVOTES", "Total Votes");
define("_LASTLOGINDATE", "Last Login Date");
define("_LASTVOTEDDATE", "Last Voted Date");
define("_LASTVOTEFROM", "Last Vote From");
define("_EDITVOTERPROFILE", "Edit profile of voter account: ");
define("_REMOVEACCOUNT", "Remove Account");
define("_UPDATEACCOUNT", "Update Account");
define("_VOTERMGMENABLEDSUCC", "Voters accounting management enabled successfully !");
define("_SELCECTEDVOTERENABLEDSUCC", "Selected voter has been enabled successully.");
define("_VOTERMGMDISABLEDSUCC", "Voters accounting management disabled successfully !");
define("_VOTERADMINCONFENABLEDSUCC", "Admin Confirmation has been enabled for voters successully.");
define("_VOTERADMINCONFDISABLEDSUCC", "Admin Confirmation has been disabled for voters successully.");
define("_PLZPROVIDEUSERNAME", "Please provide username for search !");
define("_NOSUCHVOTERUSER", "Sorry, There is no such voter user !");
define("_VOTERACCDELETEDSUCC", "Voter account successfully deleted !");
define("_VOTERPROFILEUPDATEDSUCC", "Voter profile information updated successfully.");
define("_VOTERNOWAITINGUSER", "There is no waiting voter ;)");
define("_VOTERTHEREISNOVOTEATALL", "There is no votes at all !");
define("_THANKSVOTERREGISTER", "Thanks. an email has been sent to your email your should click containing link to verify your email.");

define("_GENERATIONTIME", "Generation time : ");
define("_SECONDS", "(seconds)");
define("_CANTEMBEDITLPOLL", "You Can't embed ITLPoll when more than one poll is actived or voters login is enabled !");


// Administrator Languages
define("_UNAME","UserName");
define("_PASS","PassWord");
define("_POLLQ","Poll Question");
define("_CHOICES","Choices");
define("_ACTIVE","Active");
define("_DELETE","Delete It");
define("_CHANGE","Change This Poll");
define("_EXPIERE","Exp-Date");
define("_ADMIN","ITLPoll Administrator");
define("_BACKMAIN","Back To Main Page");
define("_LOGIN","Login");
define("_CLICKTOEDIT","Click here to edit poll number %s");
define("_WELCOME","Welcome to ITLPoll administrator");
define("_ADDPOLL","Add A New Poll");
define("_EDITPOLL","Edit or Delete Existing Polls");
define("_EDITCONFIGS","Edit Configurations and Users & Groups");
define("_LOGOUT","Logout");
define("_ADDNOTICE","Notice : When you add a new poll it doesn't show in user page (front end) until you activiate it on editing page");
define("_EDITNOTICE","To edit polls click on the icon of each poll under Number column.");
define("_DELETENOTICE","To delete polls click delete icon in each poll row, under Delete column. Please note that deleted polls can't be restore.");
define("_CHOICENUM","Enter how many choices do you want to set?");
define("_EDITINGPOLL","Editing the poll");
define("_CONTINUE","Countinue");
define("_ENTERUQH","Enter your question here");
define("_ENTERCHOICES","Enter your choices here");
define("_JALALINOTE", "Enter dates only in Jalali (Hijri) date format");
define("_DATEEXPLAIN","Only With YYYY-MM-DD Format");
define("_ENTRSTRDATE", "Enter start date");
define("_STARTDATE", "Start Date");
define("_STARTDATEEXP", "Leave blank if you don't want to start in a specific date.");
define("_NUMBER","Number");
define("_YES","Yes");
define("_NO","No");
define("_PAGES", "Pages");
define("_NEXT", "Next Page");
define("_PREV", "Previous Page");
define("_ADMINCONFS","Your Account & System Configurations");
define("_ADMINNAME","Account Username");
define("_CHADMINPASS","Account Password");
define("_PASSNOTICE","FOR ANY CHANGE YOU SHOULD ENTER YOUR PASSWORD");
define("_CHPASS","Change Password");
define("_DISABLED","Disable The Poll?");
define("_LANG","ITLPoll Default Language");
define("_TEMPLATE","Default Template");
define("_SAVE","Save Changes");
define("_OLDPASS","Old Password");
define("_NEWPASS","New Password");
define("_NEWPASSAGAIN","New Password(Again)");
define("_DEFRESULT", "Default Results");
define("_DEFGRAPH", "Default Graph ");
define("_COLOR1", "First Color");
define("_COLOR2", "Second Color");
define("_COLORSEL", "Colors Help");
define("_EMAIL", "Email");
define("_NUMARCHIVE", "Number for listing (archives and pages)");
define("_ENTERADMINPASS", "For any Change in Configuration You Should Enter Password For More Security");
define("_POLLARCHIVE", "Poll Archives");
define("_CONFIRMDEL","Are You Sure To Delete This Poll?");
define("_LOGGEDOUT","You have been successfully logged out from administrator");
define("_ENTEREXPIRE","Enter The Expire Poll Date(Blank=Never Expire)");
define("_ADDSUCC","New poll has been added successfully into database");
define("_PASSCHSUCC","Your password has been changed successfully!");
define("_CONFCHSUCC","Your configurations has been saved!");
define("_DELSUCC","Your poll has been deleted successfully!");
define("_ACTSUCC","Your poll has been Activiated/Deactiviated successfully!");
define("_FORGET", "Administrator Password Reset");
define("_RESETPASS", "Reset Password");
define("_FORGETPASSWD", "Did you forget your password?");
define("_MAILSENT", "Password reset mail has been sent to administrator email!");
define("_PASSRESETED", "Your account password has been changed successfully!");
define("_POLLCHSUCC","Your poll changed successfully");
define("_RIGHTS","All Rights Reserved To <a href=\"https://gitlab.com/b.bahrambeigy/itlpoll\">ITLPoll System</a> - Programing: <a href=\"mailto:bahramwhh@gmail.com\">Bahrambeigy</a>");
define("_GROUPDUPATED", "Groups Permissions Updated !");
define("_USERUPDATED", "Users Information Updated !");
define("_GROUPCREATED", "Group Created Successfully !");
define("_USERCREATED", "New User Created Successfully !");
define("_MANAGEGROUP", "Manage and Create Permission Groups");
define("_NAME", "Name");
define("_CANADD", "Can Add");
define("_CANEDIT", "Can Edit");
define("_CANDELETE", "Can Delete");
define("_CANCONFIG", "Can Config");
define("_CANGROUP", "Can Group or User");
define("_UPDATE", "Update");
define("_CREATEGROUP", "Create New Group");
define("_MANAGEUSERS", "Manage and Create Users");
define("_PERMISSION", "Permission");
define("_BLOCK", "Block");
define("_CREATEUSER", "Create New User");
define("_CREATE", "Create");
define("_GROUPDELETED", "Group Deleted Successfully !");
define("_USERDELETED", "User Deleted Successfully !");
define("_DELETENOTE", "Please note that delete will act with NO CONFIRMATION and deleted items can't be restore!");
define("_USERWITHGROUP", "There is user(s) selected in this group as permission group. so you must delete them before doing this!");
define("_CHANGEDLATESTPOLL", "You participated at least in one poll in the past, but there is new one and you can participate again here ! ");
define("_ENABLEMULTIPLE", "Enable Multiple Selection ?");
define("_ENABLEMULTIPLESHORT", "Multiple?");
define("_SHOWACTIVEANDCONFIRMED", "Show Only Actived and Confirmed Polls");


// Version 3.1
define("_CLICKTOACTIVATE", "Click Here to Activate This Poll");
define("_CLICKTODEACTIVATE", "Click Here to DeActivate This Poll");
define("_TIP", "Tip");
define("_ACTIVED", "Actived");
define("_NOTACTIVED", "Not Actived");
define("_CONFIRMED", "Confirmed");
define("_NOTCONFIRMED", "Not Confirmed");
define("_SAVEACTIVEMSG1", "There is changes in actived polls. you can change title message of front-end here.");
define("_SAVEACTIVEMSG2", "By Pressing \"Save Changes\" ALL STATICAL NUMBERS OF ALL ACTIVE POLLS WILL RESET TO ZERO (0)");
define("_SAVEACTIVEMSG3", "OR you can simply press \"Discard Changes\".");
define("_SURVEYTITLE", "Survey Title Message:");
define("_SAVECHANGES", "Save Changes");
define("_DISCARDCHANGES", "Discard Changes");
define("_DISCARDEDCHANGES", "All Changes Discarded !");
define("_SAVEDCHANGES", "All Changes have been saved and NUMBERES RESETED");
define("_CONFIRMSAVECHANGES", "Are you sure to save changes? ALL STATICAL NUMBERS OF ACTIVE POLLS WILL RESET TO ZERO(0) !");
define("_HAVELATESTVERSION", "You have latest version of ITLPoll");
define("_CANTCHECKLATESTVERSION", "Error ! Can't check the latest version from ITLPoll website");
define("_NEWVERSIONAVAILABLE", "ITLPoll new version is available. Please download it <a target='blank' href='http://www.itlpoll.com/'>HERE</a>");

// version 3.2
define("_BACKTOUSERSFRONTEND", "Back to Users Front-End");
define("_SHOWFRONTEND", "Show Front-End");


// Errors
define("_ECOOKIES","Your browser cookies are disabled");
define("_EPOLLED","You have voted before and you cannot vote again");
define("_CANTDEL","Sorry, you can't delete active poll. Please deactivate this poll and try again");
define("_ESERVER","Sorry, We have a server problem please try again later");
define("_EXPIERED","Sorry, Active poll or one of active polls have been EXPIRED - Contact Website Administrator"); // 3.1 changed
define("_NOTSTARTED", "Sorry, Active poll or one of active polls not started yet ! - Contact Website Administrator"); // 3.1 changed
define("_WRONGUOP","Your UserName or PassWord isn't correct");
define("_BRUTEFORCEREACHED", "Sorry, you have reached 3 times to enter username & password and you will not be able to enter again for 10minutes."); // 3.1 changed
define("_LOGGED","You are logged in now");
define("_NOTEXIST","Error : This file doesn't exist!");
define("_EMYSQL","Sorry, Script can't connect to database, Please check your config.php file");
define("_DISABLEDPOLL","Sorry, This poll script had beed disbaled by administrator");
define("_YVOTED","Sorry You have voted before!");
define("_NOROWS","Sorry, There is no row with this number in database");
define("_UPERROR","Your Username or Password isn't correct");
define("_CHERROR","Your Didn't type any choices or your choice box is empty");
define("_PASSNOTMATCH","Sorry, Your password doesn't match the exist password");
define("_PASSERRORM","Your password doesn't match the exist password");
define("_PASSERRORA","Your first password and second password(Again) don't match ");
define("_NOEMAIL", "The email that you have entered doesn't match with your email");
define("_NOUSER", "This username that you entered doesn't match with your username account");
define("_ERRORMAIL", "Error in server to send mail - Please try again later!");
define("_ERRORRESET", "Server error in reseting your password. This error may be in mysql database.");
define("_INVCODE", "Invalid code !");
define("_DNTREMOVEINST","You didn't remove install folder in your ITLPoll folder. It's dangerous! - please remove install folder now!");
define("_NOTVALIDEMAIL", "Your email address isn't valid. Please go back and check it!");
define("_NOACTPOLL", "A serious problem! There isnnot any actived poll. So the system will activate first poll automatically");
define("_BIGMAXCH", "You can't insert choices bigger than maximum choices which has been set in the installing form!");
define("_LOWNUMCH", "The choice numbers should be at least 2!");
define("_INVLDNUM", "Invalid number entered! - hacking attempt");
define("_INVLDDATE", "Entered date isn't in right format!");
define("_ERRINTER", "Error in entered information for database");
define("_ENOUGHPERM", "You don't have enough permission to do this action ! ");
define("_INVALIDUSER", "Invalid user logged in ! hacking attempt !");
define("_EUSRUPDATE", "Error! Can't update users information! ");
define("_ENEWGROUP", "Error! Can't create new group!");
define("_ENEWUSER", "Error! Can't create new user!");
define("_ACCOUNTBLOCKED", "Your acoount had been blocked by administrator! You can't log in !");
define("_NOGROUP", "Error ! There is no group in database !");
define("_NOGROUPUSER", "Error ! There is no group or user in database !");
define("_LANGDIRREAD", "Error : Can't read languages directory !");
define("_TEMPDIRREAD", "Error : Can't read templates directory!");

// voters section errors 
define("_INVALIDVERIFICATIONLINK", "Invalid Verification Link !");
define("_SUCCVERIFIEDADMINWAIT", "Your account successfully verified and you must wait until survey administrator(s) enable your account");
define("_SUCCVERIFIEDCOMPLETE", "Your Account successfully verified and you can login to system now");
define("_USERNAMENOTVALID", "Your username (Email) format is not valid");
define("_CAPTCHANOTCORRECT", "Entered Captcha code isn't correct!");
define("_USERNAMEUSEDBEFORE", "Sorry, This email (username) had been used before!");
define("_CANTADDVOTERACCOUNT", "Can't add new voter account !");
define("_VOTERNOTVERIFIED", "Your account is not verified by yourself. please see your email and click verification link.");
define("_VOTERNOTENABLED", "Your account is not enabled by administrator(s) yet. please be patient ... ");
?>
