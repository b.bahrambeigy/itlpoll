<?php
/* **********************************************************************
*************************************************************************
####    This App Is Written By : Bahrambeigy - ITLPoll Version 3    #####
########       [https://gitlab.com/b.bahrambeigy/itlpoll]        ########
####              This application is free of charge :)             #####
####                Contact me: bahramwhh@gmail.com                 #####
*************************************************************************
########################################################################
File : itlpoll_include.php
Writer : Bahrambeigy (bahramwhh@gmail.com)
Description : Using this file one can show ITLPoll on another home page
without any IFrame or Frame. for more information Please see the 
help documents in : /documents
**********************************************************************
*/

@error_reporting (E_ERROR | E_PARSE);

if(isset($_GET['js']))
{
    $width = "450";
    if(isset($_GET['width']))
	$width = $_GET['width'];
    $height = "350";
    if(isset($_GET['height']))
	$height = $_GET['height'];
    $scrollbars = "no";
    if(isset($_GET['scrollbars']))
	if($_GET['scrollbars'] == "yes")
	    $scrollbars = "yes";
    $script_path = $_SERVER['SCRIPT_NAME'];
	
    echo "document.write('<p><iframe allowtransparency=\"true\" name=\"ITLPoll_frame\" src=\"$script_path\" marginwidth=\"0\" marginheight=\"0\" height=\"$height\" width=\"$width\" border=\"0\" frameborder=\"0\" scrolling=\"$scrollbars\"></iframe></p>')";
    @exit(1);
}

if(empty($itlpoll_path))
{
    $itlpoll_path = ".";
    if(isset($_GET['itlpoll_path']))
	$itlpoll_path = $_GET['itlpoll_path'];
}

if(empty($itlpoll_path)) {
  echo "You Can't Access This File Without A ITLPoll Path!";
  @exit();
}
if( !empty($_REQUEST['itlpoll_path']) )
{
  echo "Illegal Operation out of ITLPoll Path ! - Operation Failed!";
  @exit();
}

// Installation Check !
if(@filesize("$itlpoll_path/config.php") == 0)
{
  header("Location: $itlpoll_path/install/index.php");
  @exit();
}
// Check Is There Any Config.php On Our Server!
if(!filesize("$itlpoll_path/config.php")) {
  echo "You Can't Access To A Config.php On A Forigen Server! - Hacking Attempt!";
  @exit();
}
require_once("$itlpoll_path/config.php");

if(!defined("ITLPoll_INSTALLED"))
{
  header("Location: $itlpoll_path/install/index.php");
  @exit();
}

// Functions Existance Check
if(!(file_exists("$itlpoll_path/includes/functions.php")))
{
    echo "Your Functions.php File Doesn't Exist in : .$itlpoll_path/includes/functions.php";
    @exit();
}
require_once("$itlpoll_path/includes/functions.php");
$connected_db = db_connect($host, $user, $passwd, $database);
Load_Configs($prefix);

// Language File Existance Check
file_check("$itlpoll_path/language/".$configs['language'].".php", "Error : Your Default Laguage File Doesn't Exist");
require_once("$itlpoll_path/language/".$configs['language'].".php");

// Check first then Open Theme File And Use it!
file_check("$itlpoll_path/templates/".$configs['template']."/user.xml", "Error : Your Default Template File(user.xml) Doesn't Exist");

$itl_fxml = file_get_contents("$itlpoll_path/templates/".$configs['template']."/user.xml");


if($configs['disabled'] == "yes")
{
  echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
	  <b> ". _DISABLEDPOLL ."</b>";
  @exit();
}


$query = "SELECT * FROM ".$prefix."_poll WHERE (active='yes' AND confirmed='yes') OR (active='no' AND confirmed='yes');";
$poll = mysqli_query($GLOBALS["___mysqli_ston"], $query);
$active_polls = mysqli_num_rows($poll);

// We don't let embed ITLPoll with these conditions ;)
if(($active_polls > 1) || ($configs['voters_login'] == "yes"))
{
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
	  <b> ". _CANTEMBEDITLPOLL ."</b>";	
	@exit();
}


@$id = filter($_REQUEST['pollID']);

if(empty($id)) $query = "SELECT * FROM ".$prefix."_poll WHERE (active='yes' AND confirmed='yes') OR (active='no' AND confirmed='yes');";
else {
    // Checking $id For Injection (Bug Founded By Simorgh-ev Security Group)
    if(!eregi("[1-9]", $id)) {
    echo "Hacking Attempt - You Can't Set non Numeric Value For ID Variable!";
    @exit();
    }
    else {
    $query = "SELECT * FROM ".$prefix."_poll WHERE id = '". $id ."'";
     }
}
$poll = mysqli_query($GLOBALS["___mysqli_ston"], $query);
$active_polls = mysqli_num_rows($poll);

// We don't let embed ITLPoll with these conditions ;)
if(($active_polls > 1) || ($configs['voters_login'] == "yes"))
{
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
	  <b> ". _CANTEMBEDITLPOLL ."</b>";	
	@exit();
}


// Check Cookies Enabled ?!
if(isset($_REQUEST['ITLPollCheckCookie']))
{
	if( !isset($_COOKIE['ITLPoll_Voted']) )
    {
      setcookie("ITLPoll_Voted", "");
      msg(_ECOOKIES, "?");
    }
    else
    {
      if( isset($_COOKIE['ITLPoll_Valid_Post']) AND ($_COOKIE['ITLPoll_Valid_Post'] == "yes") )
      {
      	$ch = filter($_REQUEST['Choice']);
      	$ch = explode(":", $ch);
      	
      	$error_detected = false;
      	
      	// define arrays
      	$update = array();
      	$id_set = array();
      	
      	$real_numbers_counter = 0;
      	$counter = 0;
      	
      	// check created choice selected not created by malicious user ! ;)
      	if(isset($_SESSION['CreatedChoiceStr']) && ( (filter($_REQUEST['Choice']) != $_SESSION['CreatedChoiceStr']) ) )
      		$error_detected = true;
      	
      	
      	for($counter=0; ($counter < $active_polls) && !$error_detected; $counter++)
		{
			$situation = explode(",", $ch[$counter]);
			$is_multiple = $situation[0];
	      	
			
	      	// Check for Multiple Choices Enabled
	      	if($is_multiple == "s")
	      	{
	      		$ch_expld = explode("_", $situation[1]);
	      		
	      		$id = $ch_expld[0];
	      		if($id > 0);
	      		else
	      			$error_detected = true;
	      		
	      		
	      		$update[$counter] = "UPDATE ".$prefix."_results SET $ch_expld[1]=$ch_expld[1]+1 WHERE id=$id";
	      		$id_set[$counter] = $id;
	      		
	      		$real_numbers_counter++;
	      		
	      	}
	      	else if($is_multiple == "m")
	      	{
	      		$ch_expld = explode("|", $situation[1]);
	      		$s_str = "";
	      
	      		for($i=1; $i <= $ch_expld[0]; $i++)
	      		{
	      			$ch_explded = explode("_", $ch_expld[$i]);
	      			
	      			$id = $ch_explded[0];
	      			$ch_num = $ch_explded[1];
			  		$s_str .= ($i == $ch_expld[0]) ? $ch_num ."=" . $ch_num . "+1" : $ch_num ."=" . $ch_num . "+1, ";
	      			
	      		}
	      		if($ch_expld[0] > 0)
	      		{
	      			$update[$counter] = "UPDATE ".$prefix."_results SET ". $s_str ." WHERE id='".$id."'";
	      			//$mquery5 = mysql_query($query5);
	      		}
	      		
	      		else if($ch_expld[0] == 0)
	      		{
	      			$id = $ch_expld[1];
	      			$update[$counter] = "empty";
	      		}
	      		else
	      			$error_detected = true;
	      			
	      		$id_set[$counter] = $id;
	      		$real_numbers_counter++;	
	      		
	      		//$query6 = "UPDATE ".$prefix."_results SET uniques=uniques+1 WHERE id = '".$id."'";
	      		//$mquery6 = mysql_query($query6);
	      		
	      	}
	         
		}
		
      	if( !$error_detected && ($real_numbers_counter == $active_polls))
		{
			$query_update_uniques = "UPDATE ".$prefix."_results SET uniques=uniques+1 WHERE (";
			for($counter = 0; ($counter < $active_polls) && !$error_detected; $counter++)
			{
				if($update[$counter] == "empty")
					$query_update_uniques .= "id='".$id_set[$counter]."' " . (($counter == ($active_polls-1)) ? " )" : " OR ");
				else
				{	
					$mquery_run = mysqli_query($GLOBALS["___mysqli_ston"], $update[$counter]);
					if(!$mquery_run)
						$error_detected = true;
					else
						$query_update_uniques .= "id='".$id_set[$counter]."' " . (($counter == ($active_polls-1)) ? " )" : " OR ");	
				}
			}
			if(!$error_detected)
			{
				$mqeury_uniques = mysqli_query($GLOBALS["___mysqli_ston"], $query_update_uniques);
				if(!$mqeury_uniques)
					$error_detected = true;
			}
		}
		else
			$error_detected = true;
		
		
		if(!$error_detected)
		{
			setcookie("ITLPoll_Valid_Post", "no", time()+15552000);
			$update_total_votes = "UPDATE ".$prefix."_config SET total_votes=total_votes+1;";
      		$update_exec = mysqli_query($GLOBALS["___mysqli_ston"], $update_total_votes);
      		if($update_exec)
      			msg(_THANKS, "?");
      		else 
      			echo '<script>window.location="?"</script>';
		}
		else
		{
			// I should rollback changes made by this malicious choice selection !
			// not available yet !! (needs InnoDB engine)
			msg('Malicious choice selection detected!', "?");
		}
		
      }
      else 
     	echo '<script>window.location="?"</script>';
      	
    } 
    @exit();  
}


if( !empty($id) )
{
	$choice_totals = 0;
	for($counter=0; $counter < $active_polls; $counter++)
	{

		$polllist = mysqli_fetch_array($poll);
	
		$id = $polllist['id'];
		$title = $polllist['subject'];
		$choices = $polllist['choices'];
		$multiple = $polllist['multiple'];
		$active = $polllist['active'];
		$expire = $polllist['expire'];
		$strdate = $polllist['start'];
		$crtdate = $polllist['created'];
		
		// Jalali date format check	
		if($usejalali == "1")
		{
			if($crtdate != "9999-12-31")
			{
				$create_date = explode("-", $crtdate);
				$created = ConvDate($create_date, "H");
				$crtdate = implode("-", $created);
			}
		}

		// Query The Results From Database
	    $query2 = "SELECT * FROM ".$prefix."_results WHERE id = $id";
	    
	    // Gathering uniques voters
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	    $choice_total = mysqli_fetch_array($result);
	    $choice_totals += $choice_total['uniques'];
	    
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	    $choice = mysqli_fetch_row($result);
	    // Optimize the database
	    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
	    // Query The Choices From Database
	    $query3 = "select * from ".$prefix."_choices where id = $id";
	    $cho = mysqli_query($GLOBALS["___mysqli_ston"], $query3);
	    $choi = mysqli_fetch_row($cho);
	    
	    // Optimize the database
	    ((mysqli_free_result($cho) || (is_object($cho) && (get_class($cho) == "mysqli_result"))) ? true : false);
	    
	    $sid = $counter . "|" . $id . "|"  .  $active_polls . "|" . $configs['total_votes'];
	    
	    // Finally Shows The Results Page!
		if($configs['defaultresult'] == "html") {
			$out .= Show_Results($crtdate, $choices, $title, $itl_fxml, $choice, $choi, $configs['color1'], $configs['color2'], $sid);
		}
		elseif($configs['defaultresult'] == "image") {
			
			if($configs['defaultgraph'] == "bar")
				$out .= Show_ImageResults($id, $crtdate, $choices, $title, $itl_fxml, $choice, $choi, $itlpoll_path, "bar", $sid);
			else if($configs['defaultgraph'] == "pie")
				$out .= Show_ImageResults($id, $crtdate, $choices, $title, $itl_fxml, $choice, $choi, $itlpoll_path, "pie", $sid);
			else
				$out .= Show_ImageResults($id, $crtdate, $choices, $title, $itl_fxml, $choice, $choi, $itlpoll_path, "simple", $sid);
		}
		
	}  
}

@$action = filter($_REQUEST['action']);

// This Will Show The Archive Of Polls - Added On Version 2.00
if(isset($_REQUEST['Archive']) || ($action == "Archive") ) {
    
    // Query The Polls From Database
    $query = "SELECT id, subject FROM ".$prefix."_poll ORDER BY id DESC";
    $archive = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    
	// Shows The Archives Page!
    $out = Show_Archives($itl_fxml, $archive, $configs['numarchive'], $configs['color1'], $configs['color2'], $itlpoll_path);
}

elseif(isset($_REQUEST['HtmlResults']) || ($action == "HtmlResults") ) {
	
	$choice_totals = 0;
	for($counter=0; $counter < $active_polls; $counter++)
	{

		$polllist = mysqli_fetch_array($poll);
	
		$id = $polllist['id'];
		$title = $polllist['subject'];
		$choices = $polllist['choices'];
		$multiple = $polllist['multiple'];
		$active = $polllist['active'];
		$expire = $polllist['expire'];
		$strdate = $polllist['start'];
		$crtdate = $polllist['created'];
		
		
		if($usejalali == "1")
		{
			if($crtdate != "9999-12-31")
			{
				$create_date = explode("-", $crtdate);
				$created = ConvDate($create_date, "H");
				$crtdate = implode("-", $created);
			}
		}
		
		
		// Query The Results From Database
	    $query2 = "SELECT * FROM ".$prefix."_results WHERE id = $id";
	    
	    // Gathering uniques voters
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	    $choice_total = mysqli_fetch_array($result);
	    $choice_totals += $choice_total['uniques'];
	    
	    
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	    $choice = mysqli_fetch_row($result);
	    
	    // Optimize the database
	    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
	    // Query The Choices From Database
	    $query3 = "select * from ".$prefix."_choices where id = $id";
	    $cho = mysqli_query($GLOBALS["___mysqli_ston"], $query3);
	    $choi = mysqli_fetch_row($cho);
	    // Optimize the database
	    ((mysqli_free_result($cho) || (is_object($cho) && (get_class($cho) == "mysqli_result"))) ? true : false);
	    
	    $id = $counter . "|" . $id . "|"  .  $active_polls . "|" . $configs['total_votes'];
	    
		// Finally Shows The Results Page!
		$out .= Show_Results($crtdate, $choices, $title, $itl_fxml, $choice, $choi, $configs['color1'], $configs['color2'], $id, $itlpoll_path);
	}
}

elseif(isset($_REQUEST['ImageResults']) || ($action == "ImageResults") ) {
	
    $choice_totals = 0;
	for($counter=0; $counter < $active_polls; $counter++)
	{

		$polllist = mysqli_fetch_array($poll);
	
		$id = $polllist['id'];
		$title = $polllist['subject'];
		$choices = $polllist['choices'];
		$multiple = $polllist['multiple'];
		$active = $polllist['active'];
		$expire = $polllist['expire'];
		$strdate = $polllist['start'];
		$crtdate = $polllist['created'];
		
		
		if($usejalali == "1")
		{
			if($crtdate != "9999-12-31")
			{
				$create_date = explode("-", $crtdate);
				$created = ConvDate($create_date, "H");
				$crtdate = implode("-", $created);
			}
		}
	
		// Query The Results From Database
	    $query2 = "SELECT * FROM ".$prefix."_results WHERE id = $id";
	    
	    // Gathering uniques voters
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	    $choice_total = mysqli_fetch_array($result);
	    $choice_totals += $choice_total['uniques'];
	    
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	    $choice = mysqli_fetch_row($result);
	    // Optimize the database
	    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
	    // Query The Choices From Database
	    $query3 = "select * from ".$prefix."_choices where id = $id";
	    $cho = mysqli_query($GLOBALS["___mysqli_ston"], $query3);
	    $choi = mysqli_fetch_row($cho);
	    // Optimize the database
	    ((mysqli_free_result($cho) || (is_object($cho) && (get_class($cho) == "mysqli_result"))) ? true : false);
	    
	    $sid = $counter . "|" . $id . "|"  .  $active_polls . "|" . $configs['total_votes'];
	    
	    if($configs['defaultgraph'] == "bar")
			$out .= Show_ImageResults($id, $crtdate, $choices, $title, $itl_fxml, $choice, $choi, $itlpoll_path, "bar", $sid);
		else if($configs['defaultgraph'] == "pie")
			$out .= Show_ImageResults($id, $crtdate, $choices, $title, $itl_fxml, $choice, $choi, $itlpoll_path, "pie", $sid);
		else
			$out .= Show_ImageResults($id, $crtdate, $choices, $title, $itl_fxml, $choice, $choi, $itlpoll_path, "simple", $sid);
	}
}

elseif(isset($_COOKIE['ITLPoll_Voted']))
{
	$active_poll_date = "SELECT multiactivedate FROM ".$prefix."_config";
	$active_poll_date_query = mysqli_query($GLOBALS["___mysqli_ston"], $active_poll_date);
	$active_poll_date_query = mysqli_fetch_array($active_poll_date_query);
	$active_poll_date_str = $active_poll_date_query['multiactivedate'];
	
	 
	if( Changed_Last_Poll($active_poll_date_str) )
	{
		setcookie("ITLPoll_Voted", "");
    	msg(_CHANGEDLATESTPOLL, "?");
    	@exit();
	}
	 
	// reactive cookie !
	setcookie("ITLPoll_Voted", $active_poll_date_str, time()+15552000);
	 
	 $choice_totals = 0;
	 for($counter=0; $counter < $active_polls; $counter++)
	 {

		$polllist = mysqli_fetch_array($poll);
	
		$id = $polllist['id'];
		$title = $polllist['subject'];
		$choices = $polllist['choices'];
		$multiple = $polllist['multiple'];
		$active = $polllist['active'];
		$expire = $polllist['expire'];
		$strdate = $polllist['start'];
		$crtdate = $polllist['created'];
		
		
		if($usejalali == "1")
		{
			if($crtdate != "9999-12-31")
			{
				$create_date = explode("-", $crtdate);
				$created = ConvDate($create_date, "H");
				$crtdate = implode("-", $created);
			}
		}
		
	
		// Query The Results From Database
	    $query2 = "SELECT * FROM ".$prefix."_results WHERE id = $id";
	    
	     // Gathering uniques voters
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	    $choice_total = mysqli_fetch_array($result);
	    $choice_totals += $choice_total['uniques'];
	    
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	    $choice = mysqli_fetch_row($result);
	    // Optimize the database
	    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
	    // Query The Choices From Database
	    $query3 = "select * from ".$prefix."_choices where id = $id";
	    $cho = mysqli_query($GLOBALS["___mysqli_ston"], $query3);
	    $choi = mysqli_fetch_row($cho);
	    // Optimize the database
	    ((mysqli_free_result($cho) || (is_object($cho) && (get_class($cho) == "mysqli_result"))) ? true : false);
	    
	    $sid = $counter . "|" . $id . "|"  .  $active_polls . "|" . $configs['total_votes'];
	    
	    // Finally Shows The Results Page!
	    $out .= Show_Results($crtdate, $choices, $title, $itl_fxml, $choice, $choi, $configs['color1'], $configs['color2'], $sid, $itlpoll_path);
	}
}

else
{
    $query = "SELECT * FROM ".$prefix."_poll WHERE active = 'yes'";
	$mquery = mysqli_query($GLOBALS["___mysqli_ston"], $query);
	
	if( isset($_REQUEST['Post']) )
	{
		$script_str_js = '<script>window.location="?ITLPollCheckCookie&Choice='; //
		$script_str = ""; 
		for($counter_s=0, $counter_m; ($counter_s + $counter_m) < $active_polls;)
		{
	
			$polllist = mysqli_fetch_array($mquery);
	
			$id = $polllist['id'];
			$multiple = $polllist['multiple'];
			$choices = $polllist['choices'];
	
			$choice_str = $id . "_Choice";
			if($multiple == "no")
            {
            	@$ch_[$counter_s] = filter($_POST[$choice_str]);
                if(empty($ch_[$counter_s]))
                	$ch_[$counter_s] = $id . "_ch1";
            }
            elseif($multiple == "yes")
            {
            	$ch = array();
                for ($i=0; $i < $choices; $i++)
                {
                	$ch_num = $i + 1;
                	$textbox = $id . "_ch". $ch_num;
                	@$ch[$i][$counter_m] = filter($_POST[$textbox]);
                }
            }
		    
            if($multiple == "yes")
            {
            	$changed_choices = 0;
                $choices_queue = "";
                for($i=0; $i < $choices; $i++)
                {
                	if(!empty($ch[$i][$counter_m]))
                    {
                    $choices_queue .= $ch[$i][$counter_m] . "|";
                    $changed_choices++;
                	}
            	}
            	if($changed_choices > 0)
            		$choices_queue = 'm,' . $changed_choices . "|" . $choices_queue . ":";
            	else
            		$choices_queue = 'm,' . $changed_choices . "|" . $id . ":";
            	$script_str .= $choices_queue;
            	$counter_m++;
        	}
        	elseif($multiple == "no")
        	{
        		$script_str .= 's,' . $ch_[$counter_s] . ':';
        		$counter_s++;
        	}
		      
		}
		$script_str_js .= $script_str;
		$script_str_js .= "\"</script>";
		
		$active_poll_date = "SELECT multiactivedate FROM ".$prefix."_config";
		$active_poll_date_query = mysqli_query($GLOBALS["___mysqli_ston"], $active_poll_date);
		$active_poll_date_query = mysqli_fetch_array($active_poll_date_query);
		$active_poll_date_str = $active_poll_date_query['multiactivedate'];
		
		// Will be valid for ever ( it's enough big ! ) 
	    setcookie("ITLPoll_Voted", $active_poll_date_str, time()+15552000);
	    setcookie("ITLPoll_Valid_Post", "yes", time()+15552000);
	    
	    $_SESSION['CreatedChoiceStr'] = $script_str;
	    
		echo $script_str_js;
		@exit();
	}	
	
	// In version 3.2 ITLPoll Include only allows showing 
    for($counter=0; $counter < $active_polls; $counter++)
    {
	    $polllist = mysqli_fetch_array($poll);
	    
	    $id = $polllist['id'];
	    
		$title = $polllist['subject'];
		$choices = $polllist['choices'];
		$multiple = $polllist['multiple'];
		$active = $polllist['active'];
		$expire = $polllist['expire'];
		$strdate = $polllist['start'];
		$crtdate = $polllist['created'];
		
	    // Start Date Check - Added in Version 2.7
		StartDate_Check($strdate);
		
		// Expiration Check - Completed in Version 2.6
		Expiration_Check($expire);
		
		if($usejalali == "1")
		{
			if($crtdate != "9999-12-31")
			{
				$create_date = explode("-", $crtdate);
				$created = ConvDate($create_date, "H");
				$crtdate = implode("-", $created);
			}
		}
	    
	    // If User Doen't Click Submit Show The Radio Buttons
	    $query3 = "select * from ".$prefix."_choices where id = $id";
	    $cho = mysqli_query($GLOBALS["___mysqli_ston"], $query3);
	    $choi = mysqli_fetch_row($cho);
	    // Optimize the database
	    ((mysqli_free_result($cho) || (is_object($cho) && (get_class($cho) == "mysqli_result"))) ? true : false);
	    $id = $counter . "|" . $id . "|"  .  $active_polls;
	    $out .= Show_Body($crtdate, $choices, $title, $itl_fxml, $choi, $configs['color1'], $configs['color2'], $multiple, $id, $itlpoll_path);
    }
}

if(!($echo_man))
{    
    $out = preg_replace("/<normal_heading>(.*)<\/normal_heading>/is", "", $out);
    echo $out;
}

((is_null($___mysqli_res = mysqli_close($connected_db))) ? false : $___mysqli_res);
?>