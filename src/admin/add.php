<?php
/* **********************************************************************
*************************************************************************
####    This App Is Written By : Bahrambeigy - ITLPoll Version 3    #####
########       [https://gitlab.com/b.bahrambeigy/itlpoll]        ########
####              This application is free of charge :)             #####
####                Contact me: bahramwhh@gmail.com                 #####
*************************************************************************
#########################################################################
File : add.php
Writer : Bahrambeigy (bahramwhh@gmail.com)
Description : This script is responsible for adding new polls into the database.
Notice : When you add a new poll it won't become active until manual
activation from edit polls page.
**********************************************************************
*/

session_start();
@error_reporting (E_ERROR | E_PARSE);

// Installation Check !
if(filesize("../config.php") == 0)
{
  header("Location: ../install/index.php");
}
require_once("../config.php");

if(!defined("ITLPoll_INSTALLED"))
{
  header("Location: ../install/index.php");
  @exit();
}

if(!(file_exists("../includes/functions.php")))
{
    echo "Your Functions.php File Doesn't Exist in : includes/functions.php";
    @exit();
}
require_once("../includes/functions.php");
$connected_db = db_connect($host, $user, $passwd, $database);
Load_Configs($prefix);

// Language File Existance Check
file_check("../language/".$configs['language'].".php", "Error : Your Default Laguage File Doesn't Exist");
require_once("../language/".$configs['language'].".php");

// Check first then Open Theme File And Use it!
file_check("../templates/".$configs['template']."/admin.xml", "Error : Your Default Template File(admin.xml) Doesn't Exist");

$itl_fxml = file_get_contents("../templates/".$configs['template']."/admin.xml");

$nowtime = date("H|i");

$header_html = Show_Admin_Header($itl_fxml);
$succ = "DShow";

if( (empty($_SESSION['ITLPoll']['Admin'])) || ( !Time_Valid($_SESSION['ITLPoll']['AdminTime'], $nowtime) ) )
{
    // This Will Empty ITLPoll Sessions For More Security
    if(isset($_SESSION['ITLPoll']['Admin'])) unset($_SESSION['ITLPoll']['Admin']);
    if(isset($_SESSION['ITLPoll']['AdminTime'])) unset($_SESSION['ITLPoll']['AdminTime']);
    session_destroy();

    // Now Shows The Login Page
    echo preg_replace("/<navigation_menu>(.*)<\/navigation_menu>/is", "", $header_html);
    Show_Admin_Login($itl_fxml, "add.php");
    Show_Admin_Footer($itl_fxml, $configs['version']);
    @exit();
}

// [Start] Session Fixation Prevention !
$admin_session = explode("|", $_SESSION['ITLPoll']['Admin']);
$username = base64_decode(filter($admin_session[0]));

$squery = "SELECT last_session FROM ".$prefix."_users WHERE username = '".$username."';";
$msquery = mysqli_query($GLOBALS["___mysqli_ston"], $squery);
$check_sess = mysqli_fetch_array($msquery);

if($admin_session[1] != $check_sess['last_session'])
{
	unset($_SESSION['ITLPoll']['Admin']);
	unset($_SESSION['ITLPoll']['AdminTime']);
	msg("Session Fixation Detected !", "?");
	@exit();
}
// [End] Session Fixation Prevention !

// Reactive Time Valid For Admin
$nowtime = date("H|i");
$increase_minute = 20;
$admintime = increase_admin_time($nowtime, $increase_minute);
$_SESSION['ITLPoll']['AdminTime'] = $admintime;

echo $header_html;

@$numchoices = filter(trim($_POST['ChoiceNum']));

@$type = $_REQUEST['type'];
if(empty($type))
{
    Show_Admin_Add($itl_fxml, $succ);
    Show_Admin_Footer($itl_fxml, $configs['version']);
    @exit();
}

if($type == "addpoll")
{
@$subject = filter(trim($_POST['Question']));
$expdate = array();
$strdate = array();
@$expdate['year'] = filter(trim($_POST['Expire_Year']));
@$expdate['month'] = filter(trim($_POST['Expire_Month']));
@$expdate['day'] = filter(trim($_POST['Expire_Day']));
@$strdate['year'] = filter(trim($_POST['Start_Year']));
@$strdate['month'] = filter(trim($_POST['Start_Month']));
@$strdate['day'] = filter(trim($_POST['Start_Day']));
@$numchoices = filter(trim($_POST['ChoiceNum']));  
$maxchoices = $configs['maxchoices'];

if(empty($numchoices))
{
    Show_Admin_Add($itl_fxml, $succ);
    Show_Admin_Footer($itl_fxml, $configs['version']);
    @exit();
}
else 
{
    if ( $numchoices > $maxchoices )
	{
		msg(_BIGMAXCH, "?");
		@exit();
	}
	elseif ($numchoices < 2)
	{
		msg(_LOWNUMCH, "?");
		@exit();
	}
	
	if(empty($subject)) {
    Show_Poll_Add($itl_fxml, $numchoices, $usejalali);
    Show_Admin_Footer($itl_fxml, $configs['version']);
    @exit();
	}
    else
    {
  	  	// Check Permission ( added on Version 3 )
		$admin_session = explode("|", $_SESSION['ITLPoll']['Admin']);
		$active_user = base64_decode(filter($admin_session[0]));
		$perm = Have_Permission("add", $active_user, $host, $user, $passwd, $database, $prefix);
		if(!$perm)
		{
			msg(_ENOUGHPERM, $_SERVER['PHP_SELF']);
            @exit();
		}
			
		@$subject = filter(trim($_POST['Question']));
  		@$active = "no";
  		@$multiple = filter(trim($_POST['multiple']));
		
		for ($i=0; $i < $numchoices; $i++)
		{
			$ch_num = $i + 1;
			$textbox = "ch". $ch_num;
			$ch[$i] = filter(trim($_POST[$textbox]));
		}
		
		$expdate_str = $expdate['year'] . "-" . $expdate['month'] . "-" . $expdate['day'];
		$strdate_str = $strdate['year'] . "-" . $strdate['month'] . "-" . $strdate['day'];
		
		
		if(empty($expdate['day']) || empty($expdate['month']) || empty($expdate['year']))
  			$expdate_str = "9999-12-31";
    	
    	if(empty($strdate['day']) || empty($strdate['month']) || empty($strdate['year']))
    		$strdate_str = "9999-12-31";

		if($usejalali == "1")
		{
			if($expdate_str != "9999-12-31")
			{
				$expdate_conv = explode("-", $expdate_str);
				$expd_conved = ConvDate($expdate_conv, "G");
				$expdate_str = implode("-", $expd_conved);
			}
			if($strdate_str != "9999-12-31")
			{
				$strdate_conv = explode("-", $strdate_str);
				$strd_conved = ConvDate($strdate_conv, "G");
				$strdate_str = implode("-", $strd_conved);
			}
		}
		
		$now_year = date('Y');
		$now_month = date('m');
		$now_day = date('d');
		
		$now_date = $now_year . "-" . $now_month . "-" . $now_day;
		
		$addpoll = "INSERT INTO ".$prefix."_poll
            (subject, choices, multiple, active, expire, start, created) VALUES
            ('".$subject."', '".$numchoices."', '".$multiple."', '".$active."', '".$expdate_str."', '".$strdate_str."', '".$now_date."')";

        if(mysqli_query($GLOBALS["___mysqli_ston"], $addpoll)) {}
        else
			msg(_ERRINTER, "?");
    	
		for($i=1; $i <= $numchoices; $i++)
			$s_str .= ($i == $numchoices) ? "ch". $i  : "ch". $i .", ";
			
		
		for($i=0; $i < $numchoices; $i++)
			$v_str .= ($i == ($numchoices - 1)) ? "'". $ch[$i] ."'" : "'". $ch[$i] ."', ";
			
		$num_str = "'0', ";  // You Can set it 1 !
		for($i=1; $i < $numchoices; $i++)
			$num_str .= ($i == ($numchoices - 1)) ? "'0'" : "'0', ";
		
		$addchoice = "INSERT INTO ".$prefix."_choices
            (" .$s_str .") VALUES
            (". $v_str .")";
    	
		if(mysqli_query($GLOBALS["___mysqli_ston"], $addchoice)) {}
        else
			msg(_ERRINTER, "?");
		
    	$addres = "INSERT INTO ".$prefix."_results
            (". $s_str .") VALUES
            (". $num_str .")";
    	
		if(mysqli_query($GLOBALS["___mysqli_ston"], $addres)) {}
        else
			msg(_ERRINTER, "?");
		
    	$succ = "Show";
    	Show_Admin_Add($itl_fxml, $succ);
    	Show_Admin_Footer($itl_fxml, $configs['version']);
    	@exit();
  	}
}
}

((is_null($___mysqli_res = mysqli_close($connected_db))) ? false : $___mysqli_res);

?>
