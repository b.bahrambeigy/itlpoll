<?php
/* **********************************************************************
*************************************************************************
####    This App Is Written By : Bahrambeigy - ITLPoll Version 3    #####
########       [https://gitlab.com/b.bahrambeigy/itlpoll]        ########
####              This application is free of charge :)             #####
####                Contact me: bahramwhh@gmail.com                 #####
*************************************************************************
#########################################################################
File : logout.php
Writer : Bahrambeigy (bahramwhh@gmail.com)
Description : This script is responsible for clearing admin sessions.
It makes sure that administrator sessions are kept safe.
**********************************************************************
*/

session_start();
@error_reporting (E_ERROR | E_PARSE);

// Installation Check !
if(filesize("../config.php") == 0)
{
  header("Location: ../install/index.php");
}
require_once("../config.php");
if(!(file_exists("../includes/functions.php")))
{
    echo "Your Functions.php File Doesn't Exist in : includes/functions.php";
    @exit();
}
require_once("../includes/functions.php");
db_connect($host, $user, $passwd, $database);
Load_Configs($prefix);

// Language File Existance Check
file_check("../language/".$configs['language'].".php", "Error : Your Default Laguage File Doesn't Exist");
require_once("../language/".$configs['language'].".php");

// Check first then Open Theme File And Use it!
file_check("../templates/".$configs['template']."/admin.xml", "Error : Your Default Template File(admin.xml) Doesn't Exist");

$itl_fxml = file_get_contents("../templates/".$configs['template']."/admin.xml");

$ip = getRealIpAddress();
$nowtime = date("H|i");

$header_html = Show_Admin_Header($itl_fxml);
echo preg_replace("/<navigation_menu>(.*)<\/navigation_menu>/is", "", $header_html);

if( (!empty($_SESSION['ITLPoll']['Admin'])) || ( !Time_Valid($_SESSION['ITLPoll']['AdminTime'], $nowtime) ) )
{
   	$admin_session = explode("|", $_SESSION['ITLPoll']['Admin']);
	$username = base64_decode(filter($admin_session[0]));
	
	unset($_SESSION['ITLPoll']['Admin']);
   	unset($_SESSION['ITLPoll']['AdminTime']);
   
   	$ip = stripslashes($ip);
	$query = "UPDATE ".$prefix."_users SET lastlogin='". $ip . "' WHERE username='".$username."';";
    mysqli_query($GLOBALS["___mysqli_ston"], $query);
    session_destroy();
    
    echo "<center><b>". _LOGGEDOUT . "</b><br/> IP : ". $ip ."<br/></center>";
}

Show_Admin_Login($itl_fxml);
Show_Admin_Footer($itl_fxml, $configs['version']);    

?>
