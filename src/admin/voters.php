<?php
/* **********************************************************************
*************************************************************************
####    This App Is Written By : Bahrambeigy - ITLPoll Version 3    #####
########       [https://gitlab.com/b.bahrambeigy/itlpoll]        ########
####              This application is free of charge :)             #####
####                Contact me: bahramwhh@gmail.com                 #####
*************************************************************************
#########################################################################
File : visitors.php
Writer : Bahrambeigy (bahramwhh@gmail.com)
Description : Using this file administrators can manage visitors accounts.
**********************************************************************
*/

session_start();
@error_reporting (E_ERROR | E_PARSE);

// Installation Check !
if(filesize("../config.php") == 0)
{
  header("Location: ../install/index.php");
}
require_once("../config.php");

if(!defined("ITLPoll_INSTALLED"))
{
  header("Location: ../install/index.php");
  @exit();
}

if(!(file_exists("../includes/functions.php")))
{
    echo "Your Functions.php File Doesn't Exist in : includes/functions.php";
    @exit();
}
require_once("../includes/functions.php");
$connected_db = db_connect($host, $user, $passwd, $database);
Load_Configs($prefix);

// Language File Existance Check
file_check("../language/".$configs['language'].".php", "Error : Your Default Laguage File Doesn't Exist");
require_once("../language/".$configs['language'].".php");

// Check first then Open Theme File And Use it!
file_check("../templates/".$configs['template']."/admin.xml", "Error : Your Default Template File(admin.xml) Doesn't Exist");

$itl_fxml = file_get_contents("../templates/".$configs['template']."/admin.xml");

$nowtime = date("H|i");

$header_html = Show_Admin_Header($itl_fxml);

if( (empty($_SESSION['ITLPoll']['Admin'])) || ( !Time_Valid($_SESSION['ITLPoll']['AdminTime'], $nowtime) ) )
{
    // This Will Empty ITLPoll Sessions For More Security
    if(isset($_SESSION['ITLPoll']['Admin'])) unset($_SESSION['ITLPoll']['Admin']);
    if(isset($_SESSION['ITLPoll']['AdminTime'])) unset($_SESSION['ITLPoll']['AdminTime']);
    session_destroy();

    // Now Shows The Login Page
    echo preg_replace("/<navigation_menu>(.*)<\/navigation_menu>/is", "", $header_html);
    Show_Admin_Login($itl_fxml, "voters.php");
    Show_Admin_Footer($itl_fxml, $configs['version']);
    @exit();
}

// [Start] Session Fixation Prevention !
$admin_session = explode("|", $_SESSION['ITLPoll']['Admin']);
$username = base64_decode(filter($admin_session[0]));

$squery = "SELECT last_session FROM ".$prefix."_users WHERE username = '".$username."';";
$msquery = mysqli_query($GLOBALS["___mysqli_ston"], $squery);
$check_sess = mysqli_fetch_array($msquery);

if($admin_session[1] != $check_sess['last_session'])
{
	unset($_SESSION['ITLPoll']['Admin']);
	unset($_SESSION['ITLPoll']['AdminTime']);
	msg("Session Fixation Detected !", "?");
	@exit();
}
// [End] Session Fixation Prevention !

// Reactive Time Valid For Admin
$nowtime = date("H|i");
$increase_minute = 20;
$admintime = increase_admin_time($nowtime, $increase_minute);
$_SESSION['ITLPoll']['AdminTime'] = $admintime;

echo $header_html;

@$type = $_REQUEST['type'];

// Check Permission ( added on Version 3 )
$admin_session = explode("|", $_SESSION['ITLPoll']['Admin']);
$active_user = base64_decode(filter($admin_session[0]));
$perm = Have_Permission("group", $active_user, $host, $user, $passwd, $database, $prefix);


if(empty($type) || (($configs['voters_login'] == "no") && $type != enable) )
{
	$now = date("Y-m-d");
	$query = "SELECT username,last_voted_date,last_voted_ip,last_login_date,total_votes FROM ".$prefix."_voters 
	WHERE last_voted_date != '9999-12-31'
	ORDER BY last_voted_date DESC 
	LIMIT 0,".$configs['numarchive'].";";
	$mquery = mysqli_query($GLOBALS["___mysqli_ston"], $query);
	
	$waiting = "SELECT username,enabled,verified FROM ".$prefix."_voters
	WHERE enabled='no' AND verified='yes';";
	$waiting_exec = mysqli_query($GLOBALS["___mysqli_ston"], $waiting);
	
	Show_Voters_Management($itl_fxml, $mquery, $waiting_exec, $configs['voters_login'], $configs['admin_voters_verification']);
	Show_Admin_Footer($itl_fxml, $configs['version']);
}

else if(!$perm)
{
	msg(_ENOUGHPERM, $_SERVER['PHP_SELF']);
	@exit();
}

else if($type == "enable")
{
	@$voters_management = filter($_POST['voters_management']);

	if($voters_management == "yes")
	{
		$update = "UPDATE ".$prefix."_config SET voters_login='yes';";
		$update_exec = mysqli_query($GLOBALS["___mysqli_ston"], $update);
		if($update_exec)
		{
			msg(_VOTERMGMENABLEDSUCC, "?");
			@exit();
		}
	}
	else if(isset($_GET['username']))
	{
		$username = mb_strtolower(filter($_GET['username']));
		$update = "UPDATE ".$prefix."_voters SET enabled='yes' WHERE username='".$username."';";
		$update_exec = mysqli_query($GLOBALS["___mysqli_ston"], $update);
		if($update_exec)
		{
			$subject = "[ITLPoll] Your Voter account is enabled now !";
			$msg = "Hi dear user!,
Your voter account is enabled by administrator(s) in the ITLPoll survey based website : 
http://" . $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME'] . "

Login Information:
Username: ".$username."
Password: [Password has beed emailed you before]
____________________
ITLPoll [http://www.itlpoll.com]";
			$header = 'From: "ITLPoll"';
			$mail = mail($username, $subject, $msg, $header);
				
			msg(_SELCECTEDVOTERENABLEDSUCC, "?");
			@exit();
		}
	}
}

else if($type == "disable")
{
	@$what = filter($_POST['what']);
	if($what == "voters")
	{
		$update = "UPDATE ".$prefix."_config SET voters_login='no';";
		$update_exec = mysqli_query($GLOBALS["___mysqli_ston"], $update);
		if($update_exec)
		{
			msg(_VOTERMGMDISABLEDSUCC, "?");
			@exit();
		}
	}
	else if($what == "admin_confirmation")
	{
		if($configs['admin_voters_verification'] == "yes")
			$value = "no";
		else
			$value = "yes";
		$update = "UPDATE ".$prefix."_config SET admin_voters_verification='".$value."';";
		$update_exec = mysqli_query($GLOBALS["___mysqli_ston"], $update);
		if($update_exec)
		{
			if($value == "yes")
				msg(_VOTERADMINCONFENABLEDSUCC, "?");
			else
				msg(_VOTERADMINCONFDISABLEDSUCC, "?");
			@exit();
		}
	} 
}

else if($type == "search")
{
	@$username = mb_strtolower(filter(trim($_REQUEST['username'])));
	if(empty($username))
	{
		msg(_PLZPROVIDEUSERNAME, "?");
		@exit();
	}
	else
	{
		$search = "SELECT username,total_votes,enabled,verified FROM ".$prefix."_voters WHERE username='".$username."';";
		$search_exec = mysqli_query($GLOBALS["___mysqli_ston"], $search);
		
		$founded = mysqli_num_rows($search_exec);
		if($founded < 1)
		{
			msg(_NOSUCHVOTERUSER, "?");
			@exit();
		}
		
		Show_Voters_Edit($itl_fxml, $search_exec);
		Show_Admin_Footer($itl_fxml, $configs['version']);
		
	}
}

else if($type == "edit")
{
	@$username = mb_strtolower(filter(trim($_POST['new_username'])));
	$main_username = filter(trim($_POST['username']));
	@$password = filter($_POST['password']);
	@$total_votes = filter(trim($_POST['total_votes']));
	@$enabled = filter(trim($_POST['enabled']));
	@$verified = filter(trim($_POST['verified']));
	$remove = filter(trim($_POST['remove']));
	
	if(empty($username))
	{
		msg("You must specify username !", "?");
		@exit();
	}
	
	$search = "SELECT * FROM ".$prefix."_voters WHERE username='".$main_username."';";
	$search_exec = mysqli_query($GLOBALS["___mysqli_ston"], $search);
		
	$founded = mysqli_num_rows($search_exec);
	if($founded < 1)
	{
		msg(_NOSUCHVOTERUSER, "?");
		@exit();
	}
	
	if($remove == "yes")
	{
		$remove = "DELETE FROM ".$prefix."_voters WHERE username='".$main_username."';";
		$remove_exec = mysqli_query($GLOBALS["___mysqli_ston"], $remove);
		if($remove_exec)
		{
			msg(_VOTERACCDELETEDSUCC, "?");
			@exit();
		}
	}
	
	if(empty($password))
		$update = "UPDATE ".$prefix."_voters SET username='".$username."',total_votes='".$total_votes."',enabled='".$enabled."',verified='".$verified."' WHERE username='".$main_username."';";
	else
		$update = "UPDATE ".$prefix."_voters SET username='".$username."', password='".md5($password)."', total_votes='".$total_votes."',enabled='".$enabled."',verified='".$verified."' WHERE username='".$main_username."';";
		
	$update_exec = mysqli_query($GLOBALS["___mysqli_ston"], $update);
	if($update_exec)
	{
		msg(_VOTERPROFILEUPDATEDSUCC, "?");
		@exit();
	}	
		
}

((is_null($___mysqli_res = mysqli_close($connected_db))) ? false : $___mysqli_res);

?>
