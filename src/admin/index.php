<?php
/* **********************************************************************
*************************************************************************
####    This App Is Written By : Bahrambeigy - ITLPoll Version 3    #####
########       [https://gitlab.com/b.bahrambeigy/itlpoll]        ########
####              This application is free of charge :)             #####
####                Contact me: bahramwhh@gmail.com                 #####
*************************************************************************
#########################################################################
File : Index.php

Writer : Bahrambeigy (bahramwhh@gmail.com)
Description : Administrator home page Of ITLPoll system. Using this script
one can manage polls by editing and adding, and managing the whole app.
**********************************************************************
*/

@error_reporting (E_ERROR | E_PARSE);
@session_start();

// Installation Check !
if(filesize("../config.php") == 0)
{
  header("Location: ../install/index.php");
}
require_once("../config.php");

if(!defined("ITLPoll_INSTALLED"))
{
  header("Location: ../install/index.php");
  @exit();
}

if(!(file_exists("../includes/functions.php")))
{
    echo "Your Functions.php File Doesn't Exist in : includes/functions.php";
    @exit();
}
require_once("../includes/functions.php");
db_connect($host, $user, $passwd, $database);
Load_Configs($prefix);

// Language File Existance Check

file_check("../language/".$configs['language'].".php", "Error : Your Default Laguage File Doesn't Exist");
require_once("../language/".$configs['language'].".php");

// Check first then Open Theme File And Use it!
file_check("../templates/".$configs['template']."/admin.xml", "Error : Your Default Template File(admin.xml) Doesn't Exist");

$itl_fxml = file_get_contents("../templates/".$configs['template']."/admin.xml");

$header_html = Show_Admin_Header($itl_fxml);
// remove navigation menu (we don't need this here ;) )
echo preg_replace("/<navigation_menu>(.*)<\/navigation_menu>/is", "", $header_html);

@$action = $_REQUEST['action'];
if(empty($action)) 
{

$nowtime = date("H|i");

if( (!empty($_SESSION['ITLPoll']['Admin'])) AND ( Time_Valid($_SESSION['ITLPoll']['AdminTime'], $nowtime) ) )
{
	
	// Reactive Time Valid For Admin
	$nowtime = date("H|i");
	$increase_minute = 20;
	$admintime = increase_admin_time($nowtime, $increase_minute);
	$_SESSION['ITLPoll']['AdminTime'] = $admintime;
	
	$admin_session = explode("|", $_SESSION['ITLPoll']['Admin']);
	$username = base64_decode(filter($admin_session[0]));
	
	// Session Hijack Prevention !
	$squery = "SELECT last_session FROM ".$prefix."_users WHERE username = '".$username."';";
	$msquery = mysqli_query($GLOBALS["___mysqli_ston"], $squery);
	$check_sess = mysqli_fetch_array($msquery);
	
	if($admin_session[1] != $check_sess['last_session'])
	{
		unset($_SESSION['ITLPoll']['Admin']);
   		unset($_SESSION['ITLPoll']['AdminTime']);
   		msg("Session Fixation Detected !", "?");
   		@exit();
	}
	
	$query = "SELECT lastlogin FROM ".$prefix."_users WHERE username = '".$username."';";
	$mquery = mysqli_query($GLOBALS["___mysqli_ston"], $query);
	if($mquery)
	{
		$lastlog = mysqli_fetch_array($mquery);
		$lastlogin = $lastlog['lastlogin'];
	}
	
	Show_Admin_Main($itl_fxml, $lastlogin, $configs['version']);
}
else
{
@$username = mb_strtolower(filter(stripslashes(trim($_POST['UserName']))));
@$password = filter(stripslashes($_POST['PassWord']));
    if(empty($username) || empty($password)) {
        // This Will Empty ITLPoll Sessions For More Security
        if(isset($_SESSION['ITLPoll']['Admin'])) unset($_SESSION['ITLPoll']['Admin']);
        if(isset($_SESSION['ITLPoll']['AdminTime'])) unset($_SESSION['ITLPoll']['AdminTime']);
        //session_destroy();
        
        // Now Shows The Login Page
        Show_Admin_Login($itl_fxml);
    }
    else
    {
    	if( isset($_SESSION['ITLPoll_Admin_Tries']) &&  ($_SESSION['ITLPoll_Admin_Tries'] > 3) )
        {
        	echo "<script language=\"JavaScript\">alert(\"". _BRUTEFORCEREACHED ."\")</script>";
        	echo "<script language=\"JavaScript\">history.back(1)</script>";
        	@exit();
        }
    
    	// Querring The Password Form Database
		$pas = "SELECT password, blocked, forgot, lastlogin FROM ".$prefix."_users WHERE username = '".$username."'";
		$query = @mysqli_query($GLOBALS["___mysqli_ston"], $pas);
		$num_result = @mysqli_num_rows($query);
		@$redirect = filter(trim($_POST['redirect']));
		if($num_result > 0)
		{
			$apass = mysqli_fetch_array($query);
			$apasswd = $apass['password'];
		
	  		if(md5($password) == $apasswd)
			{
				if($apass['blocked'] == 'yes')
				{
				  msg(_ACCOUNTBLOCKED, $_SERVER['PHP_SELF']);
				  @exit();
				}
	        	$nowtime = date("H|i");
	        	$increase_minute = 20;
				$admintime = increase_admin_time($nowtime, $increase_minute);
	        	
	        	$this_session = session_id();
	        		
	        	$s_query = "UPDATE ".$prefix."_users SET last_session = '".$this_session."' WHERE username='".$username."'; ";
	        	$sm_query = mysqli_query($GLOBALS["___mysqli_ston"], $s_query);
	        		
	        	$_SESSION['ITLPoll']['Admin'] = base64_encode($username) . "|" . $this_session;
	        	$_SESSION['ITLPoll']['AdminTime'] = $admintime;
	        	if($apass['forgot'] == "yes") 
				{
		        	$update = "UPDATE ".$prefix."_users SET forgot='no' WHERE username='".$username."'";
		        	mysqli_query($GLOBALS["___mysqli_ston"], $update);
	        	}
	        	if(empty($apass['lastlogin'])) $lastlogin = "None Recorded!";
	        	else $lastlogin = $apass['lastlogin'];
	        	
	        	unset($_SESSION['ITLPoll_Admin_Tries']);
	        		
	        	// Redirect latest user position
	        	if($redirect != "index.php")
	        		echo "<script>window.location = './".$redirect."'</script>";
	        	else
	        		Show_Admin_Main($itl_fxml, $lastlogin, $configs['version']);
	        	Show_Admin_Footer($itl_fxml, $configs['version']);
			}
        	else
      		{
      			if(!isset($_SESSION['ITLPoll_Admin_Tries']))
      			{
        			$_SESSION['ITLPoll_Admin_Tries'] =  1;
        			echo "<script language=\"JavaScript\">alert(\"". _UPERROR ."\")</script>";
        			echo "<script language=\"JavaScript\">history.back(1)</script>";
      			}
        		else
        		{
        			if($_SESSION['ITLPoll_Admin_Tries'] > 3)
        			{
        				echo "<script language=\"JavaScript\">alert(\"". _BRUTEFORCEREACHED ."\")</script>";
        				echo "<script language=\"JavaScript\">history.back(1)</script>";
        			}
        			else
        			{
        				$_SESSION['ITLPoll_Admin_Tries'] += 1;
        				echo "<script language=\"JavaScript\">alert(\"". _UPERROR ."\")</script>";
        				echo "<script language=\"JavaScript\">history.back(1)</script>";
        			}
        		}
      		}
      		@exit();
      	}
      	else
      	{
        	echo "<script language=\"JavaScript\">alert(\"". _UPERROR ."\")</script>";
        	echo "<script language=\"JavaScript\">history.back(1)</script>";
        	@exit();
      	}
    }
  } 
}

elseif($action == "forget") {
@$id = filter($_REQUEST['id']);
    if(empty($id)) 
	{
        @$user = mb_strtolower(filter(trim($_POST['username'])));
        @$email = filter(trim($_POST['email']));
        if(empty($user) || empty($email)) 
		{
        	Admin_Password_Forget($itl_fxml);
        	Show_Admin_Footer($itl_fxml, $configs['version']);
        	@exit();
        }
        else 
		{
          $query = "SELECT username, password, email, forgot FROM ".$prefix."_users WHERE username = '".$user."'";
          $mquery = @mysqli_query($GLOBALS["___mysqli_ston"], $query);
          $num_result = mysqli_num_rows($mquery);
          if($num_result > 0)
          {
		  	$user_inf = mysqli_fetch_array($mquery);
			if($user == $user_inf['username']) 
			{
				if($email == $user_inf['email']) 
				{
			  		$apasswd = $user_inf['password'];
              		$link = md5($user_inf['username']) . "|" . $apasswd;
              		$subject = "[ITLPoll] Reset Password Request";
              		$mes = "Hi Dear User,
You are requested to reset your ITLPoll account password.
If you don't want to reset your admin password leave this message please!

***********************************************************
For reset your password please click this link to change your password :
http://" . $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME'] . "?action=forget&id=". $link  . "
************************************************************
              
For more security the script will change your password to a random password, after reseting password you can change that in admin menu !
              
Thanks
ITLPoll Version 3
Programing : Bahrambeigy (bahramwhh@gmail.com) ";
              		$header = 'From: "ITLPoll"';
              		$mail = mail($user_inf['email'], $subject, $mes, $header);
              		if($mail) 
					{ 
			  			msg(_MAILSENT, $_SERVER['PHP_SELF']);
              			Admin_Password_Forget($itl_fxml);
              			$update = "UPDATE ".$prefix."_users set forgot = 'yes' WHERE username = '". $user ."'";
              			$ok = mysqli_query($GLOBALS["___mysqli_ston"], $update);
              			@exit();
            		}
            		if(!$mail) 
					{
              			Admin_Password_Forget($itl_fxml);
              			msg(_ERRORMAIL, $_SERVER['PHP_SELF']);
              			Show_Admin_Footer($itl_fxml, $configs['version']);
              			@exit();
            		}
              }
            
              else 
			  {
                echo '<script>alert("'. _NOEMAIL . '")</script>';
                Admin_Password_Forget($itl_fxml);
                Show_Admin_Footer($itl_fxml, $configs['version']);
                @exit();
              }
           }
          }
          else 
          {
          	echo '<script>alert("'. _NOUSER . '")</script>';
          	Admin_Password_Forget($itl_fxml);
          	Show_Admin_Footer($itl_fxml, $configs['version']);
          	@exit();
          }
        }
        
    }
    else 
	{
        @$id = filter($_REQUEST['id']);
        // Quering The Password From Database
		$pas = "SELECT username, password, email FROM ".$prefix."_users WHERE forgot = 'yes';";
		$query = @mysqli_query($GLOBALS["___mysqli_ston"], $pas);
		$num_result = mysqli_num_rows($query);
		if($num_result > 0)
		{
			$link = "";
			for($iu=0; $iu<$num_result; $iu++)
			{
				$apass = mysqli_fetch_array($query);
				$apasswd = $apass['password'];
				$aname = $apass['username'];
				// Crating The Code Link !!
        		$link = md5($aname) . "|" . $apasswd;
        		
        		if($id == $link)
        			break;
        		$query = @mysqli_query($GLOBALS["___mysqli_ston"], $pas);
        	}
        	
        	if( $id == $link ) 
			{
            	$password = get_random();
            	$update = "UPDATE ".$prefix."_users SET password='". md5($password) ."', forgot='no' WHERE username = '". $aname ."'";
            	$ok = mysqli_query($GLOBALS["___mysqli_ston"], $update);
            	if($ok) 
				{
               		$subject = "[ITLPoll] Your ITLPoll Account Password Changed";
              		$mes = "Hi Dear User,
The password of ITLPoll Script has been reseted now.

***********************************************************
You can login with this account :
UserName : $aname
Password : $password
***********************************************************
              
Login Address : http://" . $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME'] ."
              
Thanks
ITLPoll Version 3
Programing : Bahrambeigy (bahramwhh@gmail.com) ";
              		$header = 'From: "ITLPoll"';
              		$mail = mail($apass['email'], $subject, $mes, $header);
              		Admin_Password_Forget($itl_fxml);
              		Show_Admin_Footer($itl_fxml, $configs['version']);
              		msg(_PASSRESETED, $_SERVER['PHP_SELF']);
              		@exit();
            	}
            	if(!$ok) 
            	{
              		Admin_Password_Forget($itl_fxml);
              		Show_Admin_Footer($itl_fxml, $configs['version']);
              		msg(_ERRORRESET, $_SERVER['PHP_SELF']);
              		@exit();
            	}
        	}
        
		}
		
        else 
        {
          Admin_Password_Forget($itl_fxml);
          Show_Admin_Footer($itl_fxml, $configs['version']);
          msg(_INVCODE, $_SERVER['PHP_SELF']);
          @exit();
        }
        
        
    }
    
}
Show_Admin_Footer($itl_fxml, $configs['version']);
?>
