<?php
/* **********************************************************************
*************************************************************************
####    This App Is Written By : Bahrambeigy - ITLPoll Version 3    #####
########       [https://gitlab.com/b.bahrambeigy/itlpoll]        ########
####              This application is free of charge :)             #####
####                Contact me: bahramwhh@gmail.com                 #####
*************************************************************************
#########################################################################
File : edit.php
Writer : Bahrambeigy (bahramwhh@gmail.com)
Description : Using this file one can edit polls, Activate or delete them.
After adding a poll, it should be activated HERE.
**********************************************************************
*/

session_start();
@error_reporting (E_ERROR | E_PARSE);

// Installation Check !
if(filesize("../config.php") == 0)
{
  header("Location: ../install/index.php");
}
require_once("../config.php");
if(!(file_exists("../includes/functions.php")))
{
    echo "Your Functions.php File Doesn't Exist in : includes/functions.php";
    @exit();
}
require_once("../includes/functions.php");

if(!defined("ITLPoll_INSTALLED"))
{
  header("Location: ../install/index.php");
  @exit();
}

$connected_db = db_connect($host, $user, $passwd, $database);
Load_Configs($prefix);

// Language File Existance Check
file_check("../language/".$configs['language'].".php", "Error : Your Default Laguage File Doesn't Exist");
require_once("../language/".$configs['language'].".php");

// Check first then Open Theme File And Use it!
file_check("../templates/".$configs['template']."/admin.xml", "Error : Your Default Template File(admin.xml) Doesn't Exist");

$itl_fxml = file_get_contents("../templates/".$configs['template']."/admin.xml");


$nowtime = date("H|i");

// Now Show The Header File !
$header_html = Show_Admin_Header($itl_fxml);

if( (empty($_SESSION['ITLPoll']['Admin'])) || ( !Time_Valid($_SESSION['ITLPoll']['AdminTime'], $nowtime) ) )
{
    // This Will Empty ITLPoll Sessions For More Security
    if(isset($_SESSION['ITLPoll']['Admin'])) unset($_SESSION['ITLPoll']['Admin']);
    if(isset($_SESSION['ITLPoll']['AdminTime'])) unset($_SESSION['ITLPoll']['AdminTime']);
    session_destroy();

    // Now Shows The Login Page
    echo preg_replace("/<navigation_menu>(.*)<\/navigation_menu>/is", "", $header_html);
    Show_Admin_Login($itl_fxml, "edit.php");
    Show_Admin_Footer($itl_fxml, $configs['version']);
    @exit();
}

// [Start] Session Fixation Prevention !
$admin_session = explode("|", $_SESSION['ITLPoll']['Admin']);
$username = base64_decode(filter($admin_session[0]));

$squery = "SELECT last_session FROM ".$prefix."_users WHERE username = '".$username."';";
$msquery = mysqli_query($GLOBALS["___mysqli_ston"], $squery);
$check_sess = mysqli_fetch_array($msquery);

if($admin_session[1] != $check_sess['last_session'])
{
	unset($_SESSION['ITLPoll']['Admin']);
	unset($_SESSION['ITLPoll']['AdminTime']);
	msg("Session Fixation Detected !", "?");
	@exit();
}
// [End] Session Fixation Prevention !

// Reactive Time Valid For Admin
$nowtime = date("H|i");
$increase_minute = 20;
$admintime = increase_admin_time($nowtime, $increase_minute);
$_SESSION['ITLPoll']['AdminTime'] = $admintime;

echo $header_html;

$conf_enabled = 0;
if(isset($_REQUEST['ConfEnabled']))
	$conf_enabled = 1;

if($conf_enabled)
	$query = "SELECT * FROM ".$prefix."_poll WHERE active='yes' AND confirmed='yes';";
else
	$query = "SELECT * FROM ".$prefix."_poll;";
$mquery = mysqli_query($GLOBALS["___mysqli_ston"], $query);
$number = mysqli_num_rows($mquery);

$total_temp = $number / $configs['numarchive'];
$total_temp2 = floor($total_temp);

if($total_temp > $total_temp2)
	$total_pages = $total_temp2 + 1;
else
	$total_pages = $total_temp2;

if ( $number > $configs['numarchive'] )
	$number = $configs['numarchive'];

@$numchoices = filter(trim($_POST['ChoiceNum']));
@$type = $_REQUEST['type'];

@$pgrow = filter(trim($_REQUEST['number'])); // for pages

// Detecting not confirmed polls [version 3.1]
$query7 = "SELECT id FROM ".$prefix."_poll WHERE active != confirmed";
$mquery7 = mysqli_query($GLOBALS["___mysqli_ston"], $query7);
$not_confirmed = mysqli_num_rows($mquery7);

if($not_confirmed > 0)
{
	define("MULTIPLEACTIVE", "1");
	$query8 = "SELECT multiactivestr FROM ".$prefix."_config";
	$mquery8 = mysqli_query($GLOBALS["___mysqli_ston"], $query8);
	
	$multiactive = mysqli_fetch_array($mquery8);
	define("MULTACTIVESTR", stripslashes($multiactive['multiactivestr']));
}

if(empty($type))
{
    Show_Edit_Poll($usejalali, $itl_fxml, $succ, $number, $mquery, $total_pages, $not_confirmed, $conf_enabled);
    Show_Admin_Footer($itl_fxml, $configs['version']);
    @exit();
}
// Added in version 2.6
if($type == "page")
{	
	$page_offset = ($pgrow-1) * $configs['numarchive'];
	
	if($pgrow > $total_pages)
		msg("");

	if($conf_enabled)
		$query10 = "SELECT * FROM ".$prefix."_poll WHERE active='yes' AND confirmed='yes' LIMIT ".$configs['numarchive']." OFFSET ".$page_offset.";";
	else
		$query10 = "SELECT * FROM ".$prefix."_poll LIMIT ".$configs['numarchive']." OFFSET ".$page_offset.";";
	$mquery10 = mysqli_query($GLOBALS["___mysqli_ston"], $query10);
	$number = mysqli_num_rows($mquery10);

	Show_Edit_Poll($usejalali, $itl_fxml, $succ, $number, $mquery10, $total_pages, $not_confirmed, $conf_enabled, $pgrow);
	Show_Admin_Footer($itl_fxml, $configs['version']);
	@exit();	
}
if($type == "edit")
{
    
	if(trim($_POST['Question']))
	{

		// Check Permission ( added on Version 3 )
		$admin_session = explode("|", $_SESSION['ITLPoll']['Admin']);
		$active_user = base64_decode(filter($admin_session[0]));
		$perm = Have_Permission("edit", $active_user, $host, $user, $passwd, $database, $prefix);
		if(!$perm)
		{
			msg(_ENOUGHPERM, $_SERVER['PHP_SELF']);
    		@exit();
		}
	  
	  $subject = filter($_REQUEST['Question']);
	  $id = filter($_REQUEST['num']);
	  $expdate = array();
	  $strdate = array();
	  @$expdate['year'] = filter(trim($_POST['Expire_Year']));
	  @$expdate['month'] = filter(trim($_POST['Expire_Month']));
	  @$expdate['day'] = filter(trim($_POST['Expire_Day']));
	  @$strdate['year'] = filter(trim($_POST['Start_Year']));
	  @$strdate['month'] = filter(trim($_POST['Start_Month']));
	  @$strdate['day'] = filter(trim($_POST['Start_Day']));
 	  $numchoices = filter(trim($_POST['numchoices']));
 	  @$multiple = filter(trim($_POST['multiple']));
 	  
 	  for ($i=0; $i < $numchoices; $i++)
	  {
			$ch_num = $i + 1;
			$textbox = "ch". $ch_num;
			$ch[$i] = filter(trim($_POST[$textbox]));
	  }
 	  
	  for($i=1; $i <= $numchoices; $i++)
	  {
		  $ch_num = $i - 1;
		  $s_str .= ($i == $numchoices) ? "ch". $i ."='".$ch[$ch_num]."'" : "ch". $i ."='". $ch[$ch_num] ."', ";
		  
	  }
	  	  
	  $expdate_str = $expdate['year'] . "-" . $expdate['month'] . "-" . $expdate['day'];
	  $strdate_str = $strdate['year'] . "-" . $strdate['month'] . "-" . $strdate['day'];
	  
	  if(empty($expdate['day']) || empty($expdate['month']) || empty($expdate['year']))
  		  $expdate_str = "9999-12-31";
    	
      if(empty($strdate['day']) || empty($strdate['month']) || empty($strdate['year']))
    	  $strdate_str = "9999-12-31";
      
      if($usejalali == "1")
	  {
			if($expdate_str != "9999-12-31")
			{
				$expdate_conv = explode("-", $expdate_str);
				$expd_conved = ConvDate($expdate_conv, "G");
				$expdate_str = implode("-", $expd_conved);
			}
			if($strdate_str != "9999-12-31")
			{
				$strdate_conv = explode("-", $strdate_str);
				$strd_conved = ConvDate($strdate_conv, "G");
				$strdate_str = implode("-", $strd_conved);
			}
	  }
      
      $query7 = "UPDATE ".$prefix."_poll SET subject='".$subject."', multiple='".$multiple."', expire='".$expdate_str."', start='".$strdate_str."' WHERE id='".$id."'";
      mysqli_query($GLOBALS["___mysqli_ston"], $query7);
      $query8 = "UPDATE ".$prefix."_choices SET ". $s_str ." WHERE id='".$id."'";
      $mquery8 = mysqli_query($GLOBALS["___mysqli_ston"], $query8);
      if($mquery8) $succ = "Show";
        
        Show_Editing_Poll($itl_fxml, $choices, $mquery5, $numbe, $succ, $mquery6, $usejalali);
        Show_Admin_Footer($itl_fxml, $configs['version']);
    }
    else
    {
      $numbe = stripslashes($_REQUEST['number']);
      if(!preg_match("/[1-9]/", $numbe)) {
		 msg("Ivalid Number Entered! - Hacking Attempt", $_SERVER['PHP_SELF']);
		 @exit();
	  }
      $query5 = "SELECT * FROM ".$prefix."_poll WHERE id='".$numbe."'";
      $mquery5 = mysqli_query($GLOBALS["___mysqli_ston"], $query5);
      $numrows = mysqli_num_rows($mquery5);
      if($numrows == 0) msg(_NOROWS, $_SERVER['PHP_SELF']);
      else
      {
        
        $query6 = "SELECT * FROM ".$prefix."_choices WHERE id='".$numbe."'";
        $mquery6 = mysqli_query($GLOBALS["___mysqli_ston"], $query6);
        
        Show_Editing_Poll($itl_fxml, $choices, $mquery5, $numbe, $succ, $mquery6, $usejalali);
        Show_Admin_Footer($itl_fxml, $configs['version']);
      }
    }
    
}
if($type == "active")
{
  @$num = stripslashes($_REQUEST['number']);
  
  if(empty($num)) {
    Show_Edit_Poll($usejalali, $itl_fxml, $succ, $number, $mquery, $total_pages, $not_confirmed);
    Show_Admin_Footer($itl_fxml, $configs['version']);
   @exit();
  }
  else
  {
   	$query14 = "SELECT id,active FROM ".$prefix."_poll WHERE id='".$num."';";
   	$mquery14 = mysqli_query($GLOBALS["___mysqli_ston"], $query14);
   	
	$founded_poll = mysqli_fetch_array($mquery14);
	$is_active = $founded_poll['active'];
	if($is_active == "yes")
	{
		$query2 = "UPDATE ".$prefix."_poll SET active='no' WHERE id='".$founded_poll['id']."';";
    	$update = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
    	if($update)
    		$succ = "Show";
	}
    else if($is_active == "no")
    {
    	$query3 = "UPDATE ".$prefix."_poll SET active='yes' WHERE id='".$founded_poll['id']."';";
    	$updated = mysqli_query($GLOBALS["___mysqli_ston"], $query3);
    	if($updated)
    		$succ = "Show";
    }
    Show_Edit_Poll($usejalali, $itl_fxml, $succ, $number, $mquery, $total_pages, $not_confirmed);
    Show_Admin_Footer($itl_fxml, $configs['version']);
   @exit();
  }

}

if($type == "confirm")
{
	@$discard = filter($_GET['discard']);
	@$title = filter($_POST['title_message']);
	@$sure_to_save = filter($_POST['sure_to_save']);
	
	if(!empty($discard) && $discard == "yes")
	{
		$query14 = "UPDATE ".$prefix."_poll SET active=confirmed WHERE active!=confirmed ;";
		$mquery14 = mysqli_query($GLOBALS["___mysqli_ston"], $query14);
		
		if($mquery14)
		{
			msg(_DISCARDEDCHANGES, "?");
			@exit();
		}
	}
	elseif(empty($sure_to_save) || $sure_to_save != 'yes')
	{
		echo '
<form id="save_form" name="save_form" action="edit.php" method="POST">
<input type="hidden" name="type" id="type" value="confirm">
<input type="hidden" name="sure_to_save" id="sure_to_save" value="yes">
<input type="hidden" name="title_message" id="title_message" value="'. $title .'">
</form>
<script>
var answer = confirm("'._CONFIRMSAVECHANGES.'");
if(answer) 
	window.onload = document.save_form.submit();
else
	window.location = "edit.php";
</script>
</body>
</html>';
	}
	elseif(!empty($sure_to_save) && $sure_to_save == 'yes')
	{
		$query16 = "SELECT id,choices FROM ".$prefix."_poll WHERE active='yes';";
		$mquery16 = mysqli_query($GLOBALS["___mysqli_ston"], $query16);
		$actived_ids = mysqli_fetch_array($mquery16);
		$num_active_polls = mysqli_num_rows($mquery16);
		
		if($num_active_polls > 0)
		{
			for($i=0; $i < $num_active_polls; $i++)
			{
				$update_query = "UPDATE ".$prefix."_results SET ";
				for($j=1; $j <= $actived_ids['choices']; $j++)
					$update_query .= "ch". $j ."=0, ";
				$update_query .= "uniques=0 WHERE id='".$actived_ids['id']."'";
				
				$update_mquery = mysqli_query($GLOBALS["___mysqli_ston"], $update_query);
				$actived_ids = mysqli_fetch_array($mquery16);
			}
		}
	
		$query14 = "UPDATE ".$prefix."_poll SET confirmed=active WHERE active!=confirmed ;";
		$mquery14 = mysqli_query($GLOBALS["___mysqli_ston"], $query14);
		
		$query15 = "UPDATE ".$prefix."_config SET multiactivestr='".nl2br($title)."';";
		$mquery15 = mysqli_query($GLOBALS["___mysqli_ston"], $query15);
		
		$now_date = date("Y-m-d H:i:s");
		
		$query16 = "UPDATE ".$prefix."_config SET multiactivedate='".$now_date."', total_votes='0';";
		$mquery16 = mysqli_query($GLOBALS["___mysqli_ston"], $query16);
		
		if($mquery14 && $mquery15 && $mquery16)
		{
			msg(_SAVEDCHANGES, "?");
			@exit();
		}
	}
	
}

if($type == "delete")
{
  if(empty($_REQUEST['number']))
  {
    Show_Edit_Poll($usejalali, $itl_fxml, $succ, $number, $mquery, $total_pages, $not_confirmed);
    Show_Admin_Footer($itl_fxml, $configs['version']);
  }
  else
  {
    // Check Permission ( added on Version 3 )
	$admin_session = explode("|", $_SESSION['ITLPoll']['Admin']);
	$active_user = base64_decode(filter($admin_session[0]));
	$perm = Have_Permission("delete", $active_user, $host, $user, $passwd, $database, $prefix);
	if(!$perm)
	{
		msg(_ENOUGHPERM, $_SERVER['PHP_SELF']);
    	@exit();
	}
	
	$numb = $_REQUEST['number'];
    if(!preg_match("/[1-9]/", $numb)) {
		 msg(_INVLDNUM, $_SERVER['PHP_SELF']);
		 @exit();
	  }
	
	$confirm = $_REQUEST['confirm'];
	if(empty($confirm))
		Show_Delete_Poll($numb, $succ);
		
    if($confirm == "Yes")
    {
	    $query13 = "SELECT * FROM ".$prefix."_poll WHERE id='".$numb."'";
	    $mquery13 = mysqli_query($GLOBALS["___mysqli_ston"], $query13);
	    $test = mysqli_fetch_array($mquery13);
	    $id = $test['id'];
	    $active = $test['active'];
	    if($active == "yes")
	    {
	        msg(_CANTDEL, $_SERVER['PHP_SELF']);
	        @exit();
	    }
	    $query10 = "DELETE FROM ".$prefix."_poll WHERE id='".$numb."'";
	    $deleted = mysqli_query($GLOBALS["___mysqli_ston"], $query10);
	    //$query11 = "DELETE FROM ".$prefix."_choices WHERE id='".$numb."'";
	    //mysql_query($query11);
	    //$query12 = "DELETE FROM ".$prefix."_results WHERE id='".$numb."'";
	    //$mquery12 = mysql_query($query12);
	    if($deleted)
	        $succ = "Show";
		Show_Delete_Poll($numb, $succ);
		Show_Admin_Footer($itl_fxml, $configs['version']);
    
    }
    if($confirm == "No")
    {
	    Show_Edit_Poll($usejalali, $itl_fxml, $succ, $number, $mquery, $total_pages, $not_confirmed);
	    Show_Admin_Footer($itl_fxml, $configs['version']);
    }
    
  }

}

((is_null($___mysqli_res = mysqli_close($connected_db))) ? false : $___mysqli_res);
?>
