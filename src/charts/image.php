<?php
/* **********************************************************************
*************************************************************************
####    This App Is Written By : Bahrambeigy - ITLPoll Version 3    #####
########       [https://gitlab.com/b.bahrambeigy/itlpoll]        ########
####              This application is free of charge :)             #####
####                Contact me: bahramwhh@gmail.com                 #####
*************************************************************************
#########################################################################
File : image.php ( new charts !)
Writer : Bahrambeigy (bahramwhh@gmail.com)
Description : This file is the new version of image.php added on version 3.
It supports 3D charts such as 3D Pie and 3D Bar.
**********************************************************************
*/
session_start();
@error_reporting (E_ERROR | E_PARSE);

// Installation Check !
if(filesize("../config.php") == 0)
{
  header("Location: ../install/index.php");
}
require_once("../config.php");

// Functions Existance Check
if(!(file_exists("../includes/functions.php")))
{
    echo "Your Functions.php File Doesn't Exist in : includes/functions.php";
    exit;
}
require_once("../includes/functions.php");
db_connect($host, $user, $passwd, $database);
Load_Configs($prefix);

require_once("../language/".$configs['language'].".php");

function no_votes()
{
    $im = imagecreate(300,50);
    $white=ImageColorAllocate($im,255,255,255);   
    $black=ImageColorAllocate($im,0,0,0);
    ImageFilledRectangle($im,0,0,300,50,$white);  
    ImageString($im, 5, 1, 1, "Sorry, There is no vote !", $black);
	
    Header('Content-type:  image/png');
    ImagePng($im);   
    ImageDestroy($im);
}

@$id = filter($_REQUEST['pollID']);

if(empty($id)) $query = "SELECT * FROM ".$prefix."_poll WHERE active = 'yes'";
else {
    // Checking $id For Injection (Bug Founded By Simorgh-ev Security Group)
    if(!preg_match("/[1-9]/", $id)) {
    echo "Hacking Attempt - You Can't Set A String Value For ID Variable!";
    exit;
    }
    else {
    $query = "SELECT * FROM ".$prefix."_poll WHERE id = '". $id ."'";
     }
}
$poll = mysqli_query($GLOBALS["___mysqli_ston"], $query);
$polllist = mysqli_fetch_array($poll);
$id = $polllist['id'];
$choices = $polllist['choices'];
$active = $polllist['active'];

@$type = filter($_GET['type']);

// Query The Results From Database
$query2 = "SELECT * FROM ".$prefix."_results WHERE id = $id";
$result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
$choice = mysqli_fetch_row($result);
// Query The Choices From Database
$query3 = "select * from ".$prefix."_choices where id = $id";
$cho = mysqli_query($GLOBALS["___mysqli_ston"], $query3);
$choi = mysqli_fetch_row($cho);

$total = 0;
for($i=1; $i<=$choices; $i++)
	$bar[$i] = $choice[$i];
for($b=1; $b<=$choices; $b++)
	$total += $bar[$b];
for($c=1; $c<=$choices; $c++)
	@$per[$c] = $choice[$c] / $total * 100;
for($i=1; $i<=$choices; $i++)
	$strchoice[$i] = $choi[$i];

$there_is_no_vote = true;
if($total > 0 )
  $there_is_no_vote = false;

// Getting 9 Random Colors ! 
for($i=1; $i<=$choices; $i++) 
{
  $rand1[$i] = rand(0, 255);
  $rand2[$i] = rand(0, 255);
  $rand3[$i] = rand(0, 255);
}

// type == bar means 3D Bar and type == pie means 3D Pie Graph
if( $type == "bar" )
{
  require_once("./class.3dbargraph.php");
  $graph = new Graph();
  
  // Configuration Variables ( Change Carefully !)
  $paddings['left'] = 30 ;
  $paddings['top'] = 30;
  $paddings['right'] = 20;
  $paddings['bottom'] = 20;
  
  $graph->graph_areaheight = 250;
  $graph->graph_height = $graph->graph_padding['top'] + (int)$graph->graph_areaheight + $graph->graph_padding['bottom'];
  $graph->graph_areawidth = 350;
  $graph->graph_width = $graph->graph_padding['left'] + (int)$graph->graph_areawidth + $graph->graph_padding['right'];
  $graph->graph_padding = $paddings;
  $graph->graph_title = "ITLPoll Powered"; // Powered Notice !
  $graph->graph_titlecolor = array(0, 0, 0);
  $graph->graph_titlefont = 3;
  $graph->graph_transparencylevel = 70;
  $graph->graph_borderwidth = 1;
  $graph->graph_bordercolor = array($rand1[1], $rand2[1], $rand3[1]);
  $graph->graph_bgcolor = array(194, 200, 234);
  $graph->graph_bgtransparent = false;
  
  
  $graph->axis_stepY = 10;  
  $graph->scale_roundY = 0;

  for($i=1; $i<=$choices; $i++)
  {
    if(!$bar[$i])
      $bar[$i] = 0.1;
    
    $data_bars[$i] = $per[$i];
  }

  $graph->data = $data_bars;

  // Show Graph 
  if($there_is_no_vote)
    no_votes();
  else
    $graph->DrawGraph();  
  
}
else // type == pie
{
  require_once("./class.piegraph.php");
  $graph = new Graph();
  
  // You Can Do This too ! in a definition file
  //$graph->LoadGraph(realpath("./piegraph.def"));

  // Configuration Variables ( Change Carefully ! )
  $paddings['left'] = 0 ;
  $paddings['top'] = 20;
  $paddings['right'] = 40;
  $paddings['bottom'] = 10;
  
  if( $choices > 40 )
  {
    $graph->graph_areaheight = 11 * $choices;
    $graph->graph_areawidth = 8 * $choices;
  }
  else
  $graph->graph_areaheight = 250;
  $graph->graph_height = $graph->graph_padding['top'] + (int)$graph->graph_areaheight + $graph->graph_padding['bottom'];
  $graph->graph_areawidth = 350;
  $graph->graph_width = $graph->graph_padding['left'] + (int)$graph->graph_areawidth + $graph->graph_padding['right'];
  $graph->graph_padding = $paddings;
  $graph->graph_title = "ITLPoll Powered";
  $graph->graph_titlecolor = array(255, 255, 255);
  $graph->graph_transparencylevel = 10;
  $graph->graph_titlefont = 5;
  $graph->graph_borderwidth = 1;
  $graph->graph_bordercolor = array($rand1[1], $rand2[1], $rand3[1]);
  $graph->graph_bgcolor = array(194, 200, 234);
  $graph->graph_bgtransparent = false;
  
  $graph->axis_stepY = 10;
    
  $graph->scale_roundY = 0;

  for($i=0; $i<$choices; $i++)
  {
    if($bar[$i+1])
      $data_bars[$i] = $bar[$i+1];
    else
      $bar[$i+1] = 0.1;
  }
  
  $graph->data = $data_bars;

  $graph->pie_total = $total;
  $graph->pie_startoffset = 20;
  for($i=0; $i<$choices; $i++)
    $graph->pie_color[$i] = array($rand1[$i+1], $rand2[$i+1], $rand3[$i+1]);

  $graph->legend_visible = true;
  $graph->legend_floating = false;
  $graph->legend_position = 4;
  $graph->legend_padding = 10;
  for($i=0; $i<$choices; $i++)
    //if($bar[$i+1])
      $graph->legend_data[$i] = $i+1;
  
  for($i=0; $i<$choices; $i++)
    //if($bar[$i+1])
      $graph->legend_color[$i] = array($rand1[$i+1], $rand2[$i+1], $rand3[$i+1]);
  
  // Show Graph
  if($there_is_no_vote)
    no_votes();
  else
  $graph->DrawGraph(); 
}

?> 
