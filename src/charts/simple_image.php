<?php
/* **********************************************************************
*************************************************************************
####    This App Is Written By : Bahrambeigy - ITLPoll Version 3    #####
########       [https://gitlab.com/b.bahrambeigy/itlpoll]        ########
####              This application is free of charge :)             #####
####                Contact me: bahramwhh@gmail.com                 #####
*************************************************************************
#########################################################################
File : image.php (old version)
Writer : Bahrambeigy (bahramwhh@gmail.com)
Description : َUsing this file one can see old 2D bar chart. (It is possible
to change default chart type through admin panel)
**********************************************************************
*/
session_start();
@error_reporting (E_ERROR | E_PARSE);

// Installation Check !
if(filesize("../config.php") == 0)
{
  header("Location: ../install/index.php");
}
require_once("../config.php");

if(!defined("ITLPoll_INSTALLED"))
{
  header("Location: ../install/index.php");
  @exit();
}

// Functions Existance Check
if(!(file_exists("../includes/functions.php")))
{
    echo "Your Functions.php File Doesn't Exist in : includes/functions.php";
    exit;
}
require_once("../includes/functions.php");
db_connect($host, $user, $passwd, $database);
Load_Configs($prefix);

require_once("../language/".$configs['language'].".php");


@$id = filter($_REQUEST['pollID']);

if(empty($id)) $query = "SELECT * FROM ".$prefix."_poll WHERE active = 'yes'";
else {
    // Checking $id For Injection (Bug Founded By Simorgh-ev Security Group)
    if(!preg_match("[1-9]", $id)) {
    echo "Hacking Attempt - You Can't Set A String Value For ID Variable!";
    exit;
    }
    else {
    $query = "SELECT * FROM ".$prefix."_poll WHERE id = '". $id ."'";
     }
}
$poll = mysqli_query($GLOBALS["___mysqli_ston"], $query);
$polllist = mysqli_fetch_array($poll);
$id = $polllist['id'];
$choices = $polllist['choices'];
$active = $polllist['active'];

@$num = filter($_GET['num']);

// Query The Results From Database
$query2 = "SELECT * FROM ".$prefix."_results WHERE id = $id";
$result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
$choice = mysqli_fetch_row($result);
// Query The Choices From Database
$query3 = "select * from ".$prefix."_choices where id = $id";
$cho = mysqli_query($GLOBALS["___mysqli_ston"], $query3);
$choi = mysqli_fetch_row($cho);

if(empty($num)) {

	for($i=1; $i<=$choices; $i++)
    {
      $bar[$i] = $choice[$i];
    }
    for($b=1; $b<=$choices; $b++)
    {
      $total += $bar[$b];
    }
    for($c=1; $c<=$choices; $c++)
    {
      @$per[$c] = $choice[$c] / $total * 100;
    }

    for($i=1; $i<=$choices; $i++)
    {
      $strchoice[$i] = $choi[$i];
    }
    

	// Getting 9 Random Colors ! 
	for($i=1; $i<=$choices; $i++) {
		$rand1[$i] = rand(0, 255);
		$rand2[$i] = rand(0, 255);
		$rand3[$i] = rand(0, 255);
	}

	/* ^^^^^^^^ Dispalying The Image!   ^^^^^^^^^^ */
	
	// Defining Constants - You Can Change These For Customization
	$width=320;
	$right_margin= 50;
	$ch_height = 20;  
	$ch_space = $ch_height/2; 
	$font = './images/tahoma.ttf';
	$x = 20;
	$y = 20;
	$bar_unit = ($width-($x+$right_margin)) / 100;
	$height = $choices * ($ch_height + $ch_space)+45;   
	
	// Creating The Blank Image And Convas
	$im = imagecreate($width,$height);   
	$black=ImageColorAllocate($im,0,0,0);
	$white=ImageColorAllocate($im,255,255,255);   
	ImageFilledRectangle($im,0,0,$width,$height,$white);   
	ImageRectangle($im,0,0,$width-1,$height-1,$black);   
	
	// Entering Data To Image Here 
	for($i=1; $i<=$choices; $i++) {
      $color[$i] = ImageColorAllocate($im, $rand1[$i], $rand2[$i], $rand3[$i]);
	  ImageString($im, 12,  $width-40, $y+($ch_height/3),  round($per[$i]).'%',  $black);  
	  $bar_length = $x + ($per[$i] * $bar_unit);   
	  ImageFilledRectangle($im, $x, $y-2, $bar_length, $y+$ch_height, $color[$i]);   
	  ImageRectangle($im, $bar_length+1, $y-2,  ($x+(100*$bar_unit)), $y+$ch_height, $black);   
	  $y=$y+($ch_height+$ch_space);
	  
	  $_SESSION['ITLRand'][$i] = array($rand1[$i], $rand2[$i], $rand3[$i]);	  
	     
	}
	
	// CopyRight Notices Under The Image
	ImageString($im, 1, $x, $y, "Powered By: ITLPoll V 3.1", $black);
	
	// And Fillay We Show The Image Here
	Header('Content-type:  image/png');
	ImagePng($im);   
	ImageDestroy($im);

}
else {
	$im = imagecreate(15, 15);
	$color = ImageColorAllocate($im, $_SESSION['ITLRand'][$num][0], $_SESSION['ITLRand'][$num][1], $_SESSION['ITLRand'][$num][2]);
	ImageFilledRectangle($im, 0, 0, 15, 15, $color);
	Header('Content-type:  image/png');
	ImagePng($im);
	ImageDestroy($im);
}

// Programmed By : BahramBeigy
?>