<?php
// Sample Config.php

define("ITLPoll_INSTALLED", 1);

// iniset Configs
@ini_set("expose_PHP", "Off");
@ini_set("display_errors", 1);
@ini_set("log_errors", "Off");
@ini_set("html_errors", "Off");
@ini_set("asp_tags", "Off");
@ini_set("register_globals", "Off");
@ini_set("implicit_flush", "On");		

// Connecting to mysql Configs
$host = "localhost"; // Default is Locahost
$user = "yourmysqluser"; // The mysql Admin User !
$passwd = "yourmysqlpassword"; // The mysql Admin Password
$database = "yourmysqldatabase"; // The Database That ITLPoll Install There
$prefix = "itl"; // The String That Will Add Before Each Table
$usejalali = "0" // Use Jalali Date Fomrat? 1=yes, 0=no
?>