<?php
/* **********************************************************************
*************************************************************************
####    This App Is Written By : Bahrambeigy - ITLPoll Version 3    #####
########       [https://gitlab.com/b.bahrambeigy/itlpoll]        ########
####              This application is free of charge :)             #####
####                Contact me: bahramwhh@gmail.com                 #####
*************************************************************************
#########################################################################
File : functions.php
Writer : Bahrambeigy(bahramwhh@gmail.com)
Description : This File Contains The Functions That Used in ITLPoll System
Without This File You Can't Run Poll System.
**********************************************************************
*/

// Access Denied For Direct Showing
if(stristr($_SERVER['SCRIPT_NAME'], "functions.php")) {
    echo "You Can't Access This File Directly - Hacking Attempt";
    @exit();
  }

require_once("template.php");
require_once("dateconvert.php");
require_once("timeago.inc.php");

function Load_Configs($prefix) {
    global $configs;
    $query = "SELECT disabled, language, template, color1, color2, version, numarchive, defaultresult, defaultgraph, maxchoices, total_votes, voters_login, admin_voters_verification FROM ".$prefix."_config";
    $config = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $configs = mysqli_fetch_array($config);
    
    // Now Returns The Configs Variable
    return $configs;
}

function db_connect($host, $user, $passwd, $database)
{
  @$db = ($GLOBALS["___mysqli_ston"] = mysqli_connect($host,  $user,  $passwd));
  if(!$db)
  {
    echo "The Program Can't Connect To mysql Server! - Please Check Your mysql Server.";
    @exit();
  }
  else  mysqli_select_db($GLOBALS["___mysqli_ston"], $database);
  return $db;
}

function file_check($filepath, $error)
{
    if(!(file_exists("./$filepath")))
    {
    echo $error;
    @exit();
    }
}

function XML_Read($itl_fxml, $itl_file)
{
	// Read XML Theme
    $itl_xml = simplexml_load_string($itl_fxml, "SimpleXMLElement", LIBXML_NOCDATA);
    $itl_json = json_encode($itl_xml);
    $itl_array = json_decode($itl_json,TRUE);

    foreach ($itl_array['file'] as $itl_key => $itl_value )
	{
		$thf = $itl_value;
		if ($thf['name'] == $itl_file)
		{
			$ftheme = $thf['content'];
			break;
		}
	}
	return $ftheme;	
}

function Have_Permission($action, $active_user, $host, $user, $passwd, $database, $prefix) {
	
	@$db = ($GLOBALS["___mysqli_ston"] = mysqli_connect($host,  $user,  $passwd));
    if(!$db)
    {
      echo _EMYSQL;
      @exit();
    }
    else  mysqli_select_db($GLOBALS["___mysqli_ston"], $database);
    
	$query = "SELECT permission FROM ".$prefix."_users WHERE username='".$active_user."';";
	$usrs = mysqli_query($GLOBALS["___mysqli_ston"], $query);
	$num_users = mysqli_num_rows($usrs);
	if($num_users <= 0)
	{
		msg(_INVALIDUSER, $destination);
        @exit();
	}
	$user_inf = mysqli_fetch_array($usrs);
	$now_group = $user_inf['permission'];
	
	
	$query = "SELECT can_".$action." FROM ".$prefix."_groups WHERE name='".$now_group."';";
    $grps = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $num_groups = mysqli_num_rows($grps);
    if($num_groups <= 0)
	{
		msg(_NOGROUP, $destination);
        @exit();
	}
	$group_inf = mysqli_fetch_array($grps);
	
	if($group_inf["can_".$action.""] == 'yes')
		return true;
	else
		return false;
	
}

function Show_Results($crtdate, $choices, $title, $itl_fxml, $choice, $choi, $color1, $color2, $pollID, $path="") {
    
	$total=0;
	$output = "";
	for($i=1; $i<=$choices; $i++)
    {
      $bar[$i] = $choice[$i];
    }
    for($b=1; $b<=$choices; $b++)
    {
      $total += $bar[$b];
    }
    for($c=1; $c<=$choices; $c++)
    {
      @$per[$c] = $choice[$c] / $total * 100;
    }
    $max_bar = max($bar);
    $found_max = false;
    $max_strlength = strlen(stripslashes($choi[1]));
    for($i=1; $i<=$choices; $i++)
    {
      $strchoice[$i] = $choi[$i];
      if($bar[$i] == $max_bar && !$found_max)
      {
      	$strchoice[$i] = "<b>" . $strchoice[$i] . "</b>";
      	$found_max = true;
      }
      
      if($max_strlength < strlen(stripslashes($strchoice[$i])))
      	$max_strlength = strlen(stripslashes($strchoice[$i]));
      
    }
    
    $max_strlength = ($max_strlength > 30) ? 150 : $max_strlength*5;
    
    
    $pollID = explode("|", $pollID);
	
	$theme_file = XML_Read($itl_fxml, "htmlresult_choices");
	
	$result_c = new template();
	$result_c->fp = $theme_file;
	$result_c->nvars = array( "title" => stripslashes($title),
				"created date" => $crtdate
				 );
	if(empty($path))
		$imgpath = "images/bar.gif";
	else
		$imgpath = "$path/images/bar.gif";
	
	for($i=1; $i<=$choices; $i++) {
	 	if($i%2 == "0") $color = $color1;
		else $color = $color2;
		
		// percentage increase for template
		if($per[$i] <= 0)
			$per[$i] = 2;
		
		$result_c->lblock["choices"][$i] = array( "strchoice" =>  stripslashes($strchoice[$i]),
							"strlength" => "$max_strlength",
							"percent" => "$per[$i]",
							"bgimage" => $imgpath,
							"bgcolor" => $color,
							"choice" => $bar[$i] );
	}
	
	// Show Results Header if ... 
    if($pollID[0] == 0)
    {
    	if(defined("MULTIPLEACTIVE"))
			$title = _SURVEY;
    
    	$theme_file = XML_Read($itl_fxml, "htmlresult_header");
	    $result_h = new template();
		$result_h->fp = $theme_file;
		$result_h->nvars = array( "title" => stripslashes($title),
		"multiple active string" =>  MULTACTIVESTR );
		$output = $result_h->parse();
		
		if(!defined("MULTIPLEACTIVE"))
			$output = preg_replace("/<multiple_active_section>(.*)<\/multiple_active_section>/is", "", $output);

    }
    
    $output .= $result_c->parse();
    
    // Show Results Footer if ...
    if($pollID[0] == ($pollID[2]-1))
    {
    	//$pollID[3] /= $pollID[2];
    	$theme_file = XML_Read($itl_fxml, "htmlresult_footer");
	    $result_f = new template();
		$result_f->fp = $theme_file;
		$result_f->nvars = array( "totalvoted" => _TOTALVOTED,
				"total" => "$pollID[3]" );
		$output .= $result_f->parse();
    }
	
	// The Program Shows The Results Now!
    if(empty($path)) echo $output;
    else return $output;
}

function Show_ImageResults($pollID, $crtdate, $choices, $title, $itl_fxml, $choice, $choi, $path="", $type = "simple", $sPollID) {
	if(!extension_loaded("gd")) {
		echo "You Can't Use Image Resulting! Because Your PHP-GD Library Isn't Loaded! - Please Contact Your Administrator!";
		@exit();
	}
	$output = "";
	$total = 0;
	
	$title = stripslashes($title);
	
    for($i=1; $i<=$choices; $i++)
    {
      $bar[$i] = $choice[$i];
    }
    for($b=1; $b<=$choices; $b++)
    {
      $total += $bar[$b];
    }
    for($c=1; $c<=$choices; $c++)
    {
      @$per[$c] = $choice[$c] / $total * 100;
    }

    $max_bar = max($bar);
    $found_max = false;
    for($i=1; $i<=$choices; $i++)
    {
      $strchoice[$i] = $choi[$i];
      if($bar[$i] == $max_bar && !$found_max)
      {
		$strchoice[$i] = "<b>" . $strchoice[$i] . "</b>";
		$found_max = true;
      }
    }
    
    $sPollID = explode("|", $sPollID);
    
	
	$theme_file = XML_Read($itl_fxml, "imageresult_choices");
	
	$result_c = new template();
	$result_c->fp = $theme_file;
	
	if($type == "simple")
	{
		if(empty($path))
			$imgpath = "charts/simple_image.php?pollID=$pollID";
		else
			$imgpath = "$path/charts/simple_image.php?pollID=$pollID";
		
		$result_c->nvars = array( "title" => $title,
							"created date" => $crtdate,
							"mainimg" => "<img src=$imgpath>"
							 );	
			
		for($i=1; $i <= $choices; $i++) 
		{
			if(empty($path))
				$smallimg = "charts/simple_image.php?pollID=$pollID&num=$i";
			else
				$smallimg = "$path/charts/simple_image.php?pollID=$pollID&num=$i";
				
			$result_c->lblock["choices"][$i] = array( "strchoice" => stripslashes($strchoice[$i]),
							"colorch" => "<img src=\"$smallimg\">"
							 );
		}
	}
	else
	{
		if($type == "bar")
		{
			if(empty($path))
				$imgpath = "charts/image.php?type=bar&pollID=$pollID&num=$i";
			else
				$imgpath = "$path/charts/image.php?type=bar&pollID=$pollID&num=$i";

			$result_c->nvars = array( "title" => $title,
							"created date" => $crtdate,
							"mainimg" => "<img src=$imgpath>"
							 );	
				
			for($i=1; $i <= $choices; $i++) 
			{
				$strshow = stripslashes($strchoice[$i]) . " ( " . round($per[$i]) . "% ) ";
				$result_c->lblock["choices"][$i] = array( "strchoice" => $strshow,
							"colorch" => "<b> $i </b>" );
			}
		}
		
		else if($type == "pie")
		{
			if(empty($path))
				$imgpath = "charts/image.php?type=pie&pollID=$pollID&num=$i";
			else
				$imgpath = "$path/charts/image.php?type=pie&pollID=$pollID&num=$i";
			
			$result_c->nvars = array( "title" => $title,
							"created date" => $crtdate,
							"mainimg" => "<img src=$imgpath>"
							 );	
				
			for($i=1; $i <= $choices; $i++)
			{ 
				$strshow = stripslashes($strchoice[$i]) . " ( " . round($per[$i]) . "% )  (". $bar[$i] .") ";
				$result_c->lblock["choices"][$i] = array( "strchoice" => $strshow,
							"colorch" => "<b> $i </b>" );
			}
		}
	}
	
	// Show Results Header if ... 
    if($sPollID[0] == 0)
    {
    	if(defined("MULTIPLEACTIVE"))
			$title = _SURVEY;
    
    	$theme_file = XML_Read($itl_fxml, "imageresult_header");
	    $result_h = new template();
		$result_h->fp = $theme_file;
		$result_h->nvars = array( "title" => stripslashes($title),
								  "multiple active string" =>  MULTACTIVESTR );
		$output = $result_h->parse();
		
		if(!defined("MULTIPLEACTIVE"))
			$output = preg_replace("/<multiple_active_section>(.*)<\/multiple_active_section>/is", "", $output);
    }
    
    $output .= $result_c->parse();
    
    // Show Results Footer if ...
    if($sPollID[0] == ($sPollID[2]-1))
    {
    	//$sPollID[3] /= $sPollID[2];
    	$theme_file = XML_Read($itl_fxml, "imageresult_footer");
	    $result_f = new template();
		$result_f->fp = $theme_file;
		$result_f->nvars = array( "totalvoted" => _TOTALVOTED,
				"total" => "$sPollID[3]" );
		$output .= $result_f->parse();
    }

	
    // The Program Shows The Results Now!
    if(empty($path)) echo $output;
    else return $output;
    
}

function Show_Body($crtdate, $choices, $title, $itl_fxml, $choi, $color1, $color2, $multiple, $pollID, $path="") {

	$output = "";
	for($i=1; $i<=$choices; $i++)
    {
      $choice[$i] = $choi[$i];
    }
    for($b=1; $b<$choices; $b++)
    {
      $total =+ $choice[$b];
    }
    
	$pollID = explode("|", $pollID);
	
	
	// Show Choices
    
	$theme_file = XML_Read($itl_fxml, "body_choices");
	
	$body_c = new template();
	$body_c->fp = $theme_file;
		
	$body_c->nvars = array("title" => stripslashes($title),
				"created date" => $crtdate	);
		
    if($multiple == "no")
	{
		for($i=1; $i<=$choices; $i++) 
		{
			$ch = $pollID[1] . "_ch" . $i;
			if($i%2 == "0") $color = $color1;
			else $color = $color2;
			$choice_str = $pollID[1] . "_Choice";
			$body_c->lblock["choices"][$i] = array("chvalue" => $ch,
					  "selected" => ($i==1) ? "checked" : "",
					  "chname" => $choice_str,
					  "chtype" => "radio",
					  "choice" => stripslashes($choice[$i]),
					  "bgcolor" => $color  );
		}
	}
	else
	{
		for($i=1; $i<=$choices; $i++) 
		{
			$ch = $pollID[1] . "_ch" . $i;
			if($i%2 == "0") $color = $color1;
			else $color = $color2;
			$body_c->lblock["choices"][$i] = array("chvalue" => $ch,
					  "selected" => "",
					  "chname" => $ch,
					  "chtype" => "checkbox",
					  "choice" => stripslashes($choice[$i]),
					  "bgcolor" => $color  );
		}
	}
	
		
    // Show Body Header if ...
    if( $pollID[0] == 0 )
    {
    
    	$theme_file = XML_Read($itl_fxml, "body_header");
	
		$body_h = new template();
		$body_h->fp = $theme_file;
	
		$form_destination = '?';
		if(empty($path))
			$form_action = "?Post";	
		else
			$form_action = $path . "/?Post";
		
		if(defined("MULTIPLEACTIVE"))
			$title = _SURVEY;
	
		
		$body_h->nvars = array("title" => stripslashes($title),
				"multiple active string" =>  MULTACTIVESTR,
				"form action" => $form_action,
				"created date" => $crtdate,
				"Form Destination" => $form_destination	);
		
		$output = $body_h->parse();
		
		if(!defined("MULTIPLEACTIVE"))
			$output = preg_replace("/<multiple_active_section>(.*)<\/multiple_active_section>/is", "", $output);
			
		// left menu will show if administrators logged in
		if(ADMIN_LOGGED_IN && empty($path))
		{
			$admin_session = explode("|", $_SESSION['ITLPoll']['Admin']);
			$username = base64_decode(filter($admin_session[0]));	
			
			$logout = _LOGOUT . "<br/>( " . $username . " )";
			
			$admin_links = new template();
			$admin_links->fp = $output;
			$admin_links->nvars = array( "add poll" => _ADDPOLL,
						 "edit poll" => _EDITPOLL,
						 "voters management" => _VOTERSMANAGEMENT,
						 "edit configs" => _EDITCONFIGS,
						 "logout" => $logout,
						 "backlink" => _BACKMAIN,
						 "reset" => _RESET );
			$output = $admin_links->parse();
		}
		else
			$output = preg_replace("/<left_menu_section>(.*)<\/left_menu_section>/is", "", $output);
		
		// left menu will show if voters logged in
		if(isset($_SESSION['Voters_Logged']) && empty($path))
		{
			
			$usname = stripslashes(base64_decode($_SESSION['Voters_Logged']));
			
			$admin_links = new template();
			$admin_links->fp = $output;
			$admin_links->nvars = array( "logout" => _LOGOUT,
										 "voter" => $usname );
			$output = $admin_links->parse();
		}
		else
			$output = preg_replace("/<voters_menu_section>(.*)<\/voters_menu_section>/is", "", $output);
		
    }
    
    $output .= $body_c->parse();
    
    // Show Body Footer
    if($pollID[0] == ($pollID[2]-1) )
    {
    	$theme_file = XML_Read($itl_fxml, "body_footer");
	
		$body_f = new template();
		$body_f->fp = $theme_file;
		
		$body_f->nvars = array("pollarchive" => _POLLARCHIVE,
				"Html Results" => _HTMLRESULTS,
				"Image Results" => _IMGRESULTS,
				"submit" => _SUBMIT );
		$output .= $body_f->parse() ;
    }
	
    
	if(empty($path)) echo $output;
    else return $output;

}

function Show_Archives($itl_fxml, $archive, $numarchive, $color1, $color2, $path="") {
    
    for($i=1; $i<=$numarchive; $i++)
    {
      $ar = mysqli_fetch_row($archive);
      $id[$i] = $ar[0];
      $subject[$i] = $ar[1];

    }
    
    // Optimizing the Database
    ((mysqli_free_result($archive) || (is_object($archive) && (get_class($archive) == "mysqli_result"))) ? true : false);
    
    $theme_file = XML_Read($itl_fxml, "archive");
    
    $archive = new template();
    $archive->fp = $theme_file;
    $archive->nvars = array( "pollarchive" => _POLLARCHIVE,
							 "number" => _NUMBER,
							 "poll question" => _POLLQ );
	
    for($i=1; $i<=$numarchive; $i++) {
        if(empty($id[$i])) break;
        
		if($i%2 == "0") $color = $color1;
		else $color = $color2;
		
		$archive->lblock["archive"][$i] = array( "id" => "$id[$i]",
							 "num" => "$i",
							 "question" => stripslashes($subject[$i]),
							 "bgcolor" => $color );
	}
	$output = $archive->parse();
    
    if(empty($path)) echo $output;
    else return $output;

}

function Show_VotersLogin($itl_fxml) {
	
	$theme_file = XML_Read($itl_fxml, "voters_login");
	
	$voters = new template();
	$voters->fp = $theme_file;
	
	$username_value = "";
	if(isset($_SESSION['KeepEntries']['Username']))
		$username_value = $_SESSION['KeepEntries']['Username'];
	$password_value = "";
	if(isset($_SESSION['KeepEntries']['Password']))
		$password_value = $_SESSION['KeepEntries']['Password'];
	
	$voters->nvars = array( "survey needs logging" => _SURVEYNEEDSLOGGING,
							"register voter account" => _REGISTERVOTER,
							"login details" => _LOGINDETAILS,
							"username email" => _USERNAMEEMAIL,
							"password" => _PASS,
							"image verification" => _IMAGEVERIFICATION,
							"login" => _LOGIN,
							"voters login title" => _VOTERSLOGINTITLE,
							"username value" => $username_value,
							"password value" => $password_value );
	echo $voters->parse();
}

function Show_VotersRegister($itl_fxml) {
	
	$theme_file = XML_Read($itl_fxml, "voters_register");
	
	$voters = new template();
	$voters->fp = $theme_file;
	
	$username_value = "";
	if(isset($_SESSION['KeepEntries']['Username']))
		$username_value = $_SESSION['KeepEntries']['Username'];
	$password_value = "";
	if(isset($_SESSION['KeepEntries']['Password']))
		$password_value = $_SESSION['KeepEntries']['Password'];
	
	$voters->nvars = array( "survey needs logging" => _SURVEYNEEDSLOGGING,
							"register explain" => _REGISTEREXPLAIN,
							"register voter account" => _REGISTERVOTER,
							"register details" => _REGISTERETAILS,
							"username email" => _USERNAMEEMAIL,
							"password first" => _VOTERSPASSWORD,
							"password repeat" => _VOTERSPASSWORD2,
							"image verification" => _IMAGEVERIFICATION,
							"voters login title" => _VOTERSLOGINTITLE,
							"username value" => $username_value,
							"password value" => $password_value );
	echo $voters->parse();
}

function Show_Admin_Header($header) {
  
  $theme_file = XML_Read($header, "header");
  
  $heads = new template();
  $heads->fp = $theme_file;
  $heads->nvars = array( "admintitle" => _ADMIN );
  return Admin_Links( $heads->parse(), 1 );
}

function Show_Admin_Main($admin, $lastlogin, $version) {
   
   	$theme_file = XML_Read($admin, "main");
	   
	$admin_tem = new template();
   	$admin_tem->fp = $theme_file;
   	
   	if(!(file_exists("../install")))
   		$notice = "";
   	else
   		$notice = _DNTREMOVEINST;
   
//   	$new_version_note = "";
//   	$new_version = Check_New_Version($version);
//   	if($new_version == "yes")
//   		$new_version_note = "<b> ". _NEWVERSIONAVAILABLE ." </b>";
//   	else
//   	{
//   		if($new_version == "no")
//   			$new_version_note = "<b> ". _HAVELATESTVERSION ." </b>";
//   		else
//   			$new_version_note = "<b> ". _CANTCHECKLATESTVERSION ." </b>";
//   	}

    $new_version_note = _HAVELATESTVERSION;

   	$admin_session = explode("|", $_SESSION['ITLPoll']['Admin']);
	$username = base64_decode(filter($admin_session[0]));	
   	
   	$short_country_name = "../country_flags/" . get_country_name($lastlogin) . ".png";
   	if($lastlogin != '0.0.0.0' AND $lastlogin != '::1' AND file_exists($short_country_name))
	{
		$comefrom = get_country_name($lastlogin);
		$complete_name = get_country_name($lastlogin, "complete");
		$lastlogin = "<img title='".$lastlogin." - ".$complete_name."' src='../country_flags/" . $comefrom . ".png'/>";
	}
   	
   	$logout = _LOGOUT . " ( " . $username . " )";
	$admin_tem->nvars = array( 	"admintitle" => _ADMIN,
   					"welcome" => _WELCOME,
					"addpoll" => _ADDPOLL,
					"editpoll" => _EDITPOLL,
					"voters management" => _VOTERSMANAGEMENT,
					"edit configs" => _EDITCONFIGS,
					"logout" => $logout,
					"lastlogin" => "Last Login From: $lastlogin",
					"installcheck" => $notice,
					"new version note" => $new_version_note ) ;
   
   echo $admin_tem->parse();
}
function Show_Admin_Footer($footer, $version) {
    
	$theme_file = XML_Read($footer, "footer");
		
	$foot = new template();
	$foot->fp = $theme_file;
	$foot->nvars = array( "footer rights" => _RIGHTS,
			  "version" => $version );
	echo $foot->parse();
}

function Show_Admin_Login($login, $redirect="index.php") {
    
	$theme_file = XML_Read($login, "login");
	
	$log_in = new template();
	$log_in->fp = $theme_file;
	$log_in->nvars = array( "logintitle" => _LOGIN,
				   "username" => _UNAME,
				   "password" => _PASS,
				   "login" => _LOGIN,
				   "reset" => _RESET,
				   "forgetpasswd" => _FORGETPASSWD,
					"redirect value" => $redirect,
					"back to front-end" => _BACKTOUSERSFRONTEND );
	echo $log_in->parse();
}

function Admin_Links($el, $var_output=0) {
	
	$admin_session = explode("|", $_SESSION['ITLPoll']['Admin']);
	$username = base64_decode(filter($admin_session[0]));	
	
	$logout = _LOGOUT . " ( " . $username . " )";
	
	$admin_links = new template();
	$admin_links->fp = $el;
	$admin_links->nvars = array( "add poll" => _ADDPOLL,
				 "edit poll" => _EDITPOLL,
				 "voters management" => _VOTERSMANAGEMENT,
				 "edit configs" => _EDITCONFIGS,
				 "logout" => $logout,
				 "backlink" => _BACKMAIN,
				 "reset" => _RESET,
				 "show front-end" => _SHOWFRONTEND );
	if($var_output)
		return $admin_links->parse();
	else
		echo $admin_links->parse();
}

function Show_Admin_Add($add, $show) {
    
	if($show == "Show")
		$added = '<script> alert("'. _ADDSUCC .'") </script>';
	elseif($show == "DShow")
		$added = "";
	
	$theme_file = XML_Read($add, "add");
	
	$admin_add = new template();
	$admin_add->fp = $theme_file;
	$admin_add->nvars = array( "add poll" => _ADDPOLL,
					"add notice" => _ADDNOTICE,
				   "choice number" => _CHOICENUM,
				   "backlink" => _BACKMAIN,
				   "reset" => _RESET,
				   "continue" => _CONTINUE );
	
	$admin_add->lblock["added"][] = array( "suc_added" => $added );
	
	echo $admin_add->parse();
	
}

function Show_Poll_Add($added, $numchoices, $usejalali="0") {
    
	$theme_file = XML_Read($added, "adding");
	
	if($usejalali == "1")
		$j_str = _JALALINOTE;
	else
		$j_str = "";
	
	$admin_add_poll = new template();
	$admin_add_poll->fp = $theme_file;
	$admin_add_poll->nvars = array( "jalali note" => $j_str,
					"enter question" => _ENTERUQH,
				   	"enter choices" => _ENTERCHOICES,
					"enable multiple" => _ENABLEMULTIPLE,
					"yes" => _YES,
					"no" => _NO,
				   	"date explain" => _DATEEXPLAIN,
				   	"enter expire date" => _ENTEREXPIRE,
				   	"enter start date" => _ENTRSTRDATE,
				   	"start explain" => _STARTDATEEXP,
					"choices number" => $numchoices );
	for($i=1; $i<=$numchoices; $i++) {
		$ch = "ch". $i;
		$admin_add_poll->lblock["choices"][$i] = array( "chvalue" => $ch,
								"choice" => "$i",
								"number" => _NUMBER );
	}
	
	Admin_Links($admin_add_poll->parse());
}

function Show_Admin_Config($config, $show, $host, $user, $passwd, $database, $prefix, $active_user) {
    
    // Saving Varibles!
    @$db = ($GLOBALS["___mysqli_ston"] = mysqli_connect($host,  $user,  $passwd));
    if(!$db)
    {
      echo _EMYSQL;
      @exit();
    }
    else  mysqli_select_db($GLOBALS["___mysqli_ston"], $database);
    
	$query = "SELECT * FROM ".$prefix."_config";
    $configure = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $configs = mysqli_fetch_array($configure);
    ((mysqli_free_result($configure) || (is_object($configure) && (get_class($configure) == "mysqli_result"))) ? true : false);
    
    $query2 = "SELECT * FROM ".$prefix."_users WHERE username='".$active_user."';";
    $mquery2 = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
    $user_inf = mysqli_fetch_array($mquery2);
    ((mysqli_free_result($mquery2) || (is_object($mquery2) && (get_class($mquery2) == "mysqli_result"))) ? true : false);
    
    
    $aname = $user_inf['username'];
    $apasswd = $user_inf['password'];
    $disabled = $configs['disabled'];
    $lang = $configs['language'];
    $template = $configs['template'];
    $color1 = $configs['color1'];
    $color2 = $configs['color2'];
    $aemail = $user_inf['email'];
    $forgot = $user_inf['forgot'];
    $numarchive = $configs['numarchive'];
    $defaultresult = $configs['defaultresult'];
    $defaultgraph = $configs['defaultgraph'];
    
    

    
	if(!($dp = opendir("../language")))
		$language = _LANGDIRREAD ;
	$language = "<select name='language'>";
    while($file = readdir($dp)) {
    if($file !='.' && $file !='..' && $file !='index.html') {
       $file = explode(".", $file);
      if($file[0] == $lang) $language .= "<option selected>$lang</option>";
      else $language .= "<option>$file[0]</option>";
      }
    }
    $language .= "</select>";
    
    if(!($dp = opendir("../templates")))
    	$tem = _TEMPDIRREAD;
    $tem = "<select name='template'>";
    while($dir = readdir($dp)) {
    if($dir !='.' && $dir !='..' && $dir !='index.html') {
      if($dir == $template) $tem .= "<option selected>$template</option>";
      else $tem .= "<option>$dir</option>";
      }
    }
    $tem .= "</select>";
    
    if($show == "Show")
		$saved = '<script> alert("'. _CONFCHSUCC .'");
				 window.location = \'config.php\';
				 </script>';
	elseif($show == "DShow")
		$saved = "";
    
    $defresult = "<select name='defaultresult'>";
	if($defaultresult == "html") $defresult .= "<option selected>html</option><option>image</option>";
	else $defresult .= "<option selected>image</option><option>html</option>";
	$defresult .= "</select>";
	
	$defgraph = "<select name='defaultgraph'>";
	if($defaultgraph == "pie") $defgraph .= "<option selected>pie</option><option>bar</option><option>old</option>";
	else 
	{
		if($defaultgraph == "bar")
			$defgraph .= "<option selected>bar</option><option>pie</option><option>old</option>";
		else
			$defgraph .= "<option selected>old</option><option>pie</option><option>bar</option>";
	}
	
	$defgraph .= "</select>";
    
    $colorhelp1 = '<a onclick=window.open("../includes/color.php?color1","SelectingColor","toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=0,width=240,height=190") href=#>'. _COLORSEL .'</a>';
    $colorhelp2 = '<a onclick=window.open("../includes/color.php?color2","SelectingColor","toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=0,width=240,height=190") href=#>'. _COLORSEL .'</a>';
	
	
	$theme_file = XML_Read($config, "config");
	
	// Parsing Template
	$admin_config = new template();
    $admin_config->fp = $theme_file;
    $admin_config->nvars = array( "admin configs" => _ADMINCONFS,
				  "admin username" => _ADMINNAME,
				  "admin uname value" => $aname,
				  "admin password" => _CHADMINPASS,
				  "change admin pass" => _CHPASS,
				  "disabled" => _DISABLED,
				  "yes" => _YES,
				  "no" => _NO,
				  "language" => _LANG,
				  "languages" => $language,
				  "template" => _TEMPLATE,
				  "templates" => $tem,
				  "password notice" => _PASSNOTICE,
				  "save value" => _SAVE,
				  "defaultresult" => _DEFRESULT,
				  "defaultresult value" => $defresult,
				  "color1" => _COLOR1,
				  "color2" => _COLOR2,
				  "color1 value" => $color1,
				  "color2 value" => $color2,
				  "email" => _EMAIL,
				  "email value" => $aemail,
				  "numarchive" => _NUMARCHIVE,
				  "archives value" => $numarchive,
				  "colorhelp1" => $colorhelp1,
				  "colorhelp2" => $colorhelp2,
				  "defaultresult graph" => _DEFGRAPH,
				  "defaultresult graph value" => $defgraph,
				  "manage users" => _MANAGEUSERS,
				  "manage groups" => _MANAGEGROUP );
	$admin_config->lblock["saved"][] = array( "changed" => $saved );
	
	Admin_Links($admin_config->parse());

	((is_null($___mysqli_res = mysqli_close($db))) ? false : $___mysqli_res);

}

function Show_Change_Pass($pass, $show, $error, $host, $user, $passwd, $database, $prefix) {

    // Reading Password From Database
    @$db = ($GLOBALS["___mysqli_ston"] = mysqli_connect($host,  $user,  $passwd));
    if(!$db)
    {
      echo _EMYSQL;
      @exit();
    }
    else  mysqli_select_db($GLOBALS["___mysqli_ston"], $database);
    $query = "SELECT * FROM ".$prefix."_config";
    $configure = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $configs = mysqli_fetch_row($configure);
    ((mysqli_free_result($configure) || (is_object($configure) && (get_class($configure) == "mysqli_result"))) ? true : false);
    $apasswd = $configs[1];
	
	
	if($show == "Show")
		$changed = '<script> alert("'. _PASSCHSUCC .'") </script>';
	elseif($show == "DShow")
		$changed = "";
		
	if(empty($error))
		$err = "";
	else
		$err = '<script> alert("'. $error .'") </script>';
	
	$theme_file = XML_Read($pass, "changepassword");
	
	// Parsing Template
	$admin_config_pass = new template();
	$admin_config_pass->fp = $theme_file;
	$admin_config_pass->nvars = array( "change password" => _CHPASS,
						 		  	   "old password" => _OLDPASS,
						 		  	   "new password" => _NEWPASS,
						 		  	   "admin password" => _CHADMINPASS,
						 		  	   "new password again" => _NEWPASSAGAIN );
	
	$admin_config_pass->lblock["changed"][] = array( "changed password" => $changed );
	$admin_config_pass->lblock["error"][0] = array( "error" => $err );
	
	Admin_Links($admin_config_pass->parse());
	((is_null($___mysqli_res = mysqli_close($db))) ? false : $___mysqli_res);
}

function Show_Manage_Groups($groups, $show, $error, $host, $user, $passwd, $database, $prefix) {

    @$db = ($GLOBALS["___mysqli_ston"] = mysqli_connect($host,  $user,  $passwd));
    if(!$db)
    {
      echo _EMYSQL;
      @exit();
    }
    else  mysqli_select_db($GLOBALS["___mysqli_ston"], $database);
    $query = "SELECT * FROM ".$prefix."_groups";
    $grps = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $num_groups = mysqli_num_rows($grps);
    
    	
	
	
	if($show == "Show")
		$changed = '<script> alert("'. _GROUPDUPATED .'") </script>';
	elseif($show == "DShow")
		$changed = "";
		
	if(empty($error))
		$err = "";
	else
		$err = '<script> alert("'. $error .'") </script>';
	
	$theme_file = XML_Read($groups, "managegroups");
	
	// Parsing Template
	$admin_group = new template();
	$admin_group->fp = $theme_file;
	$admin_group->nvars = array( "manage groups" => _MANAGEGROUP,
						 		  	   "name" => _NAME,
						 		  	   "can add" => _CANADD,
						 		  	   "can edit" => _CANEDIT,
						 		  	   "can delete" => _CANDELETE,
									   "can config" => _CANCONFIG,
									   "can group" => _CANGROUP,
									   "update" => _UPDATE,
									   "update value" => _UPDATE,
									   "delete" => _DELETE,
									   "delete note" => _DELETENOTE,
									   "reset" => _RESET,
									   "create group" => _CREATEGROUP );
	
	$admin_group->lblock["updated"][] = array( "updated groups" => $changed );
	$admin_group->lblock["error"][0] = array( "error" => $err );
	
	for($i=0; $i < $num_groups; $i++)
	{
		$grps_res = mysqli_fetch_row($grps);
		for($j=1; $j <= 5; $j++)
		{
			if($grps_res[$j] == 'yes')
				$can_values[$j] = "checked";
			else
				$can_values[$j] = "";
		}
		$admin_group->lblock["GroupsBlock"][$i] = array( "name value" => $grps_res[0],
														"can add value" => $can_values[1],
														"can edit value" => $can_values[2],
														"can delete value" => $can_values[3],
														"can config value" => $can_values[4],
														"can group value" => $can_values[5] ); 
	}
	
	Admin_Links($admin_group->parse());
	((mysqli_free_result($grps) || (is_object($grps) && (get_class($grps) == "mysqli_result"))) ? true : false);
	((is_null($___mysqli_res = mysqli_close($db))) ? false : $___mysqli_res);
}

function Show_Manage_Users($users, $show, $error, $host, $user, $passwd, $database, $prefix) {

    @$db = ($GLOBALS["___mysqli_ston"] = mysqli_connect($host,  $user,  $passwd));
    if(!$db)
    {
      echo _EMYSQL;
      @exit();
    }
    else  mysqli_select_db($GLOBALS["___mysqli_ston"], $database);
    $query = "SELECT * FROM ".$prefix."_users";
    $usrs = mysqli_query($GLOBALS["___mysqli_ston"], $query);
    $num_users = mysqli_num_rows($usrs);
    
    $query2 = "SELECT name FROM ".$prefix."_groups";
    $grps = @mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	$num_groups = mysqli_num_rows($grps);
	if($num_groups <= 0 || $num_users <= 0)
		$error = _NOGROUPUSER;
	
	
	if($show == "Show")
		$changed = '<script> alert("'. _USERUPDATED .'") </script>';
	elseif($show == "DShow")
		$changed = "";
		
	if(empty($error))
		$err = "";
	else
		$err = '<script> alert("'. $error .'") </script>';
	
	$theme_file = XML_Read($users, "manageusers");
	
	// Parsing Template
	$admin_user = new template();
	$admin_user->fp = $theme_file;
	$admin_user->nvars = array( "manage users" => _MANAGEUSERS,
						 		  	   "username" => _UNAME,
						 		  	   "change password" => _CHPASS,
						 		  	   "permission" => _PERMISSION,
						 		  	   "block" => _BLOCK,
									   "update" => _UPDATE,
									   "delete" => _DELETE,
									   "delete note" => _DELETENOTE,
									   "reset" => _RESET,
									   "create user" => _CREATEUSER );
	
	$admin_user->lblock["updated"][] = array( "updated users" => $changed );
	$admin_user->lblock["error"][0] = array( "error" => $err );
	
	for($i=0; $i < $num_users; $i++)
	{
		$usrs_res = mysqli_fetch_array($usrs);
		
		$permission = "<select name='permission_".$usrs_res['username']."'>";
		for($j=0; $j < $num_groups; $j++)
		{
			$grps_res = mysqli_fetch_array($grps);
			if($grps_res['name'] == $usrs_res['permission'])
				$permission .= "<option selected>" . $grps_res['name'] . "</option>";
			else
				$permission .= "<option>" . $grps_res['name'] . "</option>";
		}
		$permission .= "</select>";
		
		if($usrs_res['blocked'] == 'yes')
			$blocked = "checked";
		else
			$blocked = "";
		
		$admin_user->lblock["UsersBlock"][$i] = array( "username value" => $usrs_res['username'],
														"permission value" => $permission,
														"block value" => $blocked,
														"update value" => _UPDATE ); 
														
		$grps = @mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	}
	
	Admin_Links($admin_user->parse());
	((mysqli_free_result($usrs) || (is_object($usrs) && (get_class($usrs) == "mysqli_result"))) ? true : false);
	((is_null($___mysqli_res = mysqli_close($db))) ? false : $___mysqli_res);
}

function Show_Create_Group($groups, $show, $error) {

    if($show == "Show")
		$changed = '<script> alert("'. _GROUPCREATED .'") </script>';
	elseif($show == "DShow")
		$changed = "";
		
	if(empty($error))
		$err = "";
	else
		$err = '<script> alert("'. $error .'") </script>';
	
	$theme_file = XML_Read($groups, "creategroup");
	
	// Parsing Template
	$admin_group = new template();
	$admin_group->fp = $theme_file;
	$admin_group->nvars = array( "create group" => _CREATEGROUP,
						 		"name" => _NAME,
						 		"can_add" => _CANADD,
						 		"can_edit" => _CANEDIT,
						 		"can_delete" => _CANDELETE,
								"can_config" => _CANCONFIG,
								"can_group" => _CANGROUP,
								"create" => _CREATE,
								"reset" => _RESET );
	
	$admin_group->lblock["updated"][] = array( "created group" => $changed );
	$admin_group->lblock["error"][0] = array( "error" => $err );
	
	Admin_Links($admin_group->parse());
}

function Show_Create_User($users, $show, $error, $host, $user, $passwd, $database, $prefix) {

    @$db = ($GLOBALS["___mysqli_ston"] = mysqli_connect($host,  $user,  $passwd));
    if(!$db)
    {
      echo _EMYSQL;
      @exit();
    }
    else  mysqli_select_db($GLOBALS["___mysqli_ston"], $database);
        
    $query2 = "SELECT name FROM ".$prefix."_groups";
    $grps = @mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	$num_groups = mysqli_num_rows($grps);
	if($num_groups <= 0)
		$error = _NOGROUP;
	
	$permission_list = "<select name='permission'>";
	for($i=0; $i < $num_groups; $i++)
	{
		$grps_res = @mysqli_fetch_array($grps);
		$permission_list .= "<option>" . $grps_res['name'] . "</option>";
	}
	$permission_list .= "</select>";
	
	if($show == "Show")
		$changed = '<script> alert("'. _USERCREATED .'") </script>';
	elseif($show == "DShow")
		$changed = "";
		
	if(empty($error))
		$err = "";
	else
		$err = '<script> alert("'. $error .'") </script>';
	
	$theme_file = XML_Read($users, "createuser");
	
	// Parsing Template
	$admin_user = new template();
	$admin_user->fp = $theme_file;
	$admin_user->nvars = array( "create user" => _CREATEUSER,
						 		"username" => _UNAME,
						 		"password" => _PASS,
						 		"permission" => _PERMISSION,
						 		"permission value" => $permission_list,
								"block" => _BLOCK,
								"email" => _EMAIL,
								"create" => _CREATE,
								"reset" => _RESET );
	
	$admin_user->lblock["updated"][] = array( "created user" => $changed );
	$admin_user->lblock["error"][0] = array( "error" => $err );
	
	
	Admin_Links($admin_user->parse());
	((mysqli_free_result($grps) || (is_object($grps) && (get_class($grps) == "mysqli_result"))) ? true : false);
	((is_null($___mysqli_res = mysqli_close($db))) ? false : $___mysqli_res);
}

function Show_Edit_Poll($usejalali, $edit, $activeshow, $number, $mquery, $total_pgs, $not_confirmed, $conf_enabled=0, $crr_page="1" ) {
    
	if($activeshow == "Show")
		echo '<script> 
			 alert("'. _ACTSUCC .'");
			 window.location=\'edit.php\';
			 </script>
			 ';
			
	$theme_file = XML_Read($edit, "edit");
	
	// Parsing Template
	$admin_edit = new template();
	$admin_edit->fp = $theme_file;
    	
	
	for($i=1; $i <= $number; $i++) {
	$list = mysqli_fetch_array($mquery);
    	$id = stripslashes($list['id']);
    	$subject = stripslashes($list['subject']);
    	$choices = stripslashes($list['choices']);
    	$multiple = stripslashes($list['multiple']);
    	$active = stripslashes($list['active']);
    	$confirmed = stripslashes($list['confirmed']);
    	$expdate = stripslashes($list['expire']);
    	$strdate = stripslashes($list['start']);
				
    	if($active == "yes" && $confirmed == "no") 
			$act = "<img title=\""._CLICKTODEACTIVATE."\" border=\"0\" src=\"../images/actived_not_confirmed.png\">";
		elseif($active == "yes" && $confirmed == "yes") 
			$act = "<img title=\""._CLICKTODEACTIVATE."\" border=\"0\" src=\"../images/minues.png\">";	
		elseif($active == "no" && $confirmed == "yes") 
			$act = "<img title=\""._CLICKTOACTIVATE."\" border=\"0\" src=\"../images/not_actived_not_confirmed.png\">";
		elseif($active == "no" && $confirmed == "no")
			$act = "<img title=\""._CLICKTOACTIVATE."\" border=\"0\" src=\"../images/plus.png\">";
        
		if($multiple == "yes")
			$multl = '<b>'._YES.'</b>';
		if($multiple == "no")
			$multl = '<b>'._NO.'</b>';	
        
		$edittip = str_replace("%s", "$id", _CLICKTOEDIT);
        
        
		if($usejalali == "1")
		{
			$expdate_explode = explode("-", $expdate);
			$strdate_explode = explode("-", $strdate);
			$expdate_explode = ConvDate($expdate_explode, "H");
			$strdate_explode = ConvDate($strdate_explode, "H");
        	
			if($expdate != "9999-12-31")
				$expdate = implode("-", $expdate_explode);
			if($strdate != "9999-12-31")
				$strdate = implode("-", $strdate_explode);
		}
        
		$admin_edit->lblock["edittable"][$i] = array( "id" => $id,
						"subject" => $subject,
						"edit tip" => $edittip,
						"echoices" => $choices,
						"multiple" => $multl,
						"activeimg" => $act,
						"expire date" => $expdate,
						"start date" => $strdate
						);
		if ( $i == $number )
			$last_id = $id;
		
	}
	
	$next_crr_page = $crr_page + 1;
	$next_link = $next_crr_page;
	$next_image = "../images/forward.png";
	
	if($crr_page > 1)
		$prev_link = $crr_page - 1;
	else
		$prev_link = $crr_page;
		
	$prev_image = "../images/back.png";
	
		
	$current_str = "<b>". $crr_page . "</b>/" . $total_pgs;
	
	if($not_confirmed > 0)
	{
		$title_message = preg_replace("/<br[ \/]*>/is", "", MULTACTIVESTR);
		$not_confirmed_str = '<form id="changing" method="post" action="?type=confirm">
	'._SAVEACTIVEMSG1.'<br>
	<b>'._SAVEACTIVEMSG2.'</b>
	<br>
	'._SAVEACTIVEMSG3.'
	<br><br>
	'._SURVEYTITLE.' <br> <textarea cols="50" rows="5" name="title_message">'. $title_message .'</textarea>
	<br>
	<input type="submit" value="'._SAVECHANGES.'"> <input type="button" value="'._DISCARDCHANGES.'" onclick="window.location=\'?type=confirm&discard=yes\'">';
	}
	
	$conf_enabled_str = "";
	$conf_enabled_easy = "";
	if($conf_enabled == 1)
	{
		$conf_enabled_str = "?ConfEnabled";
		$conf_enabled_easy = "&ConfEnabled";
	}
		
	// easy paging links
	$start_page = $crr_page - 4;
	$end_page = $crr_page + 4;

	if ($start_page <= 0) {
		$end_page -= ($start_page - 1);
		$start_page = 1;
	}

	if ($end_page > $total_pgs)
		$end_page = $total_pgs;
		
	if ($start_page > 1) 
		$pages_links .= " <a href='?type=page&number=1$conf_enabled_easy'>First </a> ...";
	for($i=$start_page; $i<=$end_page; $i++) 
		$pages_links .= "&nbsp;<a href='?type=page&number=$i$conf_enabled_easy'>$i</a>&nbsp;";
	if ($endPage < $total_pgs) 
		$pages_links .= " ...<a href='?type=page&number=$total_pgs$conf_enabled_easy'> Last </a>";
	
	$admin_edit->nvars = array( "edit notice" => _EDITNOTICE,
				"number" => _NUMBER,
				"poll question" => _POLLQ,
				"rchoices" => _CHOICES,
				"ractive" => _ACTIVE,
				"rmultiple" => _ENABLEMULTIPLESHORT,
				"str expire" => _EXPIERE,
				"str start" => _STARTDATE,
				"delete notice" => _DELETENOTICE,
				"delete" => _DELETE,
				"current comment" => $current_str,
				"pages" => _PAGES,
				"nextlink" => $next_link,
				"nextimage" => $next_image,
				"nexttitle" => _NEXT,
				"prevlink" => $prev_link,
				"previmage" => $prev_image,
				"prevtitle" => _PREV,
				"confirm message" => $not_confirmed_str,
				"tip" => _TIP,
				"actived" => _ACTIVED,
				"not actived" => _NOTACTIVED,
				"confirmed" => _CONFIRMED,
				"not confirmed" => _NOTCONFIRMED,
				"conf enabled" => $conf_enabled_str,
				"active and confirm str" => _SHOWACTIVEANDCONFIRMED,
				"pages links" => $pages_links
				);
	((mysqli_free_result($mquery) || (is_object($mquery) && (get_class($mquery) == "mysqli_result"))) ? true : false);	
	
	Admin_Links($admin_edit->parse());

}

function Show_Delete_Poll($num, $show) {
   		
	if($show == "Show") {
		echo '<script> alert("'. _DELSUCC .'"); ';
		echo "window.location = 'edit.php' </script>";
	}
    else 
	{
		echo '
<form id="delete_form" name="delete_form" action="edit.php" method="POST">
<input type="hidden" name="type" id="type" value="delete">
<input type="hidden" name="number" id="number" value="'.$num.'">
<input type="hidden" name="confirm" id="confirm" value="Yes">
</form>
<script>
var answer = confirm("'._CONFIRMDEL.'");
if(answer) 
	window.onload = document.delete_form.submit();
else
	window.location = "edit.php";
</script>
</body>
</html>';
		
	}
}

function Show_Editing_Poll($edit, $choices, $mquery5, $number, $show, $mquery6, $usejalali="0") {
    
	if($show == "Show")
		echo '<script> alert("'. _POLLCHSUCC .'");
		window.location = \'edit.php\' ;
		</script>';
	
	if($usejalali == "1")
		$j_str = _JALALINOTE;
	else
		$j_str = "";		
	
	$polls = @mysqli_fetch_array($mquery5);
	$id = stripslashes($polls['id']);
	$subject = stripslashes($polls['subject']);
	$choices = stripslashes($polls['choices']);
	$multiple = stripslashes($polls['multiple']);
	$expire = stripslashes($polls['expire']);
	$start = stripslashes($polls['start']);

	$expire_expl = explode("-", $expire);
	$start_expl = explode("-", $start);
	
	if($usejalali == "1")
	{
	   	if($expire != "9999-12-31")
			$expire_expl = ConvDate($expire_expl, "H");
       	if($start != "9999-12-31")
		   $start_expl = ConvDate($start_expl, "H");
	}
	

	$mult_yes = "";
	$mult_no = "";
	if($multiple == "yes")
		$mult_yes = "checked";

	else if($multiple == "no")
		$mult_no = "checked";

	$theme_file = XML_Read($edit, "editing");
	
	// Parsing Template
	$admin_editing = new template();
	$admin_editing->fp = $theme_file;
	$admin_editing->nvars = array( "jalali note" => $j_str,
				"id" => $id,
				"question" => _ENTERUQH,
				"question value" => $subject,
				"enable multiple" => _ENABLEMULTIPLE,
				"yes" => _YES,
				"no" => _NO,
				"multiple yes checked" => $mult_yes,
				"multiple no checked" => $mult_no,
				"expire day" => "$expire_expl[2]",
				"expire month" => "$expire_expl[1]",
				"expire year" => "$expire_expl[0]",
				"expire" => _ENTEREXPIRE,
				"date explain" => _DATEEXPLAIN,
				"start explain" => _STARTDATEEXP,
				"enter start date" => _ENTRSTRDATE,
				"start day" => "$start_expl[2]",
				"start month" => "$start_expl[1]",
				"start year" => "$start_expl[0]",
				"number value" => $number,
				"change" => _CHANGE,
				"choices number" => $choices,
				"enter choices" => _ENTERCHOICES,
				"editing poll" => _EDITINGPOLL );
	$chs = @mysqli_fetch_row($mquery6);
	for($i=1; $i<=$choices; $i++) {
	 	$ch = stripslashes($chs[$i]);
		$admin_editing->lblock["choices"][] = array( "number" => _NUMBER . $i,
							"choice number" => "ch". $i,
							 "choice value" => $ch);
	}
	
	
	Admin_Links($admin_editing->parse());

}

function Show_Voters_Management($voters, $voted_voters, $waiting_voters, $voters_enabled="no", $admin_confirmation="no")
{
	$theme_file = XML_Read($voters, "voters");
	
	$admin_voters = new template();
	$admin_voters->fp = $theme_file;
	
	$admin_voters->nvars = array ("voters accounting disabled" => _VOTERSACCOUNTINGDISABLED,
								"enable voters accounting" => _ENABLEVOTERSACCOUNTING,
								"enable disable voters management and confirmation" => _ENABLEDISABLEVOTERMANAGEADMINCONFIRM,
								"disable voter accounting" => _DISABLEVOTERSACCOUNTING,
								"enable disable admin confirmation" => (($admin_confirmation == "yes") ? _DISABLEADMINCONFRIMATION : _ENABLEADMINCONFRIMATION),
								"search explain" => _VOTERMGMSEARCHEXPLAIN,
								"search" => _SEARCH,
								"voters management waiting explain" => _VOTERSMGMWAITINGEXPLAIN,
								"Username" => _USERNAMEEMAIL,
								"enabled" => _ENABLED,
								"verified" => _VERIFIED,
								"latest voters" => _LATESTPARTVOTERS,
								"total votes" =>  _TOTALVOTES,
								"Last Login Date" => _LASTLOGINDATE,
								"Last Voted Date" => _LASTVOTEDDATE,
								"Last Vote From" => _LASTVOTEFROM);
	
	if($voters_enabled == "yes")
	{
		// remove voters disabled section
		$admin_voters->replace("disabled_voters_section", "");
	
		$num_voted = mysqli_num_rows($voted_voters);
		
		for($i=0; $i < $num_voted; $i++)
		{
			$voteds = mysqli_fetch_array($voted_voters);
		
			$comefrom = $voteds['last_voted_ip'];
			
			$short_country_name = "../country_flags/" . get_country_name($comefrom) . ".png";
			
			if($comefrom == '0.0.0.0')
				$comefrom = "Not voted yet";
			
			else if($comefrom != '0.0.0.0' AND $comefrom != '::1' AND file_exists($short_country_name))
			{
				$comefrom = get_country_name($comefrom);
				$complete_name = get_country_name($voteds['last_voted_ip'], "complete");
				$comefrom = "<img title='".$voteds['last_voted_ip']." - ".$complete_name."' src='../country_flags/" . $comefrom . ".png'/>";
			}
			else
			{
				$comefrom = "Unknown!";
			}
				
			$last_voted_date = $voteds['last_voted_date'];
			$last_voted_login = $voteds['last_login_date'];
			
			$today = date("Y-m-d");
			
			if($last_voted_date == '9999-12-31')
				$last_voted_date = "Never";
			else
				$last_voted_date = timeAgoInWords($last_voted_date) . " ago";
				
			if($last_voted_login == '9999-12-31')
				$last_voted_login = "Never";
			
			$admin_voters->lblock["voted_voters"][$i] = array ("vv username" => $voteds['username'],
																"vv total votes" => $voteds['total_votes'],
																"vv last login date" => $last_voted_login,
																"vv last voted date" => $last_voted_date,
																"vv last voted from" => $comefrom);
		}
		
		$num_waiting = mysqli_num_rows($waiting_voters);
		for($i=0; $i < $num_waiting; $i++)
		{
			$waiting = mysqli_fetch_array($waiting_voters);
			$username_link = "<a href='?type=enable&username=". $waiting['username'] . "'>" . $waiting['username'] . "</a>";
			$admin_voters->lblock["waiting_voters"][$i] = array ("wv username" => $username_link,
																"wv enabled" => $waiting['enabled'],
																"wv verified" => $waiting['verified'] );
		}
		
		$no_waiting = "<p align=\"center\"><b>"._VOTERNOWAITINGUSER."</b></p>";
		$no_votes = "<p align=\"center\"><b>"._VOTERTHEREISNOVOTEATALL."</b></p>";
		if($num_waiting < 1)
			$admin_voters->replace("waiting_voters_block", $no_waiting);
		if($num_voted < 1)
			$admin_voters->replace("voted_voters_block", $no_votes);
	}
	else if($voters_enabled == "no")
	{
		// remove voters enabled section
		$admin_voters->replace("enabled_voters_section", "");
		
	}
		
	Admin_Links($admin_voters->parse());
}

function Show_Voters_Edit($voters, $v_details)
{
	$theme_file = XML_Read($voters, "voters_edit");
	
	$admin_voters = new template();
	$admin_voters->fp = $theme_file;
	
	$admin_voters->nvars = array("edit voter profile" => _EDITVOTERPROFILE,
								"Username" => _USERNAMEEMAIL,
								"enabled" => _ENABLED,
								"verified" => _VERIFIED,
								"password" => _PASS,
								"total votes" =>  _TOTALVOTES,
								"yes" => _YES,
								"no" => _NO,
								"remove account" => _REMOVEACCOUNT,
								"update account" => _UPDATEACCOUNT);
	
	$details = mysqli_fetch_array($v_details);
	
	$enabled_checked_yes = "";
	$enabled_checked_no = "";
	if($details['enabled'] == "yes")
	{
		$enabled_checked_yes = "checked";
		$enabled_checked_no = "";
	}
	else
	{
		$enabled_checked_no = "checked";
		$enabled_checked_yes = "";
	}
	
	$verified_checked_yes = "";
	$verified_checked_no = "";
	if($details['enabled'] == "yes")
	{
		$verified_checked_yes = "checked";
		$verified_checked_no = "";
	}
	else
	{
		$verified_checked_no = "checked";
		$verified_checked_yes = "";
	}
	
	$admin_voters->lblock["voters_section"][] = array ("v username" => $details['username'],
														"v total votes" => $details['total_votes'],
														"v enabled checked yes" => $enabled_checked_yes,
														"v enabled checked no" => $enabled_checked_no,
														"v verified checked yes" => $verified_checked_yes,
														"v verified checked no" => $verified_checked_no);
	
	Admin_Links($admin_voters->parse());
	
}

function get_country_name($ip, $type="iso")
{
	require_once("../includes/geoip.inc");
	
	$version = (strpos($ip, ":") === false) ? 4 : 6; // IP version
	if($version == 4)
	{
	  $gi = geoip_open("../includes/GeoIPv4.dat",GEOIP_STANDARD);
	  if($type == "iso")
		  $country = geoip_country_code_by_addr($gi, $ip);
	  else if($type == "complete")
		  $country = geoip_country_name_by_addr($gi, $ip);
	}
	else if($version == 6)
	{
	  $gi = geoip_open("../includes/GeoIPv6.dat",GEOIP_STANDARD);
	  if($type == "iso")
		  $country = geoip_country_code_by_addr_v6($gi, $ip);
	  else if($type == "complete")
		  $country = geoip_country_name_by_addr_v6($gi, $ip);
	}
	geoip_close($gi);
	if($type == "iso") return strtolower($country);
	else return $country;
	
}

function Admin_Password_Forget($forget) {
    
    $theme_file = XML_Read($forget, "forget");
	
	// Parsing Template
	$admin_pass = new template();
	$admin_pass->fp = $theme_file;
	$admin_pass->nvars = array( "forgettitle" => _FORGET,
				"username" => _UNAME,
				"email" => _EMAIL,
				"resetpass" => _RESETPASS,
				"reset" => _RESET );
	
	echo $admin_pass->parse();
}

function get_random() {
  $str = array("itl", "poll", "pass", "changed");
  $size = count($str);
  srand((double) microtime() * 1000000);
  $rand_location = rand(0, $size-1);
  $word = $str[$rand_location];
  $word = trim($word);
  srand ((double) microtime() * 1000000);
  $rand_number = rand(0, 999);
  $word .= $rand_number;
  return $word;
}

function Time_Valid($last, $now) {
	$last_time = explode("|", $last);
	$now_time = explode("|", $now);
	
	if($now_time[0] == "00") $now_time[0] = "24";
	if($last_time[0] == "00") $last_time[0] = "24";
	
	if($now_time[0] > $last_time[0])
		return FALSE;
	else if($now_time[0] < $last_time[0])
		return TRUE;
	else
	{
		if($now_time[1] > $last_time[1])
			return FALSE;
		else if($now_time[1] <= $last_time[1])
			return TRUE;
	}
	
}

function msg($msg, $src) {
	echo '<script> alert("'. $msg .'"); 
	window.location = "'. $src .'";
	</script>';
}

function filter($str) {
	return htmlspecialchars(addslashes($str));
}

function Expiration_Check($expire) {

	// Expiration Check Fixed In Version 2.6
	$nowdate['day'] = date("d");
	$nowdate['month'] = date("m");
	$nowdate['year'] = date("Y");

	if($expire != "9999-12-31") {
	
		$exit_code = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">' . _EXPIERED;
	
		$expired = explode('-', $expire);
		
		if($nowdate['year'] == $expired[0]) {
			if($nowdate['month'] == $expired[1]) {
				if($nowdate['day'] == $expired[2]) { }
				elseif($nowdate['day'] < $expired[2]) { }
				else {
					setcookie("ITLPoll_Voted", "");
					echo $exit_code;
					@exit();
				}			
			}
			elseif($nowdate['month'] < $expired[1]) { }
			else {
				setcookie("ITLPoll_Voted", "");
				echo $exit_code;
				@exit();		
			}
				
		}
		elseif($nowdate['year'] < $expired[0]) { }
		else {
			setcookie("ITLPoll_Voted", "");
			echo $exit_code;
			@exit();
		}
	
	}
	// End Of Expiration Check
}

function StartDate_Check($strdate)
{
	$nowdate['day'] = date("d");
	$nowdate['month'] = date("m");
	$nowdate['year'] = date("Y");

	if($strdate != "9999-12-31") {
	
		$exit_code = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">' . _NOTSTARTED;
	
		$started = explode('-', $strdate);
		
		if($nowdate['year'] == $started[0]) {
			if($nowdate['month'] == $started[1]) {
				if($nowdate['day'] == $started[2]) { }
				elseif($nowdate['day'] > $started[2]) { }
				else {
					echo $exit_code;
					@exit();
				}			
			}
			elseif($nowdate['month'] > $started[1]) { }
			else {
				echo $exit_code;
				@exit();		
			}
				
		}
		elseif($nowdate['year'] > $started[0]) { }
		else {
			echo $exit_code;
			@exit();
		}
	
	}	
}



// Converter Date Converter. Added in version 2.7
function ConvDate($date, $type)
{
	
	$Converter = new Converter;
	
	if($type == "H")
		$converted = $Converter->GregorianToJalali($date[0], $date[1], $date[2]);
	elseif($type == "G")
		$converted = $Converter->JalaliToGregorian($date[0], $date[1], $date[2]);
	
	return $converted;
	
}

function Check_New_Version($here_version)
{
	$version_resource = array("main" => "http://www.itlpoll.com/?Get_Version"); // that's enough one link ;)S
							  //"support" => "http://itlpoll.sourceforge.net/?Get_Version");
	
	$here_version = explode(" ", $here_version);
	$here_version_num = explode(".", $here_version[2]);
	$here_version_grade = strtolower($here_version[4]);
		
	if ($fp = @fopen($version_resource["main"], 'r')) 
	{
		$now_version = "";
		while (!feof($fp)) 
		{
			$now_version .= fgets($fp, 12);
		}
		fclose($fp);		
	}
	else
	{
		if ($fp = @fopen($version_resource["support"], 'r')) 
		{
			$now_version = "";
			while (!feof($fp)) 
			{
				$now_version .= fgets($fp, 12);
			}
			fclose($fp);
		}
		else
		{
			return "error";
		}
	}
	
	
	$now_version_xpl = explode(" ", $now_version);
	$now_version_num = $now_version_xpl[0];
		
	$now_version_num = explode(".", $now_version_num);
	
	if($now_version_num[0] > $here_version_num[0])
		return "yes";
	else
	{
		if($now_version_num[0] == $here_version_num[0])
		{
			if($now_version_num[1] > $here_version_num[1])
				return "yes";
			else
			{
				if($now_version_num[1] == $here_version_num[1])
				{
					if($now_version_num[2] > $here_version_num[2])
						return "yes";
					else 
						return "no";
					
				}
				else 
					return "no";
			}
		}
		else 
			return "no";
	}
}

function Changed_Last_Poll($last_poll_date, $user_last_voted="cookie") {

	if($user_last_voted == "cookie")
		$user_last_voted = $_COOKIE['ITLPoll_Voted'];
		
	$user_last_voted_datetime = explode(" ", $user_last_voted);	
	$user_last_voted_date = explode("-", $user_last_voted_datetime[0]); // user date
	$user_last_voted_time = explode(":", $user_last_voted_datetime[1]); // user time
	
	$current_poll_datetime = explode(" ", $last_poll_date);
	$current_poll_date = explode("-", $current_poll_datetime[0]); // last poll date
	$current_poll_time = explode(":", $current_poll_datetime[1]); // last poll time
	
	$is_changed = false;
	
	if($current_poll_datetime[0] != "9999-12-31") {


		if($user_last_voted_date[0] == $current_poll_date[0]) { 
			if($user_last_voted_date[1] == $current_poll_date[1]) { 
				if($user_last_voted_date[2] == $current_poll_date[2]) {
					// check time is bigger or greater ?!
					if($user_last_voted_time[0] == "00") $user_last_voted_time = "12";
					if($current_poll_time[0] == "00") $current_poll_time[0] = "12";
					
					$user_last_voted_tt = implode("", $user_last_voted_time);
					$current_poll_tt = implode("", $current_poll_time);
					
					if($user_last_voted_tt < $current_poll_tt)
						$is_changed = true;
				}
				elseif($user_last_voted_date[2] > $current_poll_date[2]) { }
				else {
					$is_changed = true;
				}			
			}
			elseif($user_last_voted_date[1] > $current_poll_date[1]) { }
			else {
				$is_changed = true;	
			}
				
		}
		elseif($user_last_voted_date[1] > $current_poll_date[0]) { }
		else {
			$is_changed = true;
		}
	
	}
	
	return $is_changed;
}

function getRealIpAddress() {

	if (!empty($_SERVER['HTTP_CLIENT_IP']))
	//check ip from share internet
	{
		$ip=$_SERVER['HTTP_CLIENT_IP'];
	}

	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
	//to check ip is pass from proxy
	{
		$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	}

	else
	{
		$ip=$_SERVER['REMOTE_ADDR'];
	}

	return $ip;
}

function captchaVerification($userstring) {
	$string = strtoupper($_SESSION['ITLPoll_captcha_string']);
	//$userstring = strtoupper($_POST['ITLPoll_user_captcha_string']);
	session_destroy();   

	if (($string == $userstring) && (strlen($string) > 4)) {
		return TRUE;
	} else {
		return FALSE;
	}
}

function increase_admin_time($time, $much)
{
	$admin_time = explode("|", $time);
	$admin_time_hour = $admin_time[0];
	$admin_time_min = $admin_time[1];
	
	if($admin_time_hour == 24) $admin_time_hour = "00";
	
	if( (60 - $admin_time_min) > $much)
		$admin_time_min += $much;
	else
	{
		$difference = (60 - $admin_time_min);
		$admin_time_min = ($much - $difference);
		$admin_time_hour += 1;
	}
	if($admin_time_hour == 24) $admin_time_hour = "00";
	
	if($admin_time_min < 10) $admin_time_min = "0" . $admin_time_min;
	
	$admin_time_return = $admin_time_hour . "|" . $admin_time_min;
	return $admin_time_return;
}

?>