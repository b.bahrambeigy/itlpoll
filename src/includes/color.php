<html STYLE="width: 238px; height: 187px"><head><title>Color Selector</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="fa">
<script language="javascript">

function _CloseOnEsc() {
  if (event.keyCode == 27) { window.close(); return; }
}

function View(color) {                  // preview color
  document.all.ColorPreview.style.backgroundColor = '#' + color;
  document.all.ColorHex.value = '#' + color;
<?php
if(isset($_REQUEST['color1']))  
echo "window.opener.document.all.color1.value = '#' + color;";
else if ( isset($_REQUEST['color2']) )
echo "window.opener.document.all.color2.value = '#' + color;";
?>
}

function Set(string) {                   // select color
  color = ValidateColor(string);
  if (color == null) { alert("Invalid color code: " + string); }        // invalid color
  else {                                                                // valid color
    View(color);                          // show selected color
    window.returnValue = color;           // set return value
    
  }
}

function ValidateColor(string) {                // return valid color code
  string = string || '';
  string = string + "";
  string = string.toUpperCase();
  chars = '0123456789ABCDEF';
  out   = '';

  for (i=0; i<string.length; i++) {             // remove invalid color chars
    schar = string.charAt(i);
    if (chars.indexOf(schar) != -1) { out += schar; }
  }
    
  if (out.length != 6) { return null; }            // check length
  return out;
} 

</script>
</head>
<body bgcolor="#000000" topmargin=0 leftmargin=0>

<form method=POST onSubmit="Set(document.all.ColorHex.value); return false;" action="config.php">

<table border=0 cellspacing=0 cellpadding=4 width=100%>
 <tr>
  <td bgcolor="buttonface" valign=center><div style="background-color: #000000; padding: 1; height: 21px; width: 50px"><div id="ColorPreview" style="height: 100%; width: 100%"></div></div></td>
  <td bgcolor="buttonface" valign=center><input type="text" name="ColorHex" value="" size=15 style="font-size: 12px"></td>
  <td bgcolor="buttonface" width=100%>&nbsp;</td>
 </tr>
</table>

<table border=0 cellspacing=1 cellpadding=0 bgcolor=#000000 style="cursor: hand;">
<tr>
<td bgcolor=#000000 onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#000000 onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#000000 onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#000000 onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#003300 onClick=Set('003300') height=10 width=10></td>
<td bgcolor=#006600 onClick=Set('006600') height=10 width=10></td>
<td bgcolor=#009900 onClick=Set('009900') height=10 width=10></td>
<td bgcolor=#00CC00 onClick=Set('00CC00') height=10 width=10></td>
<td bgcolor=#00FF00 onClick=Set('00FF00') height=10 width=10></td>
<td bgcolor=#330000 onClick=Set('330000') height=10 width=10></td>
<td bgcolor=#333300 onClick=Set('333300') height=10 width=10></td>
<td bgcolor=#336600 onClick=Set('336600') height=10 width=10></td>
<td bgcolor=#339900 onClick=Set('339900') height=10 width=10></td>
<td bgcolor=#33CC00 onClick=Set('33CC00') height=10 width=10></td>
<td bgcolor=#33FF00 onClick=Set('33FF00') height=10 width=10></td>
<td bgcolor=#660000 onClick=Set('660000') height=10 width=10></td>
<td bgcolor=#663300 onClick=Set('663300') height=10 width=10></td>
<td bgcolor=#666600 onClick=Set('666600') height=10 width=10></td>
<td bgcolor=#669900 onClick=Set('669900') height=10 width=10></td>
<td bgcolor=#66CC00 onClick=Set('66CC00') height=10 width=10></td>
<td bgcolor=#66FF00 onClick=Set('66FF00') height=10 width=10></td>
</tr>
<tr>
<td bgcolor=#000000 onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#333333 onClick=Set('333333') height=10 width=10></td>
<td bgcolor=#000000 onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#000033 onClick=Set('000033') height=10 width=10></td>
<td bgcolor=#003333 onClick=Set('003333') height=10 width=10></td>
<td bgcolor=#006633 onClick=Set('006633') height=10 width=10></td>
<td bgcolor=#009933 onClick=Set('009933') height=10 width=10></td>
<td bgcolor=#00CC33 onClick=Set('00CC33') height=10 width=10></td>
<td bgcolor=#00FF33 onClick=Set('00FF33') height=10 width=10></td>
<td bgcolor=#330033 onClick=Set('330033') height=10 width=10></td>
<td bgcolor=#333333 onClick=Set('333333') height=10 width=10></td>
<td bgcolor=#336633 onClick=Set('336633') height=10 width=10></td>
<td bgcolor=#339933 onClick=Set('339933') height=10 width=10></td>
<td bgcolor=#33CC33 onClick=Set('33CC33') height=10 width=10></td>
<td bgcolor=#33FF33 onClick=Set('33FF33') height=10 width=10></td>
<td bgcolor=#660033 onClick=Set('660033') height=10 width=10></td>
<td bgcolor=#663333 onClick=Set('663333') height=10 width=10></td>
<td bgcolor=#666633 onClick=Set('666633') height=10 width=10></td>
<td bgcolor=#669933 onClick=Set('669933') height=10 width=10></td>
<td bgcolor=#66CC33 onClick=Set('66CC33') height=10 width=10></td>
<td bgcolor=#66FF33 onClick=Set('66FF33') height=10 width=10></td>
</tr>
<tr>
<td bgcolor=#000000 onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#666666 onClick=Set('666666') height=10 width=10></td>
<td bgcolor=#000000 onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#000066 onClick=Set('000066') height=10 width=10></td>
<td bgcolor=#003366 onClick=Set('003366') height=10 width=10></td>
<td bgcolor=#006666 onClick=Set('006666') height=10 width=10></td>
<td bgcolor=#009966 onClick=Set('009966') height=10 width=10></td>
<td bgcolor=#00CC66 onClick=Set('00CC66') height=10 width=10></td>
<td bgcolor=#00FF66 onClick=Set('00FF66') height=10 width=10></td>
<td bgcolor=#330066 onClick=Set('330066') height=10 width=10></td>
<td bgcolor=#333366 onClick=Set('333366') height=10 width=10></td>
<td bgcolor=#336666 onClick=Set('336666') height=10 width=10></td>
<td bgcolor=#339966 onClick=Set('339966') height=10 width=10></td>
<td bgcolor=#33CC66 onClick=Set('33CC66') height=10 width=10></td>
<td bgcolor=#33FF66 onClick=Set('33FF66') height=10 width=10></td>
<td bgcolor=#660066 onClick=Set('660066') height=10 width=10></td>
<td bgcolor=#663366 onClick=Set('663366') height=10 width=10></td>
<td bgcolor=#666666 onClick=Set('666666') height=10 width=10></td>
<td bgcolor=#669966 onClick=Set('669966') height=10 width=10></td>
<td bgcolor=#66CC66 onClick=Set('66CC66') height=10 width=10></td>
<td bgcolor=#66FF66 onClick=Set('66FF66') height=10 width=10></td>
</tr>
<tr>
<td bgcolor=#000000 onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#999999 onClick=Set('999999') height=10 width=10></td>
<td bgcolor=#000000 onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#000099 onClick=Set('000099') height=10 width=10></td>
<td bgcolor=#003399 onClick=Set('003399') height=10 width=10></td>
<td bgcolor=#006699 onClick=Set('006699') height=10 width=10></td>
<td bgcolor=#009999 onClick=Set('009999') height=10 width=10></td>
<td bgcolor=#00CC99 onClick=Set('00CC99') height=10 width=10></td>
<td bgcolor=#00FF99 onClick=Set('00FF99') height=10 width=10></td>
<td bgcolor=#330099 onClick=Set('330099') height=10 width=10></td>
<td bgcolor=#333399 onClick=Set('333399') height=10 width=10></td>
<td bgcolor=#336699 onClick=Set('336699') height=10 width=10></td>
<td bgcolor=#339999 onClick=Set('339999') height=10 width=10></td>
<td bgcolor=#33CC99 onClick=Set('33CC99') height=10 width=10></td>
<td bgcolor=#33FF99 onClick=Set('33FF99') height=10 width=10></td>
<td bgcolor=#660099 onClick=Set('660099') height=10 width=10></td>
<td bgcolor=#663399 onClick=Set('663399') height=10 width=10></td>
<td bgcolor=#666699 onClick=Set('666699') height=10 width=10></td>
<td bgcolor=#669999 onClick=Set('669999') height=10 width=10></td>
<td bgcolor=#66CC99 onClick=Set('66CC99') height=10 width=10></td>
<td bgcolor=#66FF99 onClick=Set('66FF99') height=10 width=10></td>
</tr>
<tr>
<td bgcolor=#000000 onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#CCCCCC onClick=Set('CCCCCC') height=10 width=10></td>
<td bgcolor=#000000 onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#0000CC onClick=Set('0000CC') height=10 width=10></td>
<td bgcolor=#0033CC onClick=Set('0033CC') height=10 width=10></td>
<td bgcolor=#0066CC onClick=Set('0066CC') height=10 width=10></td>
<td bgcolor=#0099CC onClick=Set('0099CC') height=10 width=10></td>
<td bgcolor=#00CCCC onClick=Set('00CCCC') height=10 width=10></td>
<td bgcolor=#00FFCC onClick=Set('00FFCC') height=10 width=10></td>
<td bgcolor=#3300CC onClick=Set('3300CC') height=10 width=10></td>
<td bgcolor=#3333CC onClick=Set('3333CC') height=10 width=10></td>
<td bgcolor=#3366CC onClick=Set('3366CC') height=10 width=10></td>
<td bgcolor=#3399CC onClick=Set('3399CC') height=10 width=10></td>
<td bgcolor=#33CCCC onClick=Set('33CCCC') height=10 width=10></td>
<td bgcolor=#33FFCC onClick=Set('33FFCC') height=10 width=10></td>
<td bgcolor=#6600CC onClick=Set('6600CC') height=10 width=10></td>
<td bgcolor=#6633CC onClick=Set('6633CC') height=10 width=10></td>
<td bgcolor=#6666CC onClick=Set('6666CC') height=10 width=10></td>
<td bgcolor=#6699CC onClick=Set('6699CC') height=10 width=10></td>
<td bgcolor=#66CCCC onClick=Set('66CCCC') height=10 width=10></td>
<td bgcolor=#66FFCC onClick=Set('66FFCC') height=10 width=10></td>
</tr>
<tr>
<td bgcolor=#000000 onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#FFFFFF onClick=Set('FFFFFF') height=10 width=10></td>
<td bgcolor=#000000 onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#0000FF onClick=Set('0000FF') height=10 width=10></td>
<td bgcolor=#0033FF onClick=Set('0033FF') height=10 width=10></td>
<td bgcolor=#0066FF onClick=Set('0066FF') height=10 width=10></td>
<td bgcolor=#0099FF onClick=Set('0099FF') height=10 width=10></td>
<td bgcolor=#00CCFF onClick=Set('00CCFF') height=10 width=10></td>
<td bgcolor=#00FFFF onClick=Set('00FFFF') height=10 width=10></td>
<td bgcolor=#3300FF onClick=Set('3300FF') height=10 width=10></td>
<td bgcolor=#3333FF onClick=Set('3333FF') height=10 width=10></td>
<td bgcolor=#3366FF onClick=Set('3366FF') height=10 width=10></td>
<td bgcolor=#3399FF onClick=Set('3399FF') height=10 width=10></td>
<td bgcolor=#33CCFF onClick=Set('33CCFF') height=10 width=10></td>
<td bgcolor=#33FFFF onClick=Set('33FFFF') height=10 width=10></td>
<td bgcolor=#6600FF onClick=Set('6600FF') height=10 width=10></td>
<td bgcolor=#6633FF onClick=Set('6633FF') height=10 width=10></td>
<td bgcolor=#6666FF onClick=Set('6666FF') height=10 width=10></td>
<td bgcolor=#6699FF onClick=Set('6699FF') height=10 width=10></td>
<td bgcolor=#66CCFF onClick=Set('66CCFF') height=10 width=10></td>
<td bgcolor=#66FFFF onClick=Set('66FFFF') height=10 width=10></td>
</tr>
<tr>
<td bgcolor=#000000  onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#FF0000  onClick=Set('FF0000') height=10 width=10></td>
<td bgcolor=#000000  onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#990000  onClick=Set('990000') height=10 width=10></td>
<td bgcolor=#993300  onClick=Set('993300') height=10 width=10></td>
<td bgcolor=#996600  onClick=Set('996600') height=10 width=10></td>
<td bgcolor=#999900  onClick=Set('999900') height=10 width=10></td>
<td bgcolor=#99CC00  onClick=Set('99CC00') height=10 width=10></td>
<td bgcolor=#99FF00  onClick=Set('99FF00') height=10 width=10></td>
<td bgcolor=#CC0000  onClick=Set('CC0000') height=10 width=10></td>
<td bgcolor=#CC3300  onClick=Set('CC3300') height=10 width=10></td>
<td bgcolor=#CC6600  onClick=Set('CC6600') height=10 width=10></td>
<td bgcolor=#CC9900  onClick=Set('CC9900') height=10 width=10></td>
<td bgcolor=#CCCC00  onClick=Set('CCCC00') height=10 width=10></td>
<td bgcolor=#CCFF00  onClick=Set('CCFF00') height=10 width=10></td>
<td bgcolor=#FF0000  onClick=Set('FF0000') height=10 width=10></td>
<td bgcolor=#FF3300  onClick=Set('FF3300') height=10 width=10></td>
<td bgcolor=#FF6600  onClick=Set('FF6600') height=10 width=10></td>
<td bgcolor=#FF9900  onClick=Set('FF9900') height=10 width=10></td>
<td bgcolor=#FFCC00  onClick=Set('FFCC00') height=10 width=10></td>
<td bgcolor=#FFFF00  onClick=Set('FFFF00') height=10 width=10></td>
</tr>
<tr>
<td bgcolor=#000000  onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#00FF00  onClick=Set('00FF00') height=10 width=10></td>
<td bgcolor=#000000  onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#990033  onClick=Set('990033') height=10 width=10></td>
<td bgcolor=#993333  onClick=Set('993333') height=10 width=10></td>
<td bgcolor=#996633  onClick=Set('996633') height=10 width=10></td>
<td bgcolor=#999933  onClick=Set('999933') height=10 width=10></td>
<td bgcolor=#99CC33  onClick=Set('99CC33') height=10 width=10></td>
<td bgcolor=#99FF33  onClick=Set('99FF33') height=10 width=10></td>
<td bgcolor=#CC0033  onClick=Set('CC0033') height=10 width=10></td>
<td bgcolor=#CC3333  onClick=Set('CC3333') height=10 width=10></td>
<td bgcolor=#CC6633  onClick=Set('CC6633') height=10 width=10></td>
<td bgcolor=#CC9933  onClick=Set('CC9933') height=10 width=10></td>
<td bgcolor=#CCCC33  onClick=Set('CCCC33') height=10 width=10></td>
<td bgcolor=#CCFF33  onClick=Set('CCFF33') height=10 width=10></td>
<td bgcolor=#FF0033  onClick=Set('FF0033') height=10 width=10></td>
<td bgcolor=#FF3333  onClick=Set('FF3333') height=10 width=10></td>
<td bgcolor=#FF6633  onClick=Set('FF6633') height=10 width=10></td>
<td bgcolor=#FF9933  onClick=Set('FF9933') height=10 width=10></td>
<td bgcolor=#FFCC33  onClick=Set('FFCC33') height=10 width=10></td>
<td bgcolor=#FFFF33  onClick=Set('FFFF33') height=10 width=10></td>
</tr>
<tr>
<td bgcolor=#000000  onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#0000FF  onClick=Set('0000FF') height=10 width=10></td>
<td bgcolor=#000000  onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#990066  onClick=Set('990066') height=10 width=10></td>
<td bgcolor=#993366  onClick=Set('993366') height=10 width=10></td>
<td bgcolor=#996666  onClick=Set('996666') height=10 width=10></td>
<td bgcolor=#999966  onClick=Set('999966') height=10 width=10></td>
<td bgcolor=#99CC66  onClick=Set('99CC66') height=10 width=10></td>
<td bgcolor=#99FF66  onClick=Set('99FF66') height=10 width=10></td>
<td bgcolor=#CC0066  onClick=Set('CC0066') height=10 width=10></td>
<td bgcolor=#CC3366  onClick=Set('CC3366') height=10 width=10></td>
<td bgcolor=#CC6666  onClick=Set('CC6666') height=10 width=10></td>
<td bgcolor=#CC9966  onClick=Set('CC9966') height=10 width=10></td>
<td bgcolor=#CCCC66  onClick=Set('CCCC66') height=10 width=10></td>
<td bgcolor=#CCFF66  onClick=Set('CCFF66') height=10 width=10></td>
<td bgcolor=#FF0066  onClick=Set('FF0066') height=10 width=10></td>
<td bgcolor=#FF3366  onClick=Set('FF3366') height=10 width=10></td>
<td bgcolor=#FF6666  onClick=Set('FF6666') height=10 width=10></td>
<td bgcolor=#FF9966  onClick=Set('FF9966') height=10 width=10></td>
<td bgcolor=#FFCC66  onClick=Set('FFCC66') height=10 width=10></td>
<td bgcolor=#FFFF66  onClick=Set('FFFF66') height=10 width=10></td>
</tr>
<tr>
<td bgcolor=#000000  onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#FFFF00  onClick=Set('FFFF00') height=10 width=10></td>
<td bgcolor=#000000  onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#990099  onClick=Set('990099') height=10 width=10></td>
<td bgcolor=#993399  onClick=Set('993399') height=10 width=10></td>
<td bgcolor=#996699  onClick=Set('996699') height=10 width=10></td>
<td bgcolor=#999999  onClick=Set('999999') height=10 width=10></td>
<td bgcolor=#99CC99  onClick=Set('99CC99') height=10 width=10></td>
<td bgcolor=#99FF99  onClick=Set('99FF99') height=10 width=10></td>
<td bgcolor=#CC0099  onClick=Set('CC0099') height=10 width=10></td>
<td bgcolor=#CC3399  onClick=Set('CC3399') height=10 width=10></td>
<td bgcolor=#CC6699  onClick=Set('CC6699') height=10 width=10></td>
<td bgcolor=#CC9999  onClick=Set('CC9999') height=10 width=10></td>
<td bgcolor=#CCCC99  onClick=Set('CCCC99') height=10 width=10></td>
<td bgcolor=#CCFF99  onClick=Set('CCFF99') height=10 width=10></td>
<td bgcolor=#FF0099  onClick=Set('FF0099') height=10 width=10></td>
<td bgcolor=#FF3399  onClick=Set('FF3399') height=10 width=10></td>
<td bgcolor=#FF6699  onClick=Set('FF6699') height=10 width=10></td>
<td bgcolor=#FF9999  onClick=Set('FF9999') height=10 width=10></td>
<td bgcolor=#FFCC99  onClick=Set('FFCC99') height=10 width=10></td>
<td bgcolor=#FFFF99  onClick=Set('FFFF99') height=10 width=10></td>
</tr>
<tr>
<td bgcolor=#000000  onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#00FFFF  onClick=Set('00FFFF') height=10 width=10></td>
<td bgcolor=#000000  onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#9900CC  onClick=Set('9900CC') height=10 width=10></td>
<td bgcolor=#9933CC  onClick=Set('9933CC') height=10 width=10></td>
<td bgcolor=#9966CC  onClick=Set('9966CC') height=10 width=10></td>
<td bgcolor=#9999CC  onClick=Set('9999CC') height=10 width=10></td>
<td bgcolor=#99CCCC  onClick=Set('99CCCC') height=10 width=10></td>
<td bgcolor=#99FFCC  onClick=Set('99FFCC') height=10 width=10></td>
<td bgcolor=#CC00CC  onClick=Set('CC00CC') height=10 width=10></td>
<td bgcolor=#CC33CC  onClick=Set('CC33CC') height=10 width=10></td>
<td bgcolor=#CC66CC  onClick=Set('CC66CC') height=10 width=10></td>
<td bgcolor=#CC99CC  onClick=Set('CC99CC') height=10 width=10></td>
<td bgcolor=#CCCCCC  onClick=Set('CCCCCC') height=10 width=10></td>
<td bgcolor=#CCFFCC  onClick=Set('CCFFCC') height=10 width=10></td>
<td bgcolor=#FF00CC  onClick=Set('FF00CC') height=10 width=10></td>
<td bgcolor=#FF33CC  onClick=Set('FF33CC') height=10 width=10></td>
<td bgcolor=#FF66CC  onClick=Set('FF66CC') height=10 width=10></td>
<td bgcolor=#FF99CC  onClick=Set('FF99CC') height=10 width=10></td>
<td bgcolor=#FFCCCC  onClick=Set('FFCCCC') height=10 width=10></td>
<td bgcolor=#FFFFCC  onClick=Set('FFFFCC') height=10 width=10></td>
</tr>
<tr>
<td bgcolor=#000000  onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#FF00FF  onClick=Set('FF00FF') height=10 width=10></td>
<td bgcolor=#000000  onClick=Set('000000') height=10 width=10></td>
<td bgcolor=#9900FF  onClick=Set('9900FF') height=10 width=10></td>
<td bgcolor=#9933FF  onClick=Set('9933FF') height=10 width=10></td>
<td bgcolor=#9966FF  onClick=Set('9966FF') height=10 width=10></td>
<td bgcolor=#9999FF  onClick=Set('9999FF') height=10 width=10></td>
<td bgcolor=#99CCFF  onClick=Set('99CCFF') height=10 width=10></td>
<td bgcolor=#99FFFF  onClick=Set('99FFFF') height=10 width=10></td>
<td bgcolor=#CC00FF  onClick=Set('CC00FF') height=10 width=10></td>
<td bgcolor=#CC33FF  onClick=Set('CC33FF') height=10 width=10></td>
<td bgcolor=#CC66FF  onClick=Set('CC66FF') height=10 width=10></td>
<td bgcolor=#CC99FF  onClick=Set('CC99FF') height=10 width=10></td>
<td bgcolor=#CCCCFF  onClick=Set('CCCCFF') height=10 width=10></td>
<td bgcolor=#CCFFFF  onClick=Set('CCFFFF') height=10 width=10></td>
<td bgcolor=#FF00FF  onClick=Set('FF00FF') height=10 width=10></td>
<td bgcolor=#FF33FF  onClick=Set('FF33FF') height=10 width=10></td>
<td bgcolor=#FF66FF  onClick=Set('FF66FF') height=10 width=10></td>
<td bgcolor=#FF99FF  onClick=Set('FF99FF') height=10 width=10></td>
<td bgcolor=#FFCCFF  onClick=Set('FFCCFF') height=10 width=10></td>
<td bgcolor=#FFFFFF  onClick=Set('FFFFFF') height=10 width=10></td>
</tr>

</table>
<font color="#FFFFFF"><span lang="en-us" align="center"><font size="1">&nbsp; 
Click On Each Color To Change</font><font size="2"> <b>
<?php
if(isset($_REQUEST['color1']))
  echo "color1";
else if ( isset ($_REQUEST['color2']) )
  echo "color2";
else
  echo "color";
?>
</b></font>
</font></span></font></form>
</body></html>