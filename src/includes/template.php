<?php
/* **********************************************************************
*************************************************************************
####    This App Is Written By : Bahrambeigy - ITLPoll Version 3    #####
########       [https://gitlab.com/b.bahrambeigy/itlpoll]        ########
####              This application is free of charge :)             #####
####                Contact me: bahramwhh@gmail.com                 #####
*************************************************************************
#########################################################################
File : template.php
Writer : Bahrambeigy(bahramwhh@gmail.com)
Description : This File Will Parse ITLPoll Templates In a Class And 
Will Help functions.php to show and parse templates
**********************************************************************
*/

class template {
	var $lblock; // Loop Blocks
	var $nvars; // non loop vars
	var $fp; // opened file
		
	function parse() 
	{
		
		foreach ($this->nvars as $key => $val)
			$this->fp = preg_replace("/\{". $key ."\}/is", $val, $this->fp);


		// check if lblock exists or not
        if(!isset($this->lblock))
            return $this->fp;


		foreach ($this->lblock as $key => $val)
        {
            $str1 = stristr($this->fp, "<" . $key . ">");

            if(empty($str1)) {
                echo "Error : Invalid Tag  < ". $key ." >  In Your Templates!";
                exit;
            }

            $temp = explode($str1, $this->fp);
            $str2 = stristr($str1, "</" . $key . ">");

            if(empty($str1)) {
                echo "Error : Invalid Tag  < /". $key ." >  Expected!";
                exit;
            }

            $this->fp = $temp[0] . "<!--".$key."-->" . $str2;
            $temp2 = explode($str2, $str1);
            $blockstr = $temp2[0];

            $blockstr = preg_replace("/<". $key . ">/is", "", $blockstr);


            $blockstr_final = "";
            foreach ($val as $ikey => $ival) {
                $blockstr_temp = $blockstr;
                foreach ($ival as $iikey => $iival)
                    $blockstr_temp = preg_replace("/\{" . $iikey . "\}/is", $iival, $blockstr_temp);

                $blockstr_final .= $blockstr_temp;
            }

            $this->fp = preg_replace("/<!--".$key."-->/is", $blockstr_final, $this->fp);
        }

		
		return $this->fp;
		
		 		
	}
	
	// this function can replace whole block ! ;)
	function replace($block, $replacement)
	{
		$regex = "/<".$block.">(.*)<\/".$block.">/is";
		$this->fp = preg_replace($regex, $replacement, $this->fp);
	}	
}

?>