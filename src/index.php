<?php
/* **********************************************************************
*************************************************************************
####    This App Is Written By : Bahrambeigy - ITLPoll Version 3    #####
########       [https://gitlab.com/b.bahrambeigy/itlpoll]        ########
####              This application is free of charge :)             #####
####                Contact me: bahramwhh@gmail.com                 #####
*************************************************************************
#########################################################################
File : index.php
Writer : Bahrambeigy (bahramwhh@gmail.com)
Description : Index of ITLPoll System. This is the entrypoint of ITLPoll.
Please don't change any varriable on this file!
**********************************************************************
*/
@error_reporting (E_ERROR | E_PARSE);
@session_start();

// generation time start
$time = microtime();
$time = explode(" ", $time);
$time = $time[1] + $time[0];
$start = $time;

// Installation Check !
if(filesize("config.php") == 0)
{
  header("Location: install/index.php");
  @exit();
}
require_once("config.php");

if(!defined("ITLPoll_INSTALLED"))
{
  header("Location: install/index.php");
  @exit();
}

// Functions Existance Check
if(!(file_exists("includes/functions.php")))
{
    echo "Your Functions.php File Doesn't Exist in : includes/functions.php";
    @exit();
}
require_once("includes/functions.php");
$connected_db = db_connect($host, $user, $passwd, $database);
Load_Configs($prefix);

// Language File Existance Check
file_check("language/".$configs['language'].".php", "Error : Your Default Laguage File Doesn't Exist");
require_once("language/".$configs['language'].".php");

// Check first then Open Theme File And Use it!
file_check("templates/".$configs['template']."/user.xml", "Error : Your Default Template File(user.xml) Doesn't Exist");

$itl_fxml = file_get_contents("templates/".$configs['template']."/user.xml");

if($configs['disabled'] == "yes")
{
  echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
	  <b> ". _DISABLEDPOLL ."</b>";
  @exit();
}


/* *********** Voters Section (BEGIN) *********** */
if(($configs['voters_login'] == "yes") && !isset($_SESSION['Voters_Logged']))
{
	$current_session_str = session_id();
	
	if(isset($_GET['v']))
	{
		// voter account verification part
		@$vsession = filter($_GET['v']);
		@$uname = mb_strtolower(filter($_GET['uname']));
		if(empty($uname))
		{
			msg(_INVALIDVERIFICATIONLINK, "?");
			@exit();
		}
		$getv = "SELECT last_session FROM ".$prefix."_voters WHERE username='".$uname."';";
		$mgetv = mysqli_query($GLOBALS["___mysqli_ston"], $getv);
		$num_v = mysqli_num_rows($mgetv);
		if($num_v < 1)
		{
			msg(_INVALIDVERIFICATIONLINK, "?");
			@exit();
		}
		$getv_fetch = mysqli_fetch_array($mgetv);
		if($getv_fetch['last_session'] == $vsession)
		{
			if($configs['admin_voters_verification'] == "no")
				$set_enabled = ", enabled='yes'";
			else
				$set_enabled = "";
		
			$set_verified = "UPDATE ".$prefix."_voters SET verified='yes' ".$set_enabled." WHERE username='".$uname."';";
			$mverified = mysqli_query($GLOBALS["___mysqli_ston"], $set_verified);
			if($mverified)
			{
				if($configs['admin_voters_verification'] == "yes")
					msg(_SUCCVERIFIEDADMINWAIT, "?");
				else
					msg(_SUCCVERIFIEDCOMPLETE, "?");
				@exit();
			}
		}
		else
		{
			msg(_INVALIDVERIFICATIONLINK, "?");
			@exit();
		}
	}
	else if(isset($_GET['Voters_Register']))
	{
		@$username = mb_strtolower(filter($_POST['username']));
		@$password1 = filter($_POST['password1']);
		@$password2 = filter($_POST['password2']);
		@$captchastr = strtoupper(filter($_POST['ITLPoll_user_captcha_string']));
		if(empty($username) || empty($password1) || empty($password2) || empty($captchastr))
			Show_VotersRegister($itl_fxml);
		else
		{
			if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i", $username))
			{
				msg(_USERNAMENOTVALID, "?Voters_Register");
				@exit();
			}
			
			$query_name = "SELECT username FROM ".$prefix."_voters WHERE username='".$username."'";
			$query_exec = mysqli_query($GLOBALS["___mysqli_ston"], $query_name);
			$is_there = mysqli_num_rows($query_exec);
			if($is_there > 0)
			{
				msg(_USERNAMEUSEDBEFORE, "?Voters_Register");
				@exit();
			}
			
			if($password1 != $password2)
			{
				@session_start();
				$_SESSION['KeepEntries']['Username'] = $username;
				msg(_PASSERRORA, "?Voters_Register");
				@exit();
			}
			
			if(!captchaVerification($captchastr))
			{
				@session_start();
				$_SESSION['KeepEntries']['Username'] = $username;
				$_SESSION['KeepEntries']['Password'] = $password1;
				msg(_CAPTCHANOTCORRECT, "?Voters_Register");
				@exit();
			}
			
			// non of above
			$insert_voter = "INSERT INTO ".$prefix."_voters
			(username, password, last_session) VALUES
			('".$username."', '".md5($password1)."', '".$current_session_str."');";
			$m_voter = mysqli_query($GLOBALS["___mysqli_ston"], $insert_voter);
			if($m_voter)
			{
				$subject = "[ITLPoll] Survey Voter Verification Needed";
				$msg = "Hi dear user!,
You or someone else used your email for participate in a ITLPoll based survey. please click below link to verify your account.
Otherwise simply Ignore this message ;)
Verification Link : 
http://" . $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME'] . "?v=".$current_session_str."&uname=".$username."

Username: ".$username."
Password: ".$password1."
____________________
ITLPoll [http://www.itlpoll.com]";
				$header = 'From: "ITLPoll"';
				$mail = mail($username, $subject, $msg, $header);
				if($mail)
				{					
					msg(_THANKSVOTERREGISTER, "?Voters_Register");
					@exit();
				}
				else
				{
					msg(_ERRORMAIL, "?Voters_Register");
				}
			}
			else
			{
				msg(_CANTADDVOTERACCOUNT, "?Voters_Register");
				@exit();
			}
		}
	}
	
	else // login to system
	{
		@$username = mb_strtolower(filter($_POST['username']));
		@$password = filter($_POST['password']);
		@$captchastr = strtoupper(filter($_POST['ITLPoll_user_captcha_string']));
		if(empty($username) || empty($password) || empty($password))
		{
			Show_VotersLogin($itl_fxml);
			@exit();
		}
		else
		{
			$password_str = $password;
			$password = md5($password);
			$login_query = "SELECT username, password, enabled, verified FROM ".$prefix."_voters WHERE username='".$username."' AND password='".$password."'; ";
			$login_exec = mysqli_query($GLOBALS["___mysqli_ston"], $login_query);
			if(mysqli_num_rows($login_exec) < 1)
			{
				msg(_UPERROR, "?");
				@exit();
			}
			if(!captchaVerification($captchastr))
			{
				@session_start();
				$_SESSION['KeepEntries']['Username'] = $username;
				$_SESSION['KeepEntries']['Password'] = $password_str;
				msg(_CAPTCHANOTCORRECT, "?");
				@exit();
			}
				
			$login_details = mysqli_fetch_array($login_exec);
			if($login_details['verified'] == "no")
			{
				msg(_VOTERNOTVERIFIED, "?");
				@exit();
			}
			else if($login_details['enabled'] == "no")
			{
				msg(_VOTERNOTENABLED, "?");
				@exit();
			}
				
			// Login Voter to system
			@session_start();
			$_SESSION['Voters_Logged'] = base64_encode(stripslashes($username));
			$current_date = date("Y-m-d");
			$update_infos = "UPDATE ".$prefix."_voters SET last_login_date='".$current_date."', last_session='".$current_session_str."' WHERE username='".$username."';";
			$update_exec = mysqli_query($GLOBALS["___mysqli_ston"], $update_infos);
			
			msg(_LOGGED, "?");
			@exit();
		}
		
		
	}
	@exit();
}

if(isset($_GET['Voters_Logout']))
{
	unset($_SESSION['Voters_Logged']);
	session_destroy();
	msg(_VOTERLOGGEDOUT, "?");
	@exit();
}

/* *********** Voters Section (END) *********** */

@$id = filter($_REQUEST['pollID']);

if(empty($id)) $query = "SELECT * FROM ".$prefix."_poll WHERE confirmed='yes';";
else {
    // Checking $id For Injection (Bug Founded By Simorgh-ev Security Group)
    if(!preg_match("/[1-9]/", $id)) {
    echo "Hacking Attempt - You Can't set non numeric value for ID variable!";
    @exit();
    }
    else {
    $query = "SELECT * FROM ".$prefix."_poll WHERE id = '". $id ."'";
     }
}
$poll = mysqli_query($GLOBALS["___mysqli_ston"], $query);
$active_polls = mysqli_num_rows($poll);

if($active_polls > 1)
{
	define("MULTIPLEACTIVE", "1");
	$query = "SELECT multiactivestr FROM ".$prefix."_config";
	$mquery = mysqli_query($GLOBALS["___mysqli_ston"], $query);
	
	$multiactive = mysqli_fetch_array($mquery);
	define("MULTACTIVESTR", stripslashes($multiactive['multiactivestr']));
}


// Check Cookies Enabled ?!
if(isset($_REQUEST['ITLPollCheckCookie']))
{
	if( !isset($_COOKIE['ITLPoll_Voted']) )
    {
      setcookie("ITLPoll_Voted", "");
      msg(_ECOOKIES, "?");
    }
    else
    {
      // check if voter participated before or not ! ;) (version 3.2)
      if($configs['voters_login'] == "yes")
      {
      	$can_vote = FALSE;
      	
      	$username = stripslashes(base64_decode($_SESSION['Voters_Logged']));
      	$query = "SELECT last_voted_date FROM ".$prefix."_voters WHERE username='".$username."';";
      	$mquery = mysqli_query($GLOBALS["___mysqli_ston"], $query);
      	$fetch_date = mysqli_fetch_array($mquery);
      	
      	$active_poll_date = "SELECT multiactivedate FROM ".$prefix."_config";
		$active_poll_date_query = mysqli_query($GLOBALS["___mysqli_ston"], $active_poll_date);
		$active_poll_date_query = mysqli_fetch_array($active_poll_date_query);
		$active_poll_date_str = $active_poll_date_query['multiactivedate'];
      	
      	if($fetch_date['last_voted_date'] == "9999-12-31 00:00:00")
      		$can_vote = TRUE;
      	else if(Changed_Last_Poll($active_poll_date_str, $fetch_date['last_voted_date']))	
      		$can_vote = TRUE;
      	
      	if(!$can_vote)
      	{
      		msg(_YVOTED, "?");
      		@exit();
      	}
      }
    	
      if( isset($_COOKIE['ITLPoll_Valid_Post']) AND ($_COOKIE['ITLPoll_Valid_Post'] == "yes") )
      {
      	$ch = filter($_REQUEST['Choice']);
      	$ch = explode(":", $ch);
      	
      	$error_detected = false;
      	
      	// define arrays
      	$update = array();
      	$id_set = array();
      	
      	$real_numbers_counter = 0;
      	$counter = 0;
      	
      	// check created choice selected not created by malicious user ! ;)
      	if(isset($_SESSION['CreatedChoiceStr']) && ( (filter($_REQUEST['Choice']) != $_SESSION['CreatedChoiceStr']) ) )
      		$error_detected = true;
      	
      	
      	for($counter=0; ($counter < $active_polls) && !$error_detected; $counter++)
		{
			$situation = explode(",", $ch[$counter]);
			$is_multiple = $situation[0];
	      	
			
	      	// Check for Multiple Choices Enabled
	      	if($is_multiple == "s")
	      	{
	      		$ch_expld = explode("_", $situation[1]);
	      		
	      		$id = $ch_expld[0];
	      		if($id > 0);
	      		else
	      			$error_detected = true;
	      		
	      		
	      		$update[$counter] = "UPDATE ".$prefix."_results SET $ch_expld[1]=$ch_expld[1]+1 WHERE id=$id";
	      		$id_set[$counter] = $id;
	      		
	      		$real_numbers_counter++;
	      		
	      	}
	      	else if($is_multiple == "m")
	      	{
	      		$ch_expld = explode("|", $situation[1]);
	      		$s_str = "";
	      
	      		for($i=1; $i <= $ch_expld[0]; $i++)
	      		{
	      			$ch_explded = explode("_", $ch_expld[$i]);
	      			
	      			$id = $ch_explded[0];
	      			$ch_num = $ch_explded[1];
			  		$s_str .= ($i == $ch_expld[0]) ? $ch_num ."=" . $ch_num . "+1" : $ch_num ."=" . $ch_num . "+1, ";
	      			
	      		}
	      		if($ch_expld[0] > 0)
	      		{
	      			$update[$counter] = "UPDATE ".$prefix."_results SET ". $s_str ." WHERE id='".$id."'";
	      			//$mquery5 = mysql_query($query5);
	      		}
	      		
	      		else if($ch_expld[0] == 0)
	      		{
	      			$id = $ch_expld[1];
	      			$update[$counter] = "empty";
	      		}
	      		else
	      			$error_detected = true;
	      			
	      		$id_set[$counter] = $id;
	      		$real_numbers_counter++;	
	      		
	      		//$query6 = "UPDATE ".$prefix."_results SET uniques=uniques+1 WHERE id = '".$id."'";
	      		//$mquery6 = mysql_query($query6);
	      		
	      	}
	         
		}
		
		// uniques people queries
		if( !$error_detected && ($real_numbers_counter == $active_polls))
		{
			$query_update_uniques = "UPDATE ".$prefix."_results SET uniques=uniques+1 WHERE (";
			for($counter = 0; ($counter < $active_polls) && !$error_detected; $counter++)
			{
				if($update[$counter] == "empty")
					$query_update_uniques .= "id='".$id_set[$counter]."' " . (($counter == ($active_polls-1)) ? " )" : " OR ");
				else
				{	
					$mquery_run = mysqli_query($GLOBALS["___mysqli_ston"], $update[$counter]);
					if(!$mquery_run)
						$error_detected = true;
					else
						$query_update_uniques .= "id='".$id_set[$counter]."' " . (($counter == ($active_polls-1)) ? " )" : " OR ");	
				}
			}
			if(!$error_detected)
			{
				$mqeury_uniques = mysqli_query($GLOBALS["___mysqli_ston"], $query_update_uniques);
				if(!$mqeury_uniques)
					$error_detected = true;
			}
		}
		else
			$error_detected = true;
		
		
		if(!$error_detected)
		{
			setcookie("ITLPoll_Valid_Post", "no", time()+15552000);
			// update user informations if login section enabled
			if($configs['voters_login'] == "yes")
      		{
      			$username = stripslashes(base64_decode($_SESSION['Voters_Logged']));
      			$today = date("Y-m-d H:i:s");
      			$real_ip = getRealIpAddress();
      			$update_voter = "UPDATE ".$prefix."_voters SET last_voted_date='".$today."', total_votes=total_votes+1, last_voted_ip='".$real_ip."' WHERE username='".$username."';";
      			$update_exec = mysqli_query($GLOBALS["___mysqli_ston"], $update_voter);
      		}
      		
      		$update_total_votes = "UPDATE ".$prefix."_config SET total_votes=total_votes+1;";
      		$update_exec = mysqli_query($GLOBALS["___mysqli_ston"], $update_total_votes);
      		if($update_exec)
      			msg(_THANKS, "?");
      		else 
      			echo '<script>window.location="?"</script>';
		}
		else
		{
			// roll back ?! (we don't need !;) )
		}
		
      }
      else 
     	echo '<script>window.location="?"</script>';
      	
    } 
    @exit();  
}
// end of check cookies

// Check admin logged in already ( for showing menu ) - version 3.2 added
if( (isset($_SESSION['ITLPoll']['Admin'])) && ( Time_Valid($_SESSION['ITLPoll']['AdminTime'], $nowtime) ) )
	define('ADMIN_LOGGED_IN', TRUE);
else
	define('ADMIN_LOGGED_IN', FALSE);
	

@$action = filter($_REQUEST['action']);
if( !empty($id) )
{
	$choice_totals = 0;
	for($counter=0; $counter < $active_polls; $counter++)
	{

		$polllist = mysqli_fetch_array($poll);
	
		$id = $polllist['id'];
		$title = $polllist['subject'];
		$choices = $polllist['choices'];
		$multiple = $polllist['multiple'];
		$active = $polllist['active'];
		$expire = $polllist['expire'];
		$strdate = $polllist['start'];
		$crtdate = $polllist['created'];
		
		// Jalali date format check	
		if($usejalali == "1")
		{
			if($crtdate != "9999-12-31")
			{
				$create_date = explode("-", $crtdate);
				$created = ConvDate($create_date, "H");
				$crtdate = implode("-", $created);
			}
		}

		// Query The Results From Database
	    $query2 = "SELECT * FROM ".$prefix."_results WHERE id = $id";
	    
	    // Gathering uniques voters
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	    $choice_total = mysqli_fetch_array($result);
	    $choice_totals += $choice_total['uniques'];
	    
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	    $choice = mysqli_fetch_row($result);
	    // Optimize the database
	    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
	    // Query The Choices From Database
	    $query3 = "select * from ".$prefix."_choices where id = $id";
	    $cho = mysqli_query($GLOBALS["___mysqli_ston"], $query3);
	    $choi = mysqli_fetch_row($cho);
	    
	    // Optimize the database
	    ((mysqli_free_result($cho) || (is_object($cho) && (get_class($cho) == "mysqli_result"))) ? true : false);
	    
	    $sid = $counter . "|" . $id . "|"  .  $active_polls . "|" . $configs['total_votes'];
	    
	    // Finally Shows The Results Page!
		if($configs['defaultresult'] == "html") {
			Show_Results($crtdate, $choices, $title, $itl_fxml, $choice, $choi, $configs['color1'], $configs['color2'], $sid);
		}
		elseif($configs['defaultresult'] == "image") {
			
			if($configs['defaultgraph'] == "bar")
				Show_ImageResults($id, $crtdate, $choices, $title, $itl_fxml, $choice, $choi, "", "bar", $sid);
			else if($configs['defaultgraph'] == "pie")
				Show_ImageResults($id, $crtdate, $choices, $title, $itl_fxml, $choice, $choi, "", "pie", $sid);
			else
				Show_ImageResults($id, $crtdate, $choices, $title, $itl_fxml, $choice, $choi, "", "simple", $sid);
		}
		
	}  
}

// This Will Show The Archive Of Polls - Added On Version 2.00
elseif( isset($_REQUEST['Archive']) || ($action == "Archive") ) {
    
	// Query The Polls From Database
    $query = "SELECT id, subject FROM ".$prefix."_poll ORDER BY id DESC";
    $archive = mysqli_query($GLOBALS["___mysqli_ston"], $query);

    // Shows The Archives Page!
    Show_Archives($itl_fxml, $archive, $configs['numarchive'], $configs['color1'], $configs['color2']);
}

elseif(isset($_REQUEST['HtmlResults']) || ($action == "HtmlResults") ) {
	
	$choice_totals = 0;
	for($counter=0; $counter < $active_polls; $counter++)
	{

		$polllist = mysqli_fetch_array($poll);
	
		$id = $polllist['id'];
		$title = $polllist['subject'];
		$choices = $polllist['choices'];
		$multiple = $polllist['multiple'];
		$active = $polllist['active'];
		$expire = $polllist['expire'];
		$strdate = $polllist['start'];
		$crtdate = $polllist['created'];
		
		
		if($usejalali == "1")
		{
			if($crtdate != "9999-12-31")
			{
				$create_date = explode("-", $crtdate);
				$created = ConvDate($create_date, "H");
				$crtdate = implode("-", $created);
			}
		}
		
		
		// Query The Results From Database
	    $query2 = "SELECT * FROM ".$prefix."_results WHERE id = $id";
	    
	    // Gathering uniques voters
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	    $choice_total = mysqli_fetch_array($result);
	    $choice_totals += $choice_total['uniques'];
	    
	    
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	    $choice = mysqli_fetch_row($result);
	    
	    // Optimize the database
	    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
	    // Query The Choices From Database
	    $query3 = "select * from ".$prefix."_choices where id = $id";
	    $cho = mysqli_query($GLOBALS["___mysqli_ston"], $query3);
	    $choi = mysqli_fetch_row($cho);
	    // Optimize the database
	    ((mysqli_free_result($cho) || (is_object($cho) && (get_class($cho) == "mysqli_result"))) ? true : false);
	    
	    $id = $counter . "|" . $id . "|"  .  $active_polls . "|" . $configs['total_votes'];
	    
		// Finally Shows The Results Page!
		Show_Results($crtdate, $choices, $title, $itl_fxml, $choice, $choi, $configs['color1'], $configs['color2'], $id);
	}
}


// Image Result - Added On Version 2.5

elseif(isset($_REQUEST['ImageResults']) || ($action == "ImageResults") ) {
	
    $choice_totals = 0;
	for($counter=0; $counter < $active_polls; $counter++)
	{

		$polllist = mysqli_fetch_array($poll);
	
		$id = $polllist['id'];
		$title = $polllist['subject'];
		$choices = $polllist['choices'];
		$multiple = $polllist['multiple'];
		$active = $polllist['active'];
		$expire = $polllist['expire'];
		$strdate = $polllist['start'];
		$crtdate = $polllist['created'];
		
		
		if($usejalali == "1")
		{
			if($crtdate != "9999-12-31")
			{
				$create_date = explode("-", $crtdate);
				$created = ConvDate($create_date, "H");
				$crtdate = implode("-", $created);
			}
		}
	
		// Query The Results From Database
	    $query2 = "SELECT * FROM ".$prefix."_results WHERE id = $id";
	    
	    // Gathering uniques voters
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	    $choice_total = mysqli_fetch_array($result);
	    $choice_totals += $choice_total['uniques'];
	    
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	    $choice = mysqli_fetch_row($result);
	    // Optimize the database
	    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
	    // Query The Choices From Database
	    $query3 = "select * from ".$prefix."_choices where id = $id";
	    $cho = mysqli_query($GLOBALS["___mysqli_ston"], $query3);
	    $choi = mysqli_fetch_row($cho);
	    // Optimize the database
	    ((mysqli_free_result($cho) || (is_object($cho) && (get_class($cho) == "mysqli_result"))) ? true : false);
	    
	    $sid = $counter . "|" . $id . "|"  .  $active_polls . "|" . $configs['total_votes'];
	    
	    if($configs['defaultgraph'] == "bar")
			Show_ImageResults($id, $crtdate, $choices, $title, $itl_fxml, $choice, $choi, "", "bar", $sid);
		else if($configs['defaultgraph'] == "pie")
			Show_ImageResults($id, $crtdate, $choices, $title, $itl_fxml, $choice, $choi, "", "pie", $sid);
		else
			Show_ImageResults($id, $crtdate, $choices, $title, $itl_fxml, $choice, $choi, "", "simple", $sid);
	}
}

// End Of Image Result


// IF User Voted Shows The Results

elseif(isset($_COOKIE['ITLPoll_Voted']))
{
	$active_poll_date = "SELECT multiactivedate FROM ".$prefix."_config";
	$active_poll_date_query = mysqli_query($GLOBALS["___mysqli_ston"], $active_poll_date);
	$active_poll_date_query = mysqli_fetch_array($active_poll_date_query);
	$active_poll_date_str = $active_poll_date_query['multiactivedate'];
	
	 
	if( Changed_Last_Poll($active_poll_date_str) )
	{
		setcookie("ITLPoll_Voted", "");
    	msg(_CHANGEDLATESTPOLL, "?");
    	@exit();
	}
	 
	// reactive cookie !
	setcookie("ITLPoll_Voted", $active_poll_date_str, time()+15552000);
	 
	 $choice_totals = 0;
	 for($counter=0; $counter < $active_polls; $counter++)
	 {

		$polllist = mysqli_fetch_array($poll);
	
		$id = $polllist['id'];
		$title = $polllist['subject'];
		$choices = $polllist['choices'];
		$multiple = $polllist['multiple'];
		$active = $polllist['active'];
		$expire = $polllist['expire'];
		$strdate = $polllist['start'];
		$crtdate = $polllist['created'];
		
		
		if($usejalali == "1")
		{
			if($crtdate != "9999-12-31")
			{
				$create_date = explode("-", $crtdate);
				$created = ConvDate($create_date, "H");
				$crtdate = implode("-", $created);
			}
		}
		
	
		// Query The Results From Database
	    $query2 = "SELECT * FROM ".$prefix."_results WHERE id = $id";
	    
	     // Gathering uniques voters
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	    $choice_total = mysqli_fetch_array($result);
	    $choice_totals += $choice_total['uniques'];
	    
	    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query2);
	    $choice = mysqli_fetch_row($result);
	    // Optimize the database
	    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
	    // Query The Choices From Database
	    $query3 = "select * from ".$prefix."_choices where id = $id";
	    $cho = mysqli_query($GLOBALS["___mysqli_ston"], $query3);
	    $choi = mysqli_fetch_row($cho);
	    // Optimize the database
	    ((mysqli_free_result($cho) || (is_object($cho) && (get_class($cho) == "mysqli_result"))) ? true : false);
	    
	    $sid = $counter . "|" . $id . "|"  .  $active_polls . "|" . $configs['total_votes'];
	    
	    // Finally Shows The Results Page!
	    if($configs['defaultresult'] == "html") {
			// Read The Results Template Files
	    	Show_Results($crtdate, $choices, $title, $itl_fxml, $choice, $choi, $configs['color1'], $configs['color2'], $sid);

		}
		elseif($configs['defaultresult'] == "image") {
			
		    if($configs['defaultgraph'] == "bar")
				Show_ImageResults($id, $crtdate, $choices, $title, $itl_fxml, $choice, $choi, "", "bar", $sid);
			else if($configs['defaultgraph'] == "pie")
				Show_ImageResults($id, $crtdate, $choices, $title, $itl_fxml, $choice, $choi, "", "pie", $sid);
			else
				Show_ImageResults($id, $crtdate, $choices, $title, $itl_fxml, $choice, $choi, "", "simple", $sid);
		
		}
	}
}

else
{
    $query = "SELECT * FROM ".$prefix."_poll WHERE active = 'yes'";
	$mquery = mysqli_query($GLOBALS["___mysqli_ston"], $query);
	
	if( isset($_REQUEST['Post']) )
	{
		$script_str_js = '<script>window.location="?ITLPollCheckCookie&Choice='; //
		$script_str = ""; 
		for($counter_s=0, $counter_m; ($counter_s + $counter_m) < $active_polls;)
		{
	
			$polllist = mysqli_fetch_array($mquery);
	
			$id = $polllist['id'];
			$multiple = $polllist['multiple'];
			$choices = $polllist['choices'];
	
			$choice_str = $id . "_Choice";
			if($multiple == "no")
            {
            	@$ch_[$counter_s] = filter($_POST[$choice_str]);
                if(empty($ch_[$counter_s]))
                	$ch_[$counter_s] = $id . "_ch1";
            }
            elseif($multiple == "yes")
            {
            	$ch = array();
                for ($i=0; $i < $choices; $i++)
                {
                	$ch_num = $i + 1;
                	$textbox = $id . "_ch". $ch_num;
                	@$ch[$i][$counter_m] = filter($_POST[$textbox]);
                }
            }
		    
            if($multiple == "yes")
            {
            	$changed_choices = 0;
                $choices_queue = "";
                for($i=0; $i < $choices; $i++)
                {
                	if(!empty($ch[$i][$counter_m]))
                    {
                    $choices_queue .= $ch[$i][$counter_m] . "|";
                    $changed_choices++;
                	}
            	}
            	if($changed_choices > 0)
            		$choices_queue = 'm,' . $changed_choices . "|" . $choices_queue . ":";
            	else
            		$choices_queue = 'm,' . $changed_choices . "|" . $id . ":";
            	$script_str .= $choices_queue;
            	$counter_m++;
        	}
        	elseif($multiple == "no")
        	{
        		$script_str .= 's,' . $ch_[$counter_s] . ':';
        		$counter_s++;
        	}
		      
		}
		$script_str_js .= $script_str;
		$script_str_js .= "\"</script>";
		
		$active_poll_date = "SELECT multiactivedate FROM ".$prefix."_config";
		$active_poll_date_query = mysqli_query($GLOBALS["___mysqli_ston"], $active_poll_date);
		$active_poll_date_query = mysqli_fetch_array($active_poll_date_query);
		$active_poll_date_str = $active_poll_date_query['multiactivedate'];
		
		// Will be valid for ever ( it's enough big ! ) 
	    setcookie("ITLPoll_Voted", $active_poll_date_str, time()+15552000);
	    setcookie("ITLPoll_Valid_Post", "yes", time()+15552000);
	    
	    $_SESSION['CreatedChoiceStr'] = $script_str;
	    
		echo $script_str_js;
		@exit();
	}	

    for($counter=0; $counter < $active_polls; $counter++)
    {
	    $polllist = mysqli_fetch_array($poll);
	    
	    $id = $polllist['id'];
	    
		$title = $polllist['subject'];
		$choices = $polllist['choices'];
		$multiple = $polllist['multiple'];
		$active = $polllist['active'];
		$expire = $polllist['expire'];
		$strdate = $polllist['start'];
		$crtdate = $polllist['created'];
		
	    // Start Date Check - Added in Version 2.7
		StartDate_Check($strdate);
		
		// Expiration Check - Completed in Version 2.6
		Expiration_Check($expire);
		
		if($usejalali == "1")
		{
			if($crtdate != "9999-12-31")
			{
				$create_date = explode("-", $crtdate);
				$created = ConvDate($create_date, "H");
				$crtdate = implode("-", $created);
			}
		}
	    
	    // If User Doen't Click Submit Show The Radio Buttons
	    $query3 = "select * from ".$prefix."_choices where id = $id";
	    $cho = mysqli_query($GLOBALS["___mysqli_ston"], $query3);
	    $choi = mysqli_fetch_row($cho);
	    // Optimize the database
	    ((mysqli_free_result($cho) || (is_object($cho) && (get_class($cho) == "mysqli_result"))) ? true : false);
	    $id = $counter . "|" . $id . "|"  .  $active_polls;
	    Show_Body($crtdate, $choices, $title, $itl_fxml, $choi, $configs['color1'], $configs['color2'], $multiple, $id);
    }
}

((is_null($___mysqli_res = mysqli_close($connected_db))) ? false : $___mysqli_res);

// generation time
$time = microtime();
$time = explode(" ", $time);
$time = $time[1] + $time[0];
$finish = $time;
$totaltime = ($finish - $start);
if(defined("MULTIPLEACTIVE"))
{
	$generation_time_str = "<p style='text-align: right;'>"._GENERATIONTIME." <b> %f </b> "._SECONDS."</p>";
	printf($generation_time_str, $totaltime);
}

?>
