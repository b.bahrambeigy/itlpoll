<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head><title>Install ITLPoll System</title>


<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="install.css" type="text/css"></head><body bgcolor="#f4f3fe">
<p align="center">&nbsp;</p>
<table align="center" border="0" width="86%">
	<tbody><tr>
		<td>
<p align="center">
<font color="#ffffff">
<span style="font-size: 17pt; font-weight: 700;" lang="en-us"><img src="install.png">ITLPoll Installer<img src="install.png"></span></font></p>
<form method="post" action="index.php" name="install">
<p align="center">&nbsp;</p>
<table align="center" border="0" width="88%">
	<tbody><tr>
		<td width="942">
<noscript>
<div id="dvErr">
<div class="p1">
<img alt="" src="../images/alert.gif"/></div>
<div class="p2"><br><br>
<span>Ooops! ... </span>
<b>ITLPoll needs JavaScript to work correctly.</b> Please enable JavaScript in your browser or use a browser which supports JavaScript.
</div>
</div>
</noscript>
<replace>
	<table border="3" width="100%">
	<tbody><tr>
	<td colspan="3">
<p align="center"><b><span lang="en-us"><font color="#0055aa" size="4">mysql database and basic configurations :</font></span></b></p>
		</td>
	</tr>
	<tr>
		<td width="25%">
		<p align="right"><b><span class="fieldExpl" lang="en-us"><font color="#000000" size="2">Host :</font></span></b></p></td>
		<td width="24%">
	<p align="left"><span lang="en-us"><font size="2"><input name="Host" size="26" value="localhost" type="text"></font></span></p>
		</td>
		<td width="50%">
	<span lang="en-us"><font size="2">Default is : localhost</font></span></td>
	</tr>
	<tr>
		<td width="25%">
		<p align="right"><b><span class="fieldExpl" lang="en-us"><font color="#000000" size="2">Username :</font></span></b></p></td>
		<td width="24%">
	<p align="left"><span lang="en-us"><font size="2"><input name="UserName" size="26" value="<?php echo $user ?>" type="text"></font></span></p>
		</td>
		<td width="50%">
	<span lang="en-us">
	<font size="2">Enter mysql username here</font></span></td>
	</tr>
	<tr>
		<td width="25%">
		<p align="right"><b><span class="fieldExpl" lang="en-us"><font color="#000000" size="2">Password :
	</font>
	</span></b></p></td>
		<td width="24%">
	<p align="left"><span lang="en-us">
	<font size="2">
	<input name="PassWord" size="26" value="<?php echo $pass ?>"></font>
	</span></p>
		</td>
		<td width="50%">
	<span lang="en-us">
	<font size="2">
	Enter password of mysql user</font></span></td>
	</tr>
	<tr>
		<td width="25%">
		<p align="right"><b><span class="fieldExpl" lang="en-us"><font color="#000000" size="2">Database :</font></span></b></p></td>
		<td width="24%">
	<p align="left"><span lang="en-us"><font size="2"><input name="Database" size="26" value="<?php echo $database ?>" type="text"> </font>
	</span></p></td>
		<td width="50%">
	<span lang="en-us">
	<font size="2">Name of empty and newly created mysql database</font></span></td>
	</tr>
	<tr>
		<td width="25%">
		<p align="right"><b><font class="fieldExpl" color="#000000" size="2">Prefix<span lang="en-us">
		</span>:</font></b></p></td>
		<td width="24%">
	<p align="left"><span lang="en-us"><font size="2">
	<input name="Prefix" size="26" value="<?php echo $prefix ?>" type="text"></font></span></p></td>
		<td width="50%">
	<span lang="en-us"><font size="2">A string to be prepended to the name of tables</font></span></td>
	</tr>
	<tr>
		<td width="25%">
		<p align="right"><span class="fieldExpl" lang="en-us"><font color="#000000" size="2"><b>
		Choices Max Num</b></font></span><b><font color="#000000" size="2"><span lang="en-us">
		</span>:</font></b></p></td>
		<td width="24%">
	<p align="left"><span lang="en-us"><font size="2">
	<input name="MaxCH" size="26" value="<?php echo $maxchoices ?>" type="text"></font></span></p></td>
		<td width="50%">
	<font size="2">Maximum number of choices in each poll <font color="red"> (*)</font><br></font></td>
	</tr>
	<tr>
		<td width="25%">
		<p align="right"><b><font color="#000000" size="2"><span class="fieldExpl" lang="en-us">
		Use Jalali Date
		</span>:</font></b></p></td>
		<td width="24%">
	<p align="left"><span lang="en-us"><font size="2">
	<select size="1" name="UseJalali" type="submit">
		<option selected="selected">no</option>
		<option>yes</option>
		</select>
	&nbsp; </font></span></p></td>
		<td width="50%">
	<span lang="en-us"><font size="2">Use persian Jalali date format ?!</font></span></td>
	</tr>

	</tbody></table>
<table border="3" width="100%">
	<tbody><tr>
		<td colspan="2" width="95%">
	<p align="center"><b><font color="#0055aa" size="4">ITLPoll Administrator Configurations<span lang="en-us"> : </span></font></b></p>
		</td>
	</tr>
	<tr>
		<td width="45%">
		<p align="right">
		<b>
		<span class="fieldExpl" lang="en-us"><font color="#000000" size="2">Default Language : </font> </span>
		</b></p></td>
		<td width="53%">
		<p align="left"><font size="2">
		<?php echo $s_lang; ?></font></p>
</td>
	</tr>
	<tr>
		<td width="45%">
		<p align="right"><b><font color="#000000" size="2">
		<span class="fieldExpl" lang="en-us">Default Template </span>:</font></b></p></td>
		<td width="53%">
		<p align="left"><font size="2">
		<?php echo $s_temp; ?></font></p>
</td>
	</tr>
	<tr>
		<td width="45%">
		<p align="right"><b><span class="fieldExpl" lang="en-us"><font color="#000000" size="2">
		Administrator Username :</font></span></b></p></td>
		<td width="53%">
		<p align="left"><span lang="en-us">
		<font size="2">
	<input name="AdminName" size="24" value="<?php echo $admin ?>"></font></span></p></td>
	</tr>
	<tr>
		<td width="45%">
		<p align="right"><b><span class="fieldExpl" lang="en-us"><font color="#000000" size="2">
		Administrator Password :</font></span></b></p></td>
		<td width="53%">
	<p align="left"><span lang="en-us"><font size="2">
	<input name="AdminPass" size="24" type="password"></font></span></p>
		</td>
	</tr>
	<tr>
		<td width="45%">
		<p align="right"><b><span class="fieldExpl" lang="en-us"><font color="#000000" size="2">
		Administrator Email :</font></span></b></p></td>
		<td width="53%">
	<p align="left"><span lang="en-us"><font size="2">
	<input name="AdminEmail" size="24" value="<?php echo $email ?>"></font></span></p></td>
	</tr>
	<tr>
		<td width="45%">
		<p align="right"><b><span class="fieldExpl" lang="en-us"><font color="#000000" size="2">
		Enable Voters Accounting ? :</font></span></b></p></td>
		<td width="53%">
	<p align="left"><span lang="en-us"><font size="2">
	<input name="Enable_Voters" size="24" type="radio" value="yes"> Yes &nbsp;&nbsp; <input name="Enable_Voters" size="24" type="radio" value="no" checked> No</font></span><br><font color="blue" size="2">(Admin Confirmation is disabled by default. You can enable it in ITLPoll voters management page of ITLPoll admin panel)</font></p>
		</td>
	</tbody></table>
<table border="3" width="100%">
	<tbody><tr>
		<td width="95%">
		<p align="center">
		<input value="Install ITLPoll" class="fieldExpl" name="Install1" style="font-family: Tahoma; font-size: 18pt; font-weight: bold; -webkit-border-radius: 3px; border-radius: 3px; " type="submit"><input value="Reset" name="reset" type="reset"></p></td>
	
	</tr>
</tbody></table>
		</td>
	</tr>
</tbody></table>
</form>

<table align="center" border="2" width="87%">
	<tbody><tr>
		<td><font style="font-weight: bold;" size="2">Installing Notes:</font><br>
            <font color="red">(*)</font> <font size="1">Please enter the smallest value based on your needs. Large number may affect speed of ITLPoll system, because of huge number of NULL columns in the database.</font><br>
            <p><font color="#224466" size="1"><span lang="en-us"><b>1- Before installing this 
		script you should have created an empty database for system.<br>
		2- Please change mode of "config.php" 
		file to 777.</b></span></font><span lang="en-us"><font color="#ff0000" size="1"><b><br>
		3- If you want use left to right language for 
		default system language use metalic-ltr template and If you want use 
		right to left 
		language use metalic-rtl template.</b></font></span><font color="#224466" size="1"><span lang="en-us"><b><br>
		4- After installing the ITLPoll, please remove "install" folder for more safety.<br>
		5-&nbsp; Please always get latest version from
		<a href="http://www.itlpoll.com">Here</a>.</b></span></font></p>


		
            <hr>
		
            <p align="center"><b><span lang="en-us"><font size="2"><a target="_blank" href="http://www.itlpoll.com/">Information Technology Light Poll System</a> | Licence : <a href="http://www.gnu.org/licenses/gpl-2.0.html">GPLv2</a>
		<br> Programmer : </font> <a href="mailto:bahramwhh@gmail.com">
		<font size="2">Bahrambeigy</a> </font></span></b></p>
</td>
	</tr>
</tbody></table>
&nbsp;</td>
	</tr>
</replace>
</tbody></table>
</body></html>
