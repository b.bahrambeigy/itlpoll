<?php
/* **********************************************************************
*************************************************************************
####    This App Is Written By : Bahrambeigy - ITLPoll Version 3    #####
########       [https://gitlab.com/b.bahrambeigy/itlpoll]        ########
####              This application is free of charge :)             #####
####                Contact me: bahramwhh@gmail.com                 #####
*************************************************************************
#########################################################################
File : install.php
Writer : Bahrambeigy (bahramwhh@gmail.com)
Description : This file installs the script automatically without
any prior knowledge of PHP or mysql structures.
**********************************************************************
*/
@session_start();

// Create Short Variables
@$host = addslashes(trim($_POST['Host']));
@$user = addslashes(trim($_POST['UserName'])); 
@$pass = addslashes($_POST['PassWord']); 
@$database = addslashes(trim($_POST['Database']));
@$prefix = addslashes(trim($_POST['Prefix']));
@$usejalali = addslashes(trim($_POST['UseJalali']));
@$maxchoices = addslashes(trim($_POST['MaxCH']));
@$admin =  addslashes(trim($_POST['AdminName']));
@$passwd =  addslashes($_POST['AdminPass']);
@$lang =  addslashes(trim($_POST['lang']));
@$template =  addslashes(trim($_POST['template']));
@$email =  addslashes(trim($_POST['AdminEmail']));
@$enable_voters = addslashes(trim($_POST['Enable_Voters']));

$current_version = "ITLPoll Version 3.2.5 - Final"; // current version to insert database


if (isset($_GET['CodeGen']))
{
	@$sess = $_SESSION['ITLINST'];
	if(empty($sess))
	{
		echo "<b>Error ! You Are Not in the Install Process!</b>";
		@exit();
	}
	
	$vars = explode(';', $sess);
	echo '<textarea cols="52" rows="10"><?php
define("ITLPoll_INSTALLED", 1);

// iniset Configs
@ini_set("expose_PHP", "Off");
@ini_set("display_errors", 1);
@ini_set("log_errors", "Off");
@ini_set("html_errors", "Off");
@ini_set("asp_tags", "Off");
@ini_set("register_globals", "Off");
@ini_set("implicit_flush", "On");


// Connecting to mysql Configs
$host = "'.$vars[0].'"; // Default is Locahost
$user = "'.$vars[1].'"; // The mysql Admin User !
$passwd = "'.$vars[2].'"; // The mysql Admin Password
$database = "'.$vars[3].'"; // The Database That ITLPoll Install There
$prefix = "'.$vars[4].'"; // The String That Will Add Before Each Table
$usejalali = "'.$vars[5].'" // Use Jalali Date Fomrat? 1=yes, 0=no
?></textarea>';
	unset($_SESSION['ITLINST']);
	session_destroy();
	exit;
}

if(empty($host) || empty($user) || empty($database) || empty($admin) || empty($passwd) || empty($lang) || empty($template) || empty($email) )
{
    if(empty($prefix)) $prefix="itl";
	if(empty($maxchoices)) $maxchoices = "20";
	if(empty($usejalali)) $usejalali = "no";
	
	if(!($dp = opendir("../language")))
		$s_lang = "Error : Can't Read Languages Directory !" ;
	$s_lang = "<select name='lang'>";
    while($file = readdir($dp)) {
    if($file !='.' && $file !='..' && $file !='index.html') {
       $file = explode(".", $file);
       if($file[0] == "English")
	  $s_lang .= "<option selected>$file[0]</option>";
       else
	  $s_lang .= "<option>$file[0]</option>";
      }
    }
    $s_lang .= "</select>";

	if(!($dp = opendir("../templates")))
		$s_temp = "Error : Can't Read Templates Directory !" ;
	$s_temp = "<select name='template'>";
    while($file = readdir($dp)) {
    if($file !='.' && $file !='..' && $file !='index.html') {
        if($file == "metalic-ltr")
	  $s_temp .= "<option selected>$file</option>";
	else
	  $s_temp .= "<option>$file</option>";
      }
    }
    $s_temp .= "</select>";
	
	require('install.tpl');
    exit;
    
}
else
{
    if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i", $email)) {
    echo '<script>alert("Your Email Is not Valid Please Go Back And Correct It!")</script>';
    echo "<script language=\"JavaScript\">history.back(1)</script>";
    exit; 
	}
	
	if(!preg_match("/[1-9]/", $maxchoices))
    {
    echo "Hacking Attempt! - You Entered Invalid Value For Maximum Choices!";
    exit;
    }
	
    @$db = ($GLOBALS["___mysqli_ston"] = mysqli_connect($host, $user, $pass));
    if(!$db)
    {
      echo "<br><b><font color=\"#FF0000\">Error : Your mysql Configuration Isn't Correct. Please Go Back And Check Them!</font></b>";
      exit;
    }
    else
    {
        if ( empty($pass))
        {
			echo "<script>
var pass = confirm('Are You Sure to Continue With an empty password? (not recomended)');
if(!pass)
	window.location = '?';
</script>";	
		}	
		
		$fp = file_get_contents("install.tpl");
        $str1 = stristr($fp, "<replace>");
    	$fch = explode($str1, $fp);
    	$str2 = stristr($str1, "</replace>");
    	
    	echo $fch[0];
    	
    	echo "<script>function WriteProg(text, id)
{
	document.getElementById(id).innerHTML = text;
}
</script>";
    	
    	echo "<table border=\"0\" width=\"100%\" id=\"progress\">";
		echo "<tr>
		<td><div id=\"LatestResult\"></div></td>
		</tr>";
    	
		if($usejalali == "yes")
			$usejalali = "1";
		elseif($usejalali == "no")
			$usejalali = "0";
		else
			$usejalali = "0";
		
		
		mysqli_select_db($GLOBALS["___mysqli_ston"], $database);
		
		/* ******** Create Poll Table *************** */
		
		echo "<tr>
		<td><div id=\"CreateTablePoll\"></div></td>
		<td><div id=\"CreateResultPoll\"></div></td>
		</tr>";
		
		echo "<script> WriteProg('Creating Table ".$prefix."_poll ...', 'CreateTablePoll')</script>";
		
        $install = "create table `".$prefix."_poll`
        (`id` INT PRIMARY KEY AUTO_INCREMENT,
        `subject` varchar(1000) not null,
        `choices` int not null,
        `multiple` set('yes', 'no') not null default 'no',
        `active` set('yes', 'no') not null,
        `confirmed` set('yes', 'no') not null default 'no',
        `expire` date default '9999-12-31',
		`start` date default '9999-12-31',
		`created` date default '9999-12-31' ) ENGINE=INNODB;";
        if(mysqli_query($GLOBALS["___mysqli_ston"], $install)) 
			echo "<script> WriteProg('<b> COMPLETED </b>', 'CreateResultPoll')</script>";
        else 
        {
		echo "<script> WriteProg('<font color=\"#FF0000\">An Error Accoured ! The Tables Are Exists Or The Program Can not Create The Tables!</font>', 'LatestResult')</script>";
			exit;
		}
        
        
		/* ******** Create Choices Table *************** */
        echo "<tr>
		<td><div id=\"CreateTableChoices\"></div></td>
		<td><div id=\"CreateResultChoices\"></div></td>
		</tr>";
        
		echo "<script> WriteProg('Creating Table ".$prefix."_choices ...', 'CreateTableChoices')</script>";
		
		for($i=1; $i <= $maxchoices; $i++)
			$s_str .= ($i == $maxchoices) ? "`ch". $i ."` varchar(255) " : "`ch". $i ."` varchar(255), ";
			
		$install = "create table `".$prefix."_choices`
        (`id` INT PRIMARY KEY AUTO_INCREMENT, 
		".$s_str." ,
		FOREIGN KEY (`id`) REFERENCES ".$prefix."_poll(`id`)
		ON UPDATE CASCADE ON DELETE CASCADE
		) ENGINE=INNODB;";

        if(mysqli_query($GLOBALS["___mysqli_ston"], $install)) 
			echo "<script> WriteProg('<b> COMPLETED </b>', 'CreateResultChoices')</script>";
		else 
		{
			echo "<script> WriteProg('<font color=\"#FF0000\">An Error Accoured ! Cannot Create Table Choices!</font>', 'LatestResult')</script>";
			exit;
		}
		
		
		/* ******** Create Results Table *************** */
		echo "<tr>
		<td><div id=\"CreateTableResults\"></div></td>
		<td><div id=\"CreateResultResults\"></div></td>
		</tr>";
		
        echo "<script> WriteProg('Creating Table ".$prefix."_results ...', 'CreateTableResults')</script>";
        
		for($i=1; $i <= $maxchoices; $i++)
			$v_str .= ($i == $maxchoices) ? "`ch". $i ."` int " : "`ch". $i ."` int , ";
		
		$install = "create table `".$prefix."_results`
        (`id` INT PRIMARY KEY AUTO_INCREMENT,
        ".$v_str.",
		`uniques` INT default '0',
		FOREIGN KEY (`id`) REFERENCES ".$prefix."_poll(`id`)
		ON UPDATE CASCADE ON DELETE CASCADE
		) ENGINE=INNODB;";
        
        if(mysqli_query($GLOBALS["___mysqli_ston"], $install)) 
			echo "<script> WriteProg('<b> COMPLETED </b>', 'CreateResultResults')</script>";
		else 
		{
			echo "<script> WriteProg('<font color=\"#FF0000\">An Error Accoured ! Cannot Create Table Results!</font>', 'LatestResult')</script>";
			exit;
		}
			
        echo "<script> WriteProg('<b>Tables Created Successully!</b>', 'LatestResult')</script>";
        
        
        /* ******** Add Sample Poll to Database *************** */
		echo "<tr>
		<td><div id=\"AddSample\"></div></td>
		<td><div id=\"SampleResult\"></div></td>
		</tr>";
        
		echo "<script> WriteProg('<br> Adding a Sample Poll To Database ....</b>', 'AddSample')</script>";
        
        $now_year = date('Y');
	$now_month = date('m');
	$now_day = date('d');

	$now_date = $now_year . "-" . $now_month . "-" . $now_day;
	$next_year_date = ($now_year+1) . "-" . $now_month . "-" . $now_day;
	$prev_month_date = $now_year . "-" . ($now_month-1) . "-" . $now_day;

	$sample = "INSERT INTO `".$prefix."_poll`
        (`subject`, `choices`, `multiple`, `active`, `confirmed`, `expire`, `start`, `created`) values
        ('Installing ITLPoll Script Was...', '4', 'no', 'yes', 'yes', '".$next_year_date."', '".$prev_month_date."', '".$now_date."')";
        mysqli_query($GLOBALS["___mysqli_ston"], $sample);
        $sample = "INSERT INTO `".$prefix."_choices`
        (`ch1`, `ch2`, `ch3`, `ch4`) values
        ('Very Easy', 'Not Easy', 'A Little Hard', 'Very Hard')";
        mysqli_query($GLOBALS["___mysqli_ston"], $sample);
        $sample = "INSERT INTO `".$prefix."_results`
        (`ch1`, `ch2`, `ch3`, `ch4`, `uniques`) values
        ('12', '5', '2', '1', '20')";
        mysqli_query($GLOBALS["___mysqli_ston"], $sample);
        
        $sample = "INSERT INTO `".$prefix."_poll`
        (`subject`, `choices`, `multiple`, `active`, `confirmed`, `expire`, `start`, `created`) values
        ('What do you think about ITLPoll?', '4', 'yes', 'yes', 'yes', '".$next_year_date."', '".$prev_month_date."', '".$now_date."')";
        mysqli_query($GLOBALS["___mysqli_ston"], $sample);
        $sample = "INSERT INTO `".$prefix."_choices`
        (`ch1`, `ch2`, `ch3`, `ch4`) values
        ('Great, never seen something like that!', 'Great, but I have seen something like this', 'Not bad ! I like it ;) ', 'Not so good !')";
        mysqli_query($GLOBALS["___mysqli_ston"], $sample);
        $sample = "INSERT INTO `".$prefix."_results`
        (`ch1`, `ch2`, `ch3`, `ch4`, `uniques`) values
        ('20', '10', '5', '3', '20')";
        
        if(mysqli_query($GLOBALS["___mysqli_ston"], $sample)) 
			echo "<script> WriteProg('<b> COMPLETED </b>', 'SampleResult')</script>";
		else 
		{
			echo "<script> WriteProg('<font color=\"#FF0000\">An Error Accoured ! Cannot Add Sample Polls</font>', 'LatestResult')</script>";
			exit;
		}

		/* ******** Create Groups Table *************** */
		echo "<tr>
		<td><div id=\"CreateTableGroups\"></div></td>
		<td><div id=\"CreateResultGroups\"></div></td>
		</tr>";
		
		echo "<script> WriteProg('<br>Creating Groups Table ...', 'CreateTableGroups')</script>";
		$groups = "create table `".$prefix."_groups`
        (`name` char(10) not null PRIMARY KEY ,
        `can_add` set('yes', 'no') not null ,
		`can_edit` set('yes', 'no') not null,
        `can_delete` set('yes', 'no') not null ,
        `can_config` set('yes', 'no') not null ,
        `can_group` set('yes', 'no') not null ) ENGINE=INNODB;";
        if(mysqli_query($GLOBALS["___mysqli_ston"], $groups)) 
		echo "<script> WriteProg('<b> COMPLETED </b>', 'CreateResultGroups')</script>";
			
        $group = "INSERT INTO `".$prefix."_groups`
        (`name`, `can_add`, `can_edit`, `can_delete`, `can_config`, `can_group`) values
        ('God', 'yes', 'yes', 'yes', 'yes', 'yes');";
        
        if(mysqli_query($GLOBALS["___mysqli_ston"], $group)) 
        {
			$group = "INSERT INTO `".$prefix."_groups`
        	(`name`, `can_add`, `can_edit`, `can_delete`, `can_config`, `can_group`) values
        	('Guest', 'yes', 'yes', 'no', 'no', 'no');";
        	mysqli_query($GLOBALS["___mysqli_ston"], $group);
		}
        else 
        {
			echo "<script> WriteProg('<font color=\"#FF0000\">An Error Accoured ! Cannot Insert System Groups!</font>', 'LatestResult')</script>";
			exit;
		}
		
		/* ******** Create Users Table *************** */
		echo "<tr>
		<td><div id=\"CreateTableUsers\"></div></td>
		<td><div id=\"CreateResultUsers\"></div></td>
		</tr>";
		
		echo "<script> WriteProg('<br>Creating Users Table ...', 'CreateTableUsers')</script>";
		$table_users = "create table `".$prefix."_users`
        (`username` char(50) not null PRIMARY KEY,
        `password` char(50) not null ,
		`permission` char(10) not null,
		`blocked` set('yes', 'no') not null,
		`email` char(50) not null,
        `forgot` set('yes', 'no') not null,
        `last_session` char(40),
		`lastlogin` char(39),
		FOREIGN KEY (`permission`) REFERENCES ".$prefix."_groups(`name`)
		ON UPDATE CASCADE ON DELETE CASCADE
		) ENGINE=INNODB;";
		
        if(mysqli_query($GLOBALS["___mysqli_ston"], $table_users)) 
		echo "<script> WriteProg('<b> COMPLETED </b>', 'CreateResultUsers')</script>";
			
        $passwd = md5($passwd);
		$table_user = "INSERT INTO `".$prefix."_users`
        (`username`, `password`, `permission`, `blocked`, `email`, `forgot`) values
        ('".$admin."', '".$passwd."', 'God', 'no', '".$email."', 'no');";
        
        if(mysqli_query($GLOBALS["___mysqli_ston"], $table_user)) {}
        else 
        {
			echo "<script> WriteProg('<font color=\"#FF0000\">An Error Accoured ! Cannot Insert System Users!</font>', 'LatestResult')</script>";
			@exit();
		}
		
    	/* ******** Create Voters Table *************** */
		echo "<tr>
		<td><div id=\"CreateTableVoters\"></div></td>
		<td><div id=\"CreateResultVoters\"></div></td>
		</tr>";
		
		echo "<script> WriteProg('<br>Creating Voters Table ...', 'CreateTableVoters')</script>";
		$table_voters = "create table `".$prefix."_voters`
        (`username` char(50) not null PRIMARY KEY,
        `password` char(50) not null ,
		`last_voted_date` datetime not null default '9999-12-31 00:00:00',
		`last_voted_ip` char(39) not null default '0.0.0.0',
		`last_login_date` date not null default '9999-12-31',
		`total_votes` INT not null default '0',
        `enabled` set('yes', 'no') not null default 'no',
        `verified` set('yes', 'no') not null default 'no',
        `last_session` char(40) default 'NULL' ) ENGINE=INNODB;";
        if(mysqli_query($GLOBALS["___mysqli_ston"], $table_voters)) 
			echo "<script> WriteProg('<b> COMPLETED </b>', 'CreateResultVoters')</script>";
		
		
		/* ******** Create Configs Table *************** */
		echo "<tr>
		<td><div id=\"CreateTableConfigs\"></div></td>
		<td><div id=\"CreateResultConfigs\"></div></td>
		</tr>";
			
        echo "<script> WriteProg('<br>Creating Configs Table ...', 'CreateTableConfigs')</script>";
		
		$configs = "create table `".$prefix."_config`
        (`disabled` set('yes', 'no') not null ,
        `language` char(10) not null ,
        `template` char(20) not null ,
        `color1` char(8) not null ,
        `color2` char(8) not null ,
        `version` char(100) not null,
        `numarchive` int(3) not null,
		`defaultresult` set('html', 'image') not null,
		`defaultgraph` char(3),
		`maxchoices` int not null default 30,
		`total_votes` int not null default '0',
		`voters_login` set('yes', 'no') not null default 'no',
		`admin_voters_verification` set('yes', 'no') not null default 'no',
	 	`multiactivestr` varchar(1000) default 'NULL',
	 	`multiactivedate` datetime default '9999-12-31 00:00:00' ) ENGINE=INNODB;";
        if(mysqli_query($GLOBALS["___mysqli_ston"], $configs)) 
			echo "<script> WriteProg('<b> COMPLETED </b>', 'CreateResultConfigs')</script>";
			
        // set voters management enabled if user selected !
        $set_voters1 = "";
        $set_voters2 = "";
        if($enable_voters == "yes")
        {
        	$set_voters1 = " `voters_login`,";
        	$set_voters2 = " 'yes',";
        }
        
        $now_date = date("Y-m-d H:i:s");
			
			$config = "INSERT INTO `".$prefix."_config`
        (`disabled`, `language`, `template`, `color1` , `color2`, `version`, `numarchive`, `defaultresult`, `defaultgraph`, `maxchoices`, total_votes, ".$set_voters1." `multiactivestr`, `multiactivedate`) values
        ('No', '".$lang."', '".$template."', '#ECEBFE', '#E0DEEB', '".$current_version."', '15', 'html', 'pie', '".$maxchoices."', '20', ".$set_voters2." 'Hi every body, This is a public survey using ITLPoll. Please give some of your time here and answer questions. <br>Thanks, website administrator', '".$now_date."')";
		//echo $config;
        if(mysqli_query($GLOBALS["___mysqli_ston"], $config)) {}
        else {
			echo "<script> WriteProg('<font color=\"#FF0000\">An Error Accoured ! Cannot Insert System Configs!</font>', 'LatestResult')</script>";
			@exit();
		}
		
        @$fp = fopen("../config.php", "r+");
        if(empty($fp))
        {
		  
			$_SESSION['ITLINST'] = $host . ";" . $user . ";" . $pass . ";" . $database . ";" . $prefix . ";" . $usejalali ;
			echo "<script> WriteProg('<br><b>Your Config.php File Is <font color=\"red\"> Unwritable </font></b><br><b>Please Open config.php And Copy & Paste These Codes There : <br><iframe name=\"codes\" src=\"?CodeGen\" marginheight=\"10\" width=\"500\" height=\"200\" scrolling=\"yes\"><b><a href=\"?CodeGen\" target=\"blank\">Click Here To View..</a></b></iframe><br><br><br>Thank You For Installing ITLPOLL Script!<br><b><font color=\"#FF0000\">Now Please Remove The Install Folder In Your Root ITLPoll Script</font>.<br><br> <p align=\"center\"><a href=\"../index.php\" target=\"_blank\"> ITLPoll Index </a>&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;   <a href=\"../admin/index.php\" target=\"_blank\"> ITLPoll Admin Panel </a> </b></p>', 'LatestResult')</script>";	
			
        
      	}
      	if(!empty($fp))
      	{
			$code = '<?php
define("ITLPoll_INSTALLED", 1);

// iniset Configs
@ini_set("expose_PHP", "Off");
@ini_set("display_errors", 1);
@ini_set("log_errors", "Off");
@ini_set("html_errors", "Off");
@ini_set("asp_tags", "Off");
@ini_set("register_globals", "Off");
@ini_set("implicit_flush", "On");		

// Connecting to mysql Configs
$host = "'.$host.'"; // Default is Locahost
$user = "'.$user.'"; // The mysql Admin User !
$passwd = "'.$pass.'"; // The mysql Admin Password
$database = "'.$database.'"; // The Database That ITLPoll Install There
$prefix = "'.$prefix.'"; // The String That Will Add Before Each Table
$usejalali = "'.$usejalali.'" // Use Jalali Date Fomrat? 1=yes, 0=no
?>';
    	
			fwrite($fp, $code);
    		fclose($fp);
		echo "<script> WriteProg('<p align=\"center\">Thank You For Installing ITLPOLL Script!<br><br><b><font color=\"#FF0000\"> Please Remove The Install Folder In Your Root ITLPoll Script Now !</font>.<br><br> <a href=\"../index.php\" target=\"_blank\"> ITLPoll Index </a>&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;   <a href=\"../admin/index.php\" target=\"_blank\"> ITLPoll Admin Panel </a> </b></p>', 'LatestResult')</script>";	
      	}
	}
	echo "</table>";
	echo $str2;
}
?>
