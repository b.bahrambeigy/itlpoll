<?php
/* **********************************************************************
*************************************************************************
####    This App Is Written By : Bahrambeigy - ITLPoll Version 3    #####
########       [https://gitlab.com/b.bahrambeigy/itlpoll]        ########
####              This application is free of charge :)             #####
####                Contact me: bahramwhh@gmail.com                 #####
*************************************************************************
#########################################################################
File : cli.php
Writer : Bahrambeigy (bahramwhh@gmail.com)
Description : CLI Installer
**********************************************************************
*/


$current_version = "ITLPoll Version 3.2.5 - Final"; // current version to insert database


if (!defined('STDIN')) {
    echo "This is a CLI script! Please run it using cli";
    exit(1);
}

if ($argc < 5)
{
    echo "Welcome to ITLPoll CLI installer :)\n";
    echo "In order to install it please pass the following arguments (optionals are indicated in brackets):\n";
    echo "(Database Host)     -> Default: mysql\n";
    echo "(Database User)     -> Default: itlpoll\n";
    echo "(Database Password) -> Default: itlpoll\n";
    echo "(Database Name)     -> Default: itlpoll\n";
    echo "[Use Jalali Date]   -> Default: no\n";
    echo "[Maximum Choices]   -> Default: 20\n";
    echo "[Admin Username]    -> Default: itlpoll\n";
    echo "[Admin Password]    -> Default: itlpoll\n";
    echo "[Admin Email]       -> Default: bahramwhh@gmail.com\n";
    echo "[Default Language]  -> Default: English\n";
    echo "[Default Template]  -> Default: metalic-ltr\n";
    echo "[Enable Voters]     -> Default: no\n";
    exit(1);
}



// Create Short Variables
@$host = (!isset($argv[1]) || is_null($argv[1])) ? "mysql" : $argv[1];
@$user = (!isset($argv[2]) || is_null($argv[2])) ? "itlpoll" : $argv[2];
@$pass = (!isset($argv[3]) || is_null($argv[3])) ? "itlpoll" : $argv[3];
@$database = (!isset($argv[4]) || is_null($argv[4])) ? "itlpoll" : $argv[4];
@$prefix = (!isset($argv[5]) || is_null($argv[5])) ? "itl" : $argv[5];
@$usejalali = (!isset($argv[6]) || is_null($argv[6])) ? "no" : $argv[6];
@$maxchoices = (!isset($argv[7]) || is_null($argv[7])) ? "20" : $argv[7];
@$admin =  (!isset($argv[8]) || is_null($argv[8])) ? "itlpoll" : $argv[8];
@$passwd =  (!isset($argv[9]) || is_null($argv[9])) ? "itlpoll" : $argv[9];
@$lang =  (!isset($argv[10]) || is_null($argv[10])) ? "English" : $argv[10];
@$template = (!isset($argv[11]) || is_null($argv[11])) ? "metalic-ltr" : $argv[11];
@$email =  (!isset($argv[12]) || is_null($argv[12])) ? "bahramwhh@gmail.com" : $argv[12];
@$enable_voters = (!isset($argv[13]) || is_null($argv[13])) ? "no" : $argv[13];


if(empty($prefix)) $prefix="itl";
if(empty($maxchoices)) $maxchoices = "20";
if(empty($usejalali)) $usejalali = "no";

if($usejalali == "yes")
    $usejalali = "1";
elseif($usejalali == "no")
    $usejalali = "0";
else
    $usejalali = "0";



$config_file = '<?php
define("ITLPoll_INSTALLED", 1);

// iniset Configs
@ini_set("expose_PHP", "Off");
@ini_set("display_errors", 1);
@ini_set("log_errors", "Off");
@ini_set("html_errors", "Off");
@ini_set("asp_tags", "Off");
@ini_set("register_globals", "Off");
@ini_set("implicit_flush", "On");		

// Connecting to mysql Configs
$host = "'.$host.'"; // Default is Locahost
$user = "'.$user.'"; // The mysql Admin User !
$passwd = "'.$pass.'"; // The mysql Admin Password
$database = "'.$database.'"; // The Database That ITLPoll Install There
$prefix = "'.$prefix.'"; // The String That Will Add Before Each Table
$usejalali = "'.$usejalali.'" // Use Jalali Date Fomrat? 1=yes, 0=no
?>';

if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i", $email)) {
    echo 'Your Email Is not Valid Please Go Back And Correct It!';
    exit;
}

if(!preg_match("/[1-9]/", $maxchoices))
{
    echo "Hacking Attempt! - You Entered Invalid Value For Maximum Choices!";
    exit;
}

@$db = ($GLOBALS["___mysqli_ston"] = mysqli_connect($host, $user, $pass));
if(!$db)
{
    echo "ERROR: Your mysql Configuration Isn't Correct. Please Go Back And Check Them!";
    exit;
}
else
{
        mysqli_select_db($GLOBALS["___mysqli_ston"], $database);

        /* ******** Create Poll Table *************** */

        echo "Creating Table .$prefix._poll ...";

        $install = "create table `".$prefix."_poll`
        (`id` INT PRIMARY KEY AUTO_INCREMENT,
        `subject` varchar(1000) not null,
        `choices` int not null,
        `multiple` set('yes', 'no') not null default 'no',
        `active` set('yes', 'no') not null,
        `confirmed` set('yes', 'no') not null default 'no',
        `expire` date default '9999-12-31',
		`start` date default '9999-12-31',
		`created` date default '9999-12-31' ) ENGINE=INNODB;";
        if(mysqli_query($GLOBALS["___mysqli_ston"], $install))
            echo "COMPLETED. \n";
        else
        {
            echo "An Error Occurred!! The Tables Are Exists Or The Program Can not Create The Tables!\n";
            exit;
        }


        /* ******** Create Choices Table *************** */
        echo "Creating Table .$prefix._choices ... ";

        for($i=1; $i <= $maxchoices; $i++)
            $s_str .= ($i == $maxchoices) ? "`ch". $i ."` varchar(255) " : "`ch". $i ."` varchar(255), ";

        $install = "create table `".$prefix."_choices`
        (`id` INT PRIMARY KEY AUTO_INCREMENT, 
		".$s_str." ,
		FOREIGN KEY (`id`) REFERENCES ".$prefix."_poll(`id`)
		ON UPDATE CASCADE ON DELETE CASCADE
		) ENGINE=INNODB;";

        if(mysqli_query($GLOBALS["___mysqli_ston"], $install))
            echo "COMPLETED. \n";
        else
        {
            echo "An Error Occurred ! Cannot Create Table Choices! \n";
            exit;
        }


        /* ******** Create Results Table *************** */

        echo "Creating Table ".$prefix."_results ...";

        for($i=1; $i <= $maxchoices; $i++)
            $v_str .= ($i == $maxchoices) ? "`ch". $i ."` int " : "`ch". $i ."` int , ";

        $install = "create table `".$prefix."_results`
        (`id` INT PRIMARY KEY AUTO_INCREMENT,
        ".$v_str.",
		`uniques` INT default '0',
		FOREIGN KEY (`id`) REFERENCES ".$prefix."_poll(`id`)
		ON UPDATE CASCADE ON DELETE CASCADE
		) ENGINE=INNODB;";

        if(mysqli_query($GLOBALS["___mysqli_ston"], $install))
            echo "COMPLETED. \n";
        else
        {
            echo "An Error Occurred ! Cannot Create Table Results! \n";
            exit;
        }

        echo "All Tables Created Successully! \n";


        /* ******** Add Sample Poll to Database *************** */
        echo "Adding a Sample Poll To Database ....";

        $now_year = date('Y');
        $now_month = date('m');
        $now_day = date('d');

        $now_date = $now_year . "-" . $now_month . "-" . $now_day;
        $next_year_date = ($now_year+1) . "-" . $now_month . "-" . $now_day;
        $prev_month_date = $now_year . "-" . ($now_month-1) . "-" . $now_day;

        $sample = "INSERT INTO `".$prefix."_poll`
        (`subject`, `choices`, `multiple`, `active`, `confirmed`, `expire`, `start`, `created`) values
        ('Installing ITLPoll Script Was...', '4', 'no', 'yes', 'yes', '".$next_year_date."', '".$prev_month_date."', '".$now_date."')";
        mysqli_query($GLOBALS["___mysqli_ston"], $sample);
        $sample = "INSERT INTO `".$prefix."_choices`
        (`ch1`, `ch2`, `ch3`, `ch4`) values
        ('Very Easy', 'Not Easy', 'A Little Hard', 'Very Hard')";
        mysqli_query($GLOBALS["___mysqli_ston"], $sample);
        $sample = "INSERT INTO `".$prefix."_results`
        (`ch1`, `ch2`, `ch3`, `ch4`, `uniques`) values
        ('12', '5', '2', '1', '20')";
        mysqli_query($GLOBALS["___mysqli_ston"], $sample);

        $sample = "INSERT INTO `".$prefix."_poll`
        (`subject`, `choices`, `multiple`, `active`, `confirmed`, `expire`, `start`, `created`) values
        ('What do you think about ITLPoll?', '4', 'yes', 'yes', 'yes', '".$next_year_date."', '".$prev_month_date."', '".$now_date."')";
        mysqli_query($GLOBALS["___mysqli_ston"], $sample);
        $sample = "INSERT INTO `".$prefix."_choices`
        (`ch1`, `ch2`, `ch3`, `ch4`) values
        ('Great, never seen something like that!', 'Great, but I have seen something like this', 'Not bad ! I like it ;) ', 'Not so good !')";
        mysqli_query($GLOBALS["___mysqli_ston"], $sample);
        $sample = "INSERT INTO `".$prefix."_results`
        (`ch1`, `ch2`, `ch3`, `ch4`, `uniques`) values
        ('20', '10', '5', '3', '20')";

        if(mysqli_query($GLOBALS["___mysqli_ston"], $sample))
            echo "COMPLETED. \n";
        else
        {
            echo "An Error Occurred ! Cannot Insert Sample Polls! \n";
            exit;
        }


    /* ******** Create Groups Table *************** */

        echo "Creating Groups Table ...";
        $groups = "create table `".$prefix."_groups`
        (`name` char(10) not null PRIMARY KEY ,
        `can_add` set('yes', 'no') not null ,
		`can_edit` set('yes', 'no') not null,
        `can_delete` set('yes', 'no') not null ,
        `can_config` set('yes', 'no') not null ,
        `can_group` set('yes', 'no') not null ) ENGINE=INNODB;";
        if(mysqli_query($GLOBALS["___mysqli_ston"], $groups))
            echo "COMPLETED. \n";

        $group = "INSERT INTO `".$prefix."_groups`
        (`name`, `can_add`, `can_edit`, `can_delete`, `can_config`, `can_group`) values
        ('God', 'yes', 'yes', 'yes', 'yes', 'yes');";

        if(mysqli_query($GLOBALS["___mysqli_ston"], $group))
        {
            $group = "INSERT INTO `".$prefix."_groups`
        	(`name`, `can_add`, `can_edit`, `can_delete`, `can_config`, `can_group`) values
        	('Guest', 'yes', 'yes', 'no', 'no', 'no');";
            mysqli_query($GLOBALS["___mysqli_ston"], $group);
        }
        else
        {
            echo "An Error Occurred ! Cannot Insert Sample Groups! \n";
            exit;
        }

        /* ******** Create Users Table *************** */

        echo "Creating Users Table ...";
        $table_users = "create table `".$prefix."_users`
        (`username` char(50) not null PRIMARY KEY,
        `password` char(50) not null ,
		`permission` char(10) not null,
		`blocked` set('yes', 'no') not null,
		`email` char(50) not null,
        `forgot` set('yes', 'no') not null,
        `last_session` char(40),
		`lastlogin` char(39),
		FOREIGN KEY (`permission`) REFERENCES ".$prefix."_groups(`name`)
		ON UPDATE CASCADE ON DELETE CASCADE
		) ENGINE=INNODB;";

        if(mysqli_query($GLOBALS["___mysqli_ston"], $table_users))
            echo "COMPLETED. \n";

        $passwd = md5($passwd);
        $table_user = "INSERT INTO `".$prefix."_users`
        (`username`, `password`, `permission`, `blocked`, `email`, `forgot`) values
        ('".$admin."', '".$passwd."', 'God', 'no', '".$email."', 'no');";

        if(mysqli_query($GLOBALS["___mysqli_ston"], $table_user)) {}
        else
        {
            echo "An Error Occurred ! Cannot Insert System Users! \n";
            @exit();
        }

        /* ******** Create Voters Table *************** */

        echo "Creating Voters Table ...";
        $table_voters = "create table `".$prefix."_voters`
        (`username` char(50) not null PRIMARY KEY,
        `password` char(50) not null ,
		`last_voted_date` datetime not null default '9999-12-31 00:00:00',
		`last_voted_ip` char(39) not null default '0.0.0.0',
		`last_login_date` date not null default '9999-12-31',
		`total_votes` INT not null default '0',
        `enabled` set('yes', 'no') not null default 'no',
        `verified` set('yes', 'no') not null default 'no',
        `last_session` char(40) default 'NULL' ) ENGINE=INNODB;";
        if(mysqli_query($GLOBALS["___mysqli_ston"], $table_voters))
            echo "COMPLETED. \n";


        /* ******** Create Configs Table *************** */

        echo "Creating Configs Table ...";

        $configs = "create table `".$prefix."_config`
        (`disabled` set('yes', 'no') not null ,
        `language` char(10) not null ,
        `template` char(20) not null ,
        `color1` char(8) not null ,
        `color2` char(8) not null ,
        `version` char(100) not null,
        `numarchive` int(3) not null,
		`defaultresult` set('html', 'image') not null,
		`defaultgraph` char(3),
		`maxchoices` int not null default 30,
		`total_votes` int not null default '0',
		`voters_login` set('yes', 'no') not null default 'no',
		`admin_voters_verification` set('yes', 'no') not null default 'no',
	 	`multiactivestr` varchar(1000) default 'NULL',
	 	`multiactivedate` datetime default '9999-12-31 00:00:00' ) ENGINE=INNODB;";
        if(mysqli_query($GLOBALS["___mysqli_ston"], $configs))
            echo "COMPLETED. \n";

        // set voters management enabled if user selected !
        $set_voters1 = "";
        $set_voters2 = "";
        if($enable_voters == "yes")
        {
            $set_voters1 = " `voters_login`,";
            $set_voters2 = " 'yes',";
        }

        $now_date = date("Y-m-d H:i:s");

        $config = "INSERT INTO `".$prefix."_config`
        (`disabled`, `language`, `template`, `color1` , `color2`, `version`, `numarchive`, `defaultresult`, `defaultgraph`, `maxchoices`, total_votes, ".$set_voters1." `multiactivestr`, `multiactivedate`) values
        ('No', '".$lang."', '".$template."', '#ECEBFE', '#E0DEEB', '".$current_version."', '15', 'html', 'pie', '".$maxchoices."', '20', ".$set_voters2." 'Hi every body, This is a public survey using ITLPoll. Please give some of your time here and answer questions. <br>Thanks, website administrator', '".$now_date."')";
        //echo $config;
        if(mysqli_query($GLOBALS["___mysqli_ston"], $config)) {}
        else {
            echo "An Error Occoured! Cannot Insert System Configs!\n";
            @exit();
        }

        echo "Config File Contents: \n";
        echo "$config_file";
        echo "\n";

        $file_write = file_put_contents("./config.php", $config_file);
        if ($file_write) {
            echo "SUCCESSFULLY INSTALLED.\n";
            echo "Thank You For Installing ITLPOLL Script!\n\n";
        }

        else{
            echo "Config.php file could not be written :(";
        }


        echo "Please Remove The Install Folder In Your Root ITLPoll Script Now !";

}
?>
